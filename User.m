//
//  User.m
//  ClueBox
//
//  Created by user on 11/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize userName = userName_;
@synthesize password = password_;
@synthesize token = token_;
@synthesize email = email_;
@synthesize numberOfReferral = numberOfReferral_;
@synthesize ListEmails = ListEmails_;

@synthesize IsFirstTime = IsFirstTime_;

@synthesize floor = floor_;
@synthesize unit = unit_;
@synthesize block = block_;
@synthesize building = building_;
@synthesize street = street_;
@synthesize postalCode = postalCode_;
@synthesize country = country_;
@synthesize contact = contact_;

@synthesize name = name_;
@synthesize dob = dob_;
@synthesize gender = gender_;
@synthesize maritalStatus = maritalStatus_;
@synthesize education = education_;
@synthesize occupation = occupation_;
@synthesize race = race_;
@synthesize religion = religion_;


static User *activeUser = nil;

+(User *)activeUser {
    
    @synchronized(self) {
        
        if(activeUser == nil) {
            
            activeUser = [[User alloc] init];
        }
    }
    return activeUser;
}

- (void) dealloc
{
    self.email = nil;
	self.userName = nil;
	self.password = nil;
    self.token = nil;
    self.numberOfReferral = nil;
    self.ListEmails = nil;
    
    self.floor = nil;
    self.unit = nil;
    self.block = nil;
    self.building = nil;
    self.street = nil;
    self.postalCode = nil;
    self.country = nil;
    self.contact = nil;
    
    self.name = nil;
    self.dob = nil;
    self.gender = nil;
    self.maritalStatus = nil;
    self.education = nil;
    self.occupation = nil;
    self.race = nil;
    self.religion = nil;
    
    
	//[super dealloc];
}

@end
