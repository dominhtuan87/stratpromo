//
//  UpdateProfileAPI.h
//  ClueBox
//
//  Created by cat on 28/11/12.
//
//

#import <Foundation/Foundation.h>
#import "SOAPWebService.h"
#import "User.h"

@protocol UpdateProfileAPIDelegate <NSObject>
@optional

-(void)api:(id)sender didFinishUpdateProfileWithStatus:(BOOL)status;
-(void)api:(id)sender didFailUpdateProfileWithError:(NSString*)error;

@end

@interface UpdateProfileAPI : NSObject <SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <UpdateProfileAPIDelegate> * UpdateProfileDelegate_;
}

@property(nonatomic,assign) NSObject <UpdateProfileAPIDelegate> * UpdateProfileAPIDelegate;

- (void)UpdateProfileOfUser:(User *)user withToken:(NSString*)token;

@end

