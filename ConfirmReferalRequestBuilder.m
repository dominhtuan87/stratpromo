//
//  ConfirmReferalRequestBuilder.m
//  ClueBox
//
//  Created by Rakesh Raveendran on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ConfirmReferalRequestBuilder.h"
#import "Utility.h"

@implementation ConfirmReferalRequestBuilder

+(NSString*)confirmReferalForUserToken:(NSString *)token withEmail:(NSString*)email {
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ConfirmReferal" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__" 
																 withString:token];
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Email__" 
																 withString:email];
		return contentFormat;
	}
	
	return @""; 
}

@end
