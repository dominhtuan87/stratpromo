//
//  ReferralsWebservice.m
//  ClueBox
//
//  Created by cat on 5/12/12.
//
//

#import "ReferralsAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "ReferralsRequestBuilder.h"


@implementation ReferralsAPI

@synthesize ReferralsAPIDelegate = ReferralsAPIDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}

-(void)ReferralsWithToken:(NSString *)token withFriends:(NSString *)Friends{
    [self retain];
	service_.serviceKey = REFERRALS_KEY;
	NSString * soapBody = [ReferralsRequestBuilder ReferralsWithToken:token andFriends:Friends];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:REFERRALS_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
    
    if ([responseData objectForKey:REFERRALS_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:REFERRALS_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"ReferralsResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            NSLog(@"errorReason: %@",[TBXML textForElement:errorOnFailure]);
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        
        NSLog(@"[TBXML textForElement:status]: %@",[TBXML textForElement:status]);
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            
            NSLog(@"[self.ReferralsAPIDelegate]: %@",self.ReferralsAPIDelegate);
            if (self.ReferralsAPIDelegate) {
                NSLog(@" if (self.ReferralsAPIDelegate) ");
                if ([self.ReferralsAPIDelegate respondsToSelector:@selector(api:didFinishReferralsWithStatus:andMessage:)]) {
                    NSLog(@"if ([self.ReferralsAPIDelegate respondsToSelector:@selector(api:didFinishReferralsWithStatus:)])");
                    errorReason = [TBXML textForElement:errorOnFailure];
                    
                    [self.ReferralsAPIDelegate api:self didFinishReferralsWithStatus:YES andMessage:errorReason];
                }
            }
        } else {
            
            if (self.ReferralsAPIDelegate) {
                
                if ([self.ReferralsAPIDelegate respondsToSelector:@selector(api:didFailReferralsWithError:)]) {
                    [self.ReferralsAPIDelegate api:self didFailReferralsWithError:errorReason];
                }
            }
        }
        
    } else {
        
        if (self.ReferralsAPIDelegate) {
            if ([self.ReferralsAPIDelegate respondsToSelector:@selector(api:didFailReferralsWithError:)]) {
                
                [self.ReferralsAPIDelegate api:self didFailReferralsWithError:@"Unknown Error"];
            }
        }
    }
    
    
    
    [responseData release];
    [self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.ReferralsAPIDelegate = nil;
	[super dealloc];
}

@end
