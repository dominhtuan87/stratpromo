//
//  CBRegisterViewController.m
//  ClueBox
//
//  Created by cat on 12/9/12.
//
//

#import "CBRegisterViewController.h"

#import "User.h"
#import "ActivityView.h"
#import "Constant.h"
#import "Utility.h"
#import "CBConfirmReferralViewController.h"
//#import "CBCategoryViewController.h"


int temp=0;

@interface CBRegisterViewController (private)

- (void)setupConfirmReferalAPI;

@end

@implementation CBRegisterViewController
@synthesize userName;
@synthesize email;
@synthesize password;
@synthesize confirmPassword;
@synthesize activity1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    activity1 = [[ActivityView alloc] init];
    
    //self.navigationItem.hidesBackButton = YES;
    
    }

-(IBAction)backgroundTap:(id)sender {
        
    [userName resignFirstResponder];
	[password resignFirstResponder];
    [confirmPassword resignFirstResponder];
	[email resignFirstResponder];
}

- (IBAction)submitButtonPressed:(id)sender {
    [userName resignFirstResponder];
	[password resignFirstResponder];
    [confirmPassword resignFirstResponder];
	[email resignFirstResponder];
    
    NSLog(@"temp: %i", temp );
    
    if ([self isValid]) {
		
        [activity1 presentActivityView:SIGNUP_LOADING];
        User *user = [[User alloc] init];
        user.userName = self.userName.text;
        user.email = self.email.text;
        user.password = self.password.text;
        LoginAPI *loginAPI = [[LoginAPI alloc] init];
        loginAPI.authDelegate = self;
        [loginAPI signUpForUser:user];
        [loginAPI release];
        [user release];
    }
        
}

-(BOOL)isValid {
    BOOL status = YES;
    NSString * message = nil;
    
    NSLog(@"userName_.text: %@", self.userName.text);
    if(![self.userName.text length]){
        status = NO;
        NSLog(@"Status userName_: %c", status);
        message = @"Name field cannot be empty";
        
    }
    else if(![self.email.text length]){
        status = NO;
        NSLog(@"Status email_: %c", status);
        message = @"Email field cannot be empty";
    }
    else if([Utility isValidEmail:self.email.text]==NO){
        status = NO;
        NSLog(@"Status email_: %c", status);
        message = @"Please enter a valid email";
    }
    else if(![self.password.text length]){
        status = NO;
        NSLog(@"Status password_: %c", status);
        message = @"Password field cannot be empty";
    }
    else if(![self.confirmPassword.text length]){
        status = NO;
        NSLog(@"Status password_: %c", status);
        message = @"Confirm Password field cannot be empty";
    }
    else if(![self.password.text isEqualToString:self.confirmPassword.text]){
        status = NO;
        NSLog(@"Status password_: %c", status);
        message = @"Password and Confirm Password must be same";
    }
    
    if (status==NO) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
    return status;
}


-(void)api:(id)sender didFinishSignUpWithStatus:(BOOL)status {
    
    [activity1 dismissActivityView];
    
    [[NSUserDefaults standardUserDefaults]setValue:@"YES" forKey:@"FirstLogin"];
    
    
    NSLog(@"[[User activeUser].ListEmails count]: %lu", (unsigned long)[[User activeUser].ListEmails count]);
    
    if ([[User activeUser].ListEmails count] > 0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please choose a referrer" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        NSLog(@"ListEmails MAIN ---- %@",[User activeUser].ListEmails);
        
        NSArray *dataSource;
        
        dataSource = [[[User activeUser].ListEmails sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] retain];
        NSLog(@"Openning dataSource: %@",dataSource);
        
        GenericPickerViewController *pickerViewController = [[GenericPickerViewController alloc] initWithDataSource:dataSource andDelegate:self];
        
        pickerViewController.delegate = self;
        
        //[pickerViewController.label setHidden:false];
        
        //[pickerViewController setUI];
        
        pickerViewController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:pickerViewController.view];

    }
    
    else
        [self loginButtonPressed:(id)1];
    
    
    
    
    /*if(status) {
        
        [self setupConfirmReferalAPI];
    }
     */
    //self.userName.text = @"";
    //self.email.text = @"";
    //self.password.text = @"";
    //self.confirmPassword.text = @"";
    
    
    //[Utility showAlertViewWithMessage:@"Your account has been setup! Press OK to go to Home Screen." withDelegate:self];
    
}

- (void)pickerViewCancelButtonDidPressed:(id)sender{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please choose a referrer" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [alert release];

}

- (void)pickerViewDoneButtonDidPressed: (id)sender withIndex:(int)index andTag:(int)tag{
    NSLog(@"DONE button pressed");
    //[self.view removeFromSuperview];
    NSLog(@"CHOICE: %@", sender);
    //NSLog(@"index: %i", index);
    //NSLog(@"tag: %i", tag);
    //UITextField * textField = (UITextField *) [self.mscrollview viewWithTag:tag];
    //textField.text = sender;
    //textField = (UITextField *) [self.mscrollview viewWithTag:(tag+100)];
    //textField.text = [NSString stringWithFormat:@"%d", index];
    
    //NSLog(@"ID: %@", textField.text);
    [self setupConfirmReferalAPI:sender];
    
    
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    //[self.navigationController popViewControllerAnimated:YES];
    [self loginButtonPressed:(id)1];
}


- (void)setupConfirmReferalAPI: (NSString*)email {
    NSLog(@"email: %@",email);
    [activity1 presentActivityView:ACTIVITY_INDICATOR_MESSAGE];
    ReferalAPI *refralAPI = [[ReferalAPI alloc] init];
    refralAPI.referalDelegate = self;
    [refralAPI confirmReferalForUserToken:[User activeUser].token withEmail:email];
    [refralAPI release];
    
    
}

-(void)api:(id)sender didFinishConfirmReferalWithStatus:(BOOL)status {
    
    [activity1 dismissActivityView];
    [self loginButtonPressed:(id)1];
}

-(void)api:(id)sender didFailConfirmReferalWithError:(NSString*)error {
    
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:error];
}

- (IBAction)loginButtonPressed:(id)sender {
    
    [activity1 presentActivityView:LOGIN_LOADING];
    User *user = [[User alloc] init];
    user.userName = self.email.text;
    user.password = self.password.text;
    LoginAPI *loginAPI = [[LoginAPI alloc] init];
    loginAPI.authDelegate = self;
    [loginAPI authorizeUser:user];
    [loginAPI release];
    [user release];
}

#pragma mark - LoginAPI Delegate Methods

-(void)api:(id)sender didFinishUserAuthenticationWithStatus:(BOOL)status {
    //if(rememberButton_.selected == YES) {
        NSLog(@"userName_: %@",self.email.text);
        NSLog(@"password_: %@",self.password.text);
        
        NSDictionary * userInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.email.text,@"username",self.password.text,@"password",nil];
        [Utility setRememberUserInfo:userInfo];
        
    //}
    //else{
    //    [Utility removeUserInfo];
    //}
    
    
    
    [activity1 dismissActivityView];
    if(status) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
}

-(void)api:(id)sender didFailAuthWithError:(NSString*)error {
    
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:error];
    NSLog(@"%@",error);
}


- (void)addBackButton {
    
    UIImage *image = [UIImage imageNamed: @"logo.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    self.navigationItem.titleView = imageView;
    
    UIImage *buttonImage = [UIImage imageNamed:@"back_btn_black.png"];
    backButton_ = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton_ setImage:buttonImage forState:UIControlStateNormal];
    [backButton_ setFrame:CGRectMake(10,(self.navigationController.navigationBar.frame.size.height-40)/2+5, 50, 30)];
    [backButton_ addTarget:self action:@selector(goToHomeScreen) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:backButton_];
}

- (void)goToHomeScreen {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) slideFrameUp2;
{
    [self slideFrame2:YES];
}

-(IBAction) slideFrameUp1;
{
    [self slideFrame1:YES];
}
-(IBAction) slideFrameUp;
{
    [self slideFrame:YES];
}

-(IBAction) slideFrameDown;
{
    [self slideFrame:NO];
}

-(IBAction) slideFrameDown1;
{
    [self slideFrame1:NO];
}

-(IBAction) slideFrameDown2;
{
    [self slideFrame2:NO];
}

-(void) slideFrame:(BOOL) up
{
    const int movementDistance = 40; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    temp = movement;
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(void) slideFrame1:(BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void) slideFrame2:(BOOL) up
{
    const int movementDistance = 120; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    //self.navigationItem.hidesBackButton = YES;
    [self addBackButton];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [backButton_ removeFromSuperview];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    //[backButton_ removeFromSuperview];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

- (void)dealloc {
	
    [super dealloc];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
