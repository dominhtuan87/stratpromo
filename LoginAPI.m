//
//  LoginAPI.m
//  ClueBox
//
//  Created by user on 11/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginAPI.h"
#import "LoginRequestBuilder.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"

@implementation LoginAPI

@synthesize authDelegate = authDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}

- (void)authorizeUser:(User *)user {
    
    [self retain];
	service_.serviceKey = LOGIN_KEY;
	NSString * soapBody = [LoginRequestBuilder authRequestContentForUser:user withAuthType:LOGIN];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:LOGIN_SOAP_ACTION];
}

- (void)loginAgainWithToken:(NSString *)token {
    
    [self retain];
	service_.serviceKey = LOGIN_AGAIN_KEY;
	NSString * soapBody = [LoginRequestBuilder loginAgainForUserToken:token];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:LOGIN_AGAIN_SOAP_ACTION];
}

- (void)logoutUserWithToken:(NSString *)token {
    
    [self retain];
	service_.serviceKey = LOGOUT_KEY;
	NSString * soapBody = [LoginRequestBuilder logoutForUserToken:token];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:LOGOUT_SOAP_ACTION];
}

- (void)forgotPasswordForUserWithEmail:(NSString *)email {
    
    [self retain];
	service_.serviceKey = FORGOT_PASSWORD_KEY;
	NSString * soapBody = [LoginRequestBuilder forgotPasswordForUserWithEmail:email];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:FORGOT_PASSWORD_SOAP_ACTION];
}

- (void)signUpForUser:(User *)user {
    
    [self retain];
	service_.serviceKey = SIGNUP_KEY;
	NSString * soapBody = [LoginRequestBuilder signUpRequestForUser:user];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:SIGNUP_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
	
    if ([responseData objectForKey:LOGIN_KEY]) {
    
        CBXML * parser = (CBXML *)[responseData objectForKey:LOGIN_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"LoginResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"IsValidLogin" parentElement:childeElement];
        TBXMLElement * token = [TBXML childElementNamed:@"Token" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
            
        if (errorOnFailure) {
            
            errorReason = [TBXML textForElement:errorOnFailure];
        }
          
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {				
            
            [Utility setUserToken:[TBXML textForElement:token]];
            
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFinishUserAuthenticationWithStatus:)]) {
                    
                    [self.authDelegate api:self didFinishUserAuthenticationWithStatus:YES];
                }
            }
        } else {
                        
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFailAuthWithError:)]) {
                    [self.authDelegate api:self didFailAuthWithError:errorReason];
                }
            }
        }
    } else if ([responseData objectForKey:LOGIN_AGAIN_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:LOGIN_AGAIN_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"LoginAgainResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"IsValidLogin" parentElement:childeElement];
        TBXMLElement * token = [TBXML childElementNamed:@"Token" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {				
            
            [Utility setUserToken:[TBXML textForElement:token]];
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFinishLoginAgainWithStatus:)]) {
                    
                    [self.authDelegate api:self didFinishLoginAgainWithStatus:YES];
                }
            }
        } else {
                        
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFailAuthWithError:)]) {
                    [self.authDelegate api:self didFailAuthWithError:errorReason];
                }
            }
        }
    } else if ([responseData objectForKey:LOGOUT_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:LOGOUT_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"LogoutResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {				
            
            [Utility removeUserToken];
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFinishLogoutWithStatus:)]) {
                    [self.authDelegate api:self didFinishLogoutWithStatus:YES];
                }
            }
        } else {
            
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFailAuthWithError:)]) {
                    [self.authDelegate api:self didFailAuthWithError:errorReason];
                }
            }
        }
    } else if ([responseData objectForKey:FORGOT_PASSWORD_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:FORGOT_PASSWORD_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"ForgetPasswordResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {				
            
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFinishForgotPasswordWithStatus:)]) {
                    [self.authDelegate api:self didFinishForgotPasswordWithStatus:YES];
                }
            }
        } else {
            
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFailAuthWithError:)]) {
                    [self.authDelegate api:self didFailAuthWithError:errorReason];
                }
            }
        }        
    } else if ([responseData objectForKey:SIGNUP_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:SIGNUP_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"SignupResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement * token = [TBXML childElementNamed:@"Token" parentElement:childeElement];
        //TBXMLElement * noOfReferral = [TBXML childElementNamed:@"NumberofReferral" parentElement:childeElement];
        
       // TBXMLElement * listEmails = [TBXML childElementNamed:@"ListEmails" parentElement:childeElement];
       // TBXMLElement *roleCode = roles->firstChild;

        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            
            
            [User activeUser].token = [TBXML textForElement:token];
            /*
            [User activeUser].numberOfReferral = [TBXML textForElement:noOfReferral];
            if ([[User activeUser].numberOfReferral intValue] > 0)
            {
                TBXMLElement * listEmails = [TBXML childElementNamed:@"ListEmails" parentElement:childeElement];
                TBXMLElement * emailItem = listEmails->firstChild;
            
                while (emailItem) {
                    //TBXMLElement * emailstring = [TBXML childElementNamed:@"string" parentElement:emailItem];
                    
                    NSLog(@"Child email string: %@", [TBXML textForElement:emailItem]);
                    //[[User activeUser].ListEmails addObject:[NSString stringWithFormat:@"%@",[TBXML textForElement:emailItem]]];
                    
                    if (![User activeUser].ListEmails) [User activeUser].ListEmails = [[NSMutableArray alloc] init];
                    
                    [[User activeUser].ListEmails addObject:[TBXML textForElement:emailItem]];
                
                
                    emailItem = emailItem->nextSibling;
                    //NSLog(@"ListEmails ---- %@",[User activeUser].ListEmails);
                }
            }
            
            NSLog(@"ListEmails ---- %@",[User activeUser].ListEmails);
            
             */
             
             
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFinishSignUpWithStatus:)]) {
                    [self.authDelegate api:self didFinishSignUpWithStatus:YES];
                }
            }
        } else {
            
            if (self.authDelegate) {
                
                if ([self.authDelegate respondsToSelector:@selector(api:didFailAuthWithError:)]) {
                    [self.authDelegate api:self didFailAuthWithError:errorReason];
                }
            }
        }
        
    } else {
        
        if (self.authDelegate) {
            if ([self.authDelegate respondsToSelector:@selector(api:didFailAuthWithError:)]) {
				
                [self.authDelegate api:self didFailAuthWithError:@"Unknown Error"];
            }
        }
    }
    
    [responseData release];
    [self release];
}
      

-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.authDelegate) {
		if ([self.authDelegate respondsToSelector:@selector(api:didFailAuthWithError:)]) {
			[self.authDelegate api:self didFailAuthWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.authDelegate = nil;
	[super dealloc];
}


@end
