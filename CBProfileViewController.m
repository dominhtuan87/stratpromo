//
//  CBProfileViewController.m
//  ClueBox
//
//  Created by Rakesh Raveendran on 17/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CBProfileViewController.h"
#import "Utility.h"
#import "Constant.h"
#import "User.h"

#import "GenericPickerViewController.h"
#import "IIViewDeckController.h"
#import "LeftViewController.h"

#import "CBChangePasswordViewController.h"

CGFloat animatedDistance;
int temp;
int movement;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;




@interface CBProfileViewController  (Private)


@end

@implementation CBProfileViewController


@synthesize webView;
@synthesize mscrollview;
@synthesize countries;

@synthesize activity1;

@synthesize CountryButton;
@synthesize GenderButton;
@synthesize MaritalStatusButton;
@synthesize EducationButton;
@synthesize OccupationButton;
@synthesize RaceButton;
@synthesize ReligionButton;
@synthesize DobButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"VIEWDIDLOAD countryNames: %@", countryNames);
    floor_.tag = 1;
    unit_.tag = 2;
    block_.tag = 3;
    building_.tag = 4;
    street_.tag = 5;
    postalCode_.tag = 6;
    country_.tag = 7;
    countryID_.tag = 107;
    contact_.tag = 8;
    
    name_.tag = 9;
    dob_.tag = 10;
    gender_.tag = 11;
    maritalStatus_.tag = 12;
    education_.tag = 13;
    occupation_.tag = 14;
    educationID_.tag = 113;
    occupationID_.tag = 114;
    race_.tag = 15;
    religion_.tag = 16;
    raceID_.tag = 115;
    religionID_.tag = 116;
    
    CountryButton.tag = 7;
    GenderButton.tag = 11;
    MaritalStatusButton.tag = 12;
    EducationButton.tag = 13;
    OccupationButton.tag = 14;
    RaceButton.tag = 15;
    ReligionButton.tag = 16;
    DobButton.tag  = 10;
    
    
    //self.webView.scrollView.bounces = NO;
    self.view.userInteractionEnabled = YES;
    self.viewDeckController.leftLedge = 220;
    
    mscrollview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    mscrollview.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
	mscrollview.contentSize = CGSizeMake(320, 840);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTap:)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [mscrollview addGestureRecognizer:tapGesture];
    
    //[tapGesture release];
    
    self.navigationItem.hidesBackButton = YES;
        
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"FirstLaunched"] isEqualToString:@"NO"])  {
        NSLog(@"SHOW");
        /*
         UIView *myView = [self view];
         CALayer *layer = [myView layer];
         [layer setRasterizationScale:0.2];
         [layer setShouldRasterize:YES];
         
         //self.swipeButton
         UIView *myView2 = self.swipeButton;
         CALayer *layer2 = [myView2 layer];
         [layer2 setRasterizationScale:1];
         [layer2 setShouldRasterize:NO];
         */
        
        
        [self swipeOverlay];
        
        // UIView *myView3 = [self view];
        // CALayer *layer3 = [myView3 layer];
        // [layer3 setRasterizationScale:1];
        // [layer3 setShouldRasterize:NO];
        
    }
}



- (void)swipeOverlay {
    NSLog(@"swipeOverlay !");
    if(transparentView_!=nil) {
        
        [transparentView_ removeFromSuperview];
        //[transparentView_ release];
        transparentView_ = nil;
    }
    
    
    transparentView_ = [[UIView alloc]init];
    transparentView_.tag = 7001;
	transparentView_.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.8];
	
    
    UIImageView * swipeIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"drag_btn.png"]];
    /*if ([SCUtility isCurrentDeviceiPad]) {
     
     transparentView_.frame = CGRectMake(0, 0, 1024, 768);
     swipeIconView.frame = CGRectMake(0, 0, 66, 114);
     swipeIconView.center =  CGPointMake(card_.frame.origin.x*2+card_.iconOffset.x*2+165, card_.frame.origin.y*2+card_.iconOffset.y*2+200);
     } else {
     */
    //transparentView_.frame = CGRectMake(0,0, self.view.frame.size.width,420);
    transparentView_.frame = CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height+200);
    swipeIconView.frame = CGRectMake(0, 0, 94, 70);
    /*
     if ([SCUtility isCurrentDeviceiPhone5]) {
     
     swipeIconView.center =  CGPointMake(card_.frame.origin.x+card_.iconOffset.x+106, card_.frame.origin.y+card_.iconOffset.y+67);
     
     } else {*/
    
    swipeIconView.center =  CGPointMake(self.swipeButton.frame.origin.x+70, self.swipeButton.frame.origin.y+27);
    // }
    
    //}
    
    [transparentView_ addSubview:swipeIconView];
    transparentView_.userInteractionEnabled = YES;
	[self.view insertSubview:transparentView_ belowSubview:self.swipeButton];
    //[swipeIconView release];
}


-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    NSLog(@"Transparent touch!");
    if(transparentView_!=nil) {
        [[NSUserDefaults standardUserDefaults]setValue:@"NO" forKey:@"FirstLaunched"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [transparentView_ removeFromSuperview];
        //[transparentView_ release];
        
        //UIView *myView = [self view];
        //CALayer *layer = [myView layer];
        //[layer setRasterizationScale:1];
        //[layer setShouldRasterize:NO];
        
        transparentView_ = nil;
    }
}
- (void)viewWillAppear:(BOOL)animated{
    
    //[super viewDidAppear:animated];
    //self.navigationItem.hidesBackButton = YES;
    [self getProfileAction];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    //self.navigationItem.hidesBackButton = YES;
    [self addBackButton];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [backButton_ removeFromSuperview];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    //[backButton_ removeFromSuperview];
}

- (void)addBackButton {
 
    UIImage *image = [UIImage imageNamed: @"logo.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    self.navigationItem.titleView = imageView;

    
    
    UIImage *buttonImage = [UIImage imageNamed:@"home_btn_black.png"];
    backButton_ = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton_ setImage:buttonImage forState:UIControlStateNormal];
    [backButton_ setFrame:CGRectMake(20,(self.navigationController.navigationBar.frame.size.height-40)/2+4, 55, 30)];
    [backButton_ addTarget:self action:@selector(goToHomeScreen) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:backButton_];
}

- (void)goToHomeScreen {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)swipeButtonPressed:(id)sender {
    if(transparentView_!=nil) {
        [[NSUserDefaults standardUserDefaults]setValue:@"NO" forKey:@"FirstLaunched"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [transparentView_ removeFromSuperview];
        //[transparentView_ release];
        
        //UIView *myView = [self view];
        //CALayer *layer = [myView layer];
        //[layer setRasterizationScale:1];
        //[layer setShouldRasterize:NO];
        
        transparentView_ = nil;
    }

    [self.viewDeckController toggleLeftView];
}


- (void)getProfileAction {
    //NSLog(@"START GETPROFILE countryNames: %@", countryNames);
    //NSLog(@"Getting Profile from server");
    [activity1 presentActivityView:PROFILE_LOADING];
    GetProfileAPI *getProfileAPI = [[GetProfileAPI alloc] init];
    getProfileAPI.GetProfileDelegate = self;
    //NSLog(@"... Continue ...");
    [getProfileAPI getProfileWithToken:[Utility getUserToken]];
    NSLog(@"UserToken: %@", [Utility getUserToken]);
    //[getProfileAPI release];
}



-(void)api:(id)sender didFinishGetProfileWithStatus:(BOOL)status {
    
    
    
    //NSLog(@"countryNames: %@", countryNames);
    
    //NSLog(@"FINISH countryNames: %@", countryNames);
    
    
    [activity1 dismissActivityView];
    floor_.text = [NSString stringWithFormat:@"%@",[User activeUser].floor];
    unit_.text = [NSString stringWithFormat:@"%@",[User activeUser].unit];
    block_.text = [NSString stringWithFormat:@"%@",[User activeUser].block];
    building_.text = [NSString stringWithFormat:@"%@",[User activeUser].building];
    street_.text = [NSString stringWithFormat:@"%@",[User activeUser].street];
    postalCode_.text = [NSString stringWithFormat:@"%@",[User activeUser].postalCode];
    
    /* COUNTRY */
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Countries" ofType:@"plist"];
    NSMutableDictionary *codesByName = [NSMutableDictionary dictionary];
    if ([[User activeUser].country intValue]==0) {
        country_.text = @"";
    }
    else {
        
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        
        for (NSString *code in [countryNamesByCode allKeys])
        {
            [codesByName setObject:code forKey:[countryNamesByCode objectForKey:code]];
        }
        countryCodesByName = [codesByName copy];
        
        NSArray *names = [countryNamesByCode allValues];
        countries = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        country_.text = [countries objectAtIndex:[[User activeUser].country intValue]-1];
    }
    
    contact_.text = [NSString stringWithFormat:@"%@",[User activeUser].contact];
    
    name_.text = [NSString stringWithFormat:@"%@",[User activeUser].name];
    dob_.text = [NSString stringWithFormat:@"%@",[User activeUser].dob];
    gender_.text = [NSString stringWithFormat:@"%@",[User activeUser].gender];
    maritalStatus_.text = [NSString stringWithFormat:@"%@",[User activeUser].maritalStatus];
    
    if ([[User activeUser].education intValue] == 0) {
        education_.text = @"";
        
    }
    else{
    
        path = [[NSBundle mainBundle] pathForResource:@"Educations" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        codesByName = [NSMutableDictionary dictionary];
        for (NSString *code in [countryNamesByCode allKeys])
        {
            [codesByName setObject:code forKey:[countryNamesByCode objectForKey:code]];
        }
        countryCodesByName = [codesByName copy];
        
        NSArray *names = [countryNamesByCode allValues];
        countries = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"DATA: %@", countries);
        education_.text = [countries objectAtIndex:[[User activeUser].education intValue]-1];

        
    }
    
    
    if ([[User activeUser].occupation intValue] == 0) {
        occupation_.text = @"";
        
    }
    else{
        path = [[NSBundle mainBundle] pathForResource:@"Occupations" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        codesByName = [NSMutableDictionary dictionary];
        for (NSString *code in [countryNamesByCode allKeys])
        {
            [codesByName setObject:code forKey:[countryNamesByCode objectForKey:code]];
        }
        countryCodesByName = [codesByName copy];
        
        NSArray *names = [countryNamesByCode allValues];
        countries = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"DATA: %@", countries);
        occupation_.text = [countries objectAtIndex:[[User activeUser].occupation intValue]-1];
    }
    
    if ([[User activeUser].race intValue] == 0) {
        race_.text = @"";
        
    }
    else{
        path = [[NSBundle mainBundle] pathForResource:@"Races" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        codesByName = [NSMutableDictionary dictionary];
        for (NSString *code in [countryNamesByCode allKeys])
        {
            [codesByName setObject:code forKey:[countryNamesByCode objectForKey:code]];
        }
        countryCodesByName = [codesByName copy];
        
        NSArray *names = [countryNamesByCode allValues];
        countries = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"DATA: %@", countries);
        race_.text = [countries objectAtIndex:[[User activeUser].race intValue]-1];
    }
    
    if ([[User activeUser].religion intValue] == 0) {
        religion_.text = @"";
        
    }
    else{
        path = [[NSBundle mainBundle] pathForResource:@"Religions" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        codesByName = [NSMutableDictionary dictionary];
        for (NSString *code in [countryNamesByCode allKeys])
        {
            [codesByName setObject:code forKey:[countryNamesByCode objectForKey:code]];
        }
        countryCodesByName = [codesByName copy];
        
        NSArray *names = [countryNamesByCode allValues];
        countries = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"DATA: %@", countries);
        religion_.text = [countries objectAtIndex:[[User activeUser].religion intValue]-1];
    }
    
}

-(void)api:(id)sender didFailGetProfileWithError:(NSString *)error {
    
    [activity1 dismissActivityView];
    
    //if(isFromLogin) {
        [Utility showAlertViewWithMessage:error];
    //} else {
    //    [self reloadSyncedItems];
    //}
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    //CBLoginViewController *loginViewController = [[CBLoginViewController alloc] initWithNibName:@"CBLoginViewController" bundle:nil];
    //[self.navigationController pushViewController:loginViewController animated:YES];
    //[loginViewController release];
}




- (IBAction)UpdateProfileAction:(id)sender {
    //NSLog(@"START GETPROFILE countryNames: %@", countryNames);
    //NSLog(@"Updating Profile to server");
    [activity1 presentActivityView:UPDATE_PROFILE_LOADING];
    UpdateProfileAPI *updateProfile = [[UpdateProfileAPI alloc] init];
    updateProfile.UpdateProfileAPIDelegate = self;
    //NSLog(@"... Continue ...");
    
    User *tempUser = [[User alloc] init];
    tempUser.floor = floor_.text;
    tempUser.unit =unit_.text;
    tempUser.block =block_.text;
    tempUser.building =building_.text;
    tempUser.street =street_.text;
    tempUser.postalCode =postalCode_.text;
    if ([countryID_.text length]) {
        tempUser.country =countryID_.text;
    }
    else
        tempUser.country = [User activeUser].country;
    tempUser.contact =contact_.text;
    
    tempUser.name =name_.text;
    tempUser.dob =dob_.text;
    tempUser.gender =gender_.text;
    tempUser.maritalStatus =maritalStatus_.text;
    
    if ([educationID_.text length]) {
        tempUser.education = educationID_.text;
    }
    else
        tempUser.education = [User activeUser].education;
    
    if ([occupationID_.text length]) {
        tempUser.occupation = occupationID_.text;
    }
    else
        tempUser.occupation = [User activeUser].occupation;;
    
    
    if ([raceID_.text length]) {
        tempUser.race = raceID_.text;
    }
    else
        tempUser.race = [User activeUser].race;
    
    if ([religionID_.text length]) {
        tempUser.religion = religionID_.text;
    }
    else
        tempUser.religion = [User activeUser].religion;

    
    [updateProfile UpdateProfileOfUser:tempUser withToken:[Utility getUserToken]];
    
    //[updateProfile release];
    //[tempUser release];
}


-(void)api:(id)sender didFinishUpdateProfileWithStatus:(BOOL)status {
    
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:@"Your Profile is Updated" withDelegate:self];
    
    [[NSUserDefaults standardUserDefaults]setValue:@"NO" forKey:@"FirstLogin"];
    
    [self getProfileAction ];

}

-(void)api:(id)sender didFailUpdateProfileWithError:(NSString*)error {
    
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:error];
}


-(IBAction)pickerButtonClicked:(UIButton*)sender {
    
    NSLog(@"Button Clicked: %i", sender.tag);
    
    
    for(int i =1 ;i<= 16;i++) {
		
		UITextField * textField = (UITextField *) [self.mscrollview viewWithTag:i];
		if([textField isFirstResponder]) {
			[textField resignFirstResponder];
            
			
		}
	}
    NSLog(@"AfterResign Responder");
    
    
    //NSLog(@"Button pressed: %i", sender.tag);
    
    if (sender.tag==7) {
        
        NSLog(@"Managed get in if");
        
        NSArray *dataSource;
        
        
		
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Countries" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        
        NSArray *names = [countryNamesByCode allValues];
        dataSource = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        NSLog(@"Openning dataSource: %@",dataSource);
        
        
        GenericPickerViewController *pickerViewController = [[GenericPickerViewController alloc] initWithDataSource:dataSource andDelegate:self];
        pickerViewController.pickerTag = sender.tag;
        pickerViewController.delegate = self;
        
	    
        pickerViewController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:pickerViewController.view];
    }
    
    else if (sender.tag==11) {
        NSArray *dataSource;
		
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Genders" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        
        NSArray *names = [countryNamesByCode allValues];
        dataSource = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"country: %@", dataSource);
        
        
        GenericPickerViewController *pickerViewController = [[GenericPickerViewController alloc] initWithDataSource:dataSource andDelegate:self];
        pickerViewController.pickerTag = sender.tag;
        pickerViewController.delegate = self;
        
	    
        pickerViewController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:pickerViewController.view];
    }
	
    else if (sender.tag==12) {
        NSArray *dataSource;
		
        NSString *path = [[NSBundle mainBundle] pathForResource:@"MaritalStatus" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        
        NSArray *names = [countryNamesByCode allValues];
        dataSource = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        NSLog(@"MaritalStatus: %@", dataSource);
        
        
        GenericPickerViewController *pickerViewController = [[GenericPickerViewController alloc] initWithDataSource:dataSource andDelegate:self];
        pickerViewController.pickerTag = sender.tag;
        pickerViewController.delegate = self;
        
	    
        pickerViewController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:pickerViewController.view];
        [UIView commitAnimations];
    }
    
    else if (sender.tag==13) {
        NSArray *dataSource;
		
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Educations" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        
        NSArray *names = [countryNamesByCode allValues];
        dataSource = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"country: %@", dataSource);
        
        
        GenericPickerViewController *pickerViewController = [[GenericPickerViewController alloc] initWithDataSource:dataSource andDelegate:self];
        pickerViewController.pickerTag = sender.tag;
        pickerViewController.delegate = self;
        
	    
        pickerViewController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:pickerViewController.view];
    }
    
    else if (sender.tag==14) {
        NSArray *dataSource;
		
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Occupations" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        
        NSArray *names = [countryNamesByCode allValues];
        dataSource = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"country: %@", dataSource);
        
        
        GenericPickerViewController *pickerViewController = [[GenericPickerViewController alloc] initWithDataSource:dataSource andDelegate:self];
        pickerViewController.pickerTag = sender.tag;
        pickerViewController.delegate = self;
        
	    
        pickerViewController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:pickerViewController.view];
    }
    
    else if (sender.tag==15) {
        NSArray *dataSource;
		
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Races" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        
        NSArray *names = [countryNamesByCode allValues];
        dataSource = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"country: %@", dataSource);
        
        
        GenericPickerViewController *pickerViewController = [[GenericPickerViewController alloc] initWithDataSource:dataSource andDelegate:self];
        pickerViewController.pickerTag = sender.tag;
        pickerViewController.delegate = self;
        
	    
        pickerViewController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:pickerViewController.view];
    }
    
    else if (sender.tag==16) {
        NSArray *dataSource;
		
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Religions" ofType:@"plist"];
        countryNamesByCode = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        
        NSArray *names = [countryNamesByCode allValues];
        dataSource = [names sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        //NSLog(@"country: %@", dataSource);
        
        
        GenericPickerViewController *pickerViewController = [[GenericPickerViewController alloc] initWithDataSource:dataSource andDelegate:self];
        pickerViewController.pickerTag = sender.tag;
        pickerViewController.delegate = self;
        
	    
        pickerViewController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:pickerViewController.view];
    }
    
    else if (sender.tag==10) {
        
		
        DatePickerViewController *datePickerController = [[DatePickerViewController alloc] init];
        datePickerController.pickerTag = sender.tag;
        datePickerController.datePickerDelegate = self;
        
       // NSLog(@"Button Clicked: %@", datePickerController);
        
	    
        datePickerController.view.frame = CGRectMake(0,160, 320, 257);
        [self.view addSubview:datePickerController.view];
    }
    
	
}

- (void)pickerViewCancelButtonDidPressed: (id)sender{
    NSLog(@"Cancewl button pressed");
    //[self.view removeFromSuperview];
}

- (void)pickerViewDoneButtonDidPressed: (id)sender withIndex:(int)index andTag:(int)tag{
    NSLog(@"DONE button pressed");
    //[self.view removeFromSuperview];
    //NSLog(@"CHOICE: %@", sender);
    //NSLog(@"index: %i", index);
    //NSLog(@"tag: %i", tag);
    UITextField * textField = (UITextField *) [self.mscrollview viewWithTag:tag];
    textField.text = sender;
    textField = (UITextField *) [self.mscrollview viewWithTag:(tag+100)];
    textField.text = [NSString stringWithFormat:@"%d", index];
    
    //NSLog(@"ID: %@", textField.text);
}


- (void)DatePickerViewCancelButtonDidPressed: (id)sender{
    NSLog(@"Cancewl button pressed");
    //[self.view removeFromSuperview];
}

- (void)DatePickerViewDoneButtonDidPressed:(NSDate *)date andTag:(int)tag{
    NSLog(@"DONE button pressed");
    
    NSLog(@"Chosen Date: %@", date);
    NSLog(@"Chosen TAG: %i", tag);
    
    
    //[self.view removeFromSuperview];
    //NSLog(@"CHOICE: %@", sender);
    //NSLog(@"index: %i", index);
    //NSLog(@"tag: %i", tag);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    UITextField * textField = (UITextField *) [self.mscrollview viewWithTag:tag];
    textField.text = [formatter stringFromDate:date];
    textField = (UITextField *) [self.mscrollview viewWithTag:(tag+100)];
    
    //[formatter release];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	
    //NSString *urlString = [request.URL absoluteString];
	//NSLog(@"%@",urlString);
    
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    CGRect textFieldRect =
    [self.mscrollview.window convertRect:textField.bounds fromView:textField];
    int movementDistance;
    
    if (up) {
        movementDistance = textFieldRect.origin.y - 190; // tweak as needed
    }
    else
        movementDistance = temp; // tweak as needed
    
    temp = movementDistance;
    
    const float movementDuration = 0.3f; // tweak as needed
    
    NSLog(@"temp1: %i", temp);
    
    
    movement = (up ? (0-movementDistance) : (0+movementDistance));
    
    NSLog(@"movement: %i", movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.mscrollview.frame = CGRectOffset(self.mscrollview.frame, 0, movement);
    
    [UIView commitAnimations];
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.webView = nil;
    self.mscrollview = nil;
}

- (void)dealloc {
	self.webView = nil;
    self.mscrollview = nil;
    //[super dealloc];
}

-(IBAction)backgroundTap:(id)sender {
    
    for(int i =1 ;i<= 16;i++) {
		
		UITextField * textField = (UITextField *) [self.mscrollview viewWithTag:i];
		if([textField isFirstResponder]) {
			[textField resignFirstResponder];
			
		}
	}
}

- (IBAction)ChangePasswordButtonPressed:(id)sender {
    
    CBChangePasswordViewController *changePasswordViewController = [[CBChangePasswordViewController alloc] initWithNibName:@"CBChangePasswordViewController" bundle:nil];
    LeftViewController* leftController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:changePasswordViewController leftViewController:leftController];
    deckController.rightLedge = 100;
    
    
    [self.navigationController pushViewController:deckController animated:YES];
    //[changePasswordViewController release];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
