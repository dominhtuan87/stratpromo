//
//  ReferalAPI.h
//  ClueBox
//
//  Created by Rakesh Raveendran on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SOAPWebService.h"
#import "User.h"

@protocol ReferalAPIDelegate <NSObject>

@optional

-(void)api:(id)sender didFinishConfirmReferalWithStatus:(BOOL)status;
-(void)api:(id)sender didFailConfirmReferalWithError:(NSString*)error;

@end

@interface ReferalAPI : NSObject <SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <ReferalAPIDelegate> * referalDelegate_;
}

@property(nonatomic,assign) NSObject <ReferalAPIDelegate> * referalDelegate;

- (void)confirmReferalForUserToken:(NSString *)token withEmail:(NSString *)email;

@end
