//
//  UpdateProfileRequestBuilder.m
//  ClueBox
//
//  Created by cat on 28/11/12.
//
//

#import "UpdateProfileRequestBuilder.h"
#import "Utility.h"


@implementation UpdateProfileRequestBuilder

+(NSString*)UpdateProfileForUser:(User*)user withToken:(NSString*)token{
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"UpdateProfile" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__"
																 withString:token];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Floor__"
																 withString:[Utility absoluteValueForString:user.floor]];
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Block__"
																 withString:[Utility absoluteValueForString:user.block]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Building__"
																 withString:[Utility absoluteValueForString:user.building]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__ContactNo__"
																 withString:[Utility absoluteValueForString:user.contact]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__DOB__"
																 withString:[Utility absoluteValueForString:user.dob]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Name__"
																 withString:[Utility absoluteValueForString:user.name]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__PostCode__"
																 withString:[Utility absoluteValueForString:user.postalCode]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Street__"
																 withString:[Utility absoluteValueForString:user.street]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Unit__"
																 withString:[Utility absoluteValueForString:user.unit]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__CountryID__"
																 withString:[Utility absoluteValueForString:user.country]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Gender__"
																 withString:[Utility absoluteValueForString:user.gender]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__MaritalStatus__"
																 withString:[Utility absoluteValueForString:user.maritalStatus]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__EducationID__"
																 withString:[Utility absoluteValueForString:user.education]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__OccupationID__"
																 withString:[Utility absoluteValueForString:user.occupation]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__RaceID__"
																 withString:[Utility absoluteValueForString:user.race]];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__ReligionID__"
																 withString:[Utility absoluteValueForString:user.religion]];
        
		
        return contentFormat;
	}
	
	return @"";
}

@end
