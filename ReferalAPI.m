//
//  ReferalAPI.m
//  ClueBox
//
//  Created by Rakesh Raveendran on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ReferalAPI.h"
#import "Constant.h"
#import "ConfirmReferalRequestBuilder.h"
#import "CBXML.h"
@implementation ReferalAPI

@synthesize referalDelegate = referalDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}

- (void)confirmReferalForUserToken:(NSString *)token withEmail:(NSString *)email {
    
    [self retain];
	service_.serviceKey = CONFIRM_REFERAL_KEY;
	NSString * soapBody = [ConfirmReferalRequestBuilder confirmReferalForUserToken:token withEmail:email];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:CONFIRM_REFERAL_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
    
    if ([responseData objectForKey:CONFIRM_REFERAL_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:CONFIRM_REFERAL_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"ConfirmReferralResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
                        
            if (self.referalDelegate) {
                
                if ([self.referalDelegate respondsToSelector:@selector(api:didFinishConfirmReferalWithStatus:)]) {
                    
                    [self.referalDelegate api:self didFinishConfirmReferalWithStatus:YES];
                }
            }
        } else {
            
            if (self.referalDelegate) {
                
                if ([self.referalDelegate respondsToSelector:@selector(api:didFailConfirmReferalWithError:)]) {
                    [self.referalDelegate api:self didFailConfirmReferalWithError:errorReason];
                }
            }
        }
        
    } else {
            
            if (self.referalDelegate) {
                if ([self.referalDelegate respondsToSelector:@selector(api:didFailConfirmReferalWithError:)]) {
                    
                    [self.referalDelegate api:self didFailConfirmReferalWithError:@"Unknown Error"];
                }
            }
        }


    
    [responseData release];
    [self release];
}

-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.referalDelegate) {
		if ([self.referalDelegate respondsToSelector:@selector(api:didFailAuthWithError:)]) {
			[self.referalDelegate api:self didFailConfirmReferalWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.referalDelegate = nil;
	[super dealloc];
}

@end
