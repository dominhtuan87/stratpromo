//
//  UpdateProfileAPI.m
//  ClueBox
//
//  Created by cat on 28/11/12.
//
//

#import "UpdateProfileAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "UpdateProfileRequestBuilder.h"

@implementation UpdateProfileAPI

@synthesize UpdateProfileAPIDelegate = UpdateProfileDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}

- (void)UpdateProfileOfUser:(User *)user withToken:(NSString*)token{
    
    [self retain];
	service_.serviceKey = UPDATE_PROFILE_KEY;
	NSString * soapBody = [UpdateProfileRequestBuilder UpdateProfileForUser:user withToken:token];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:UPDATE_PROFILE_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
    
    if ([responseData objectForKey:UPDATE_PROFILE_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:UPDATE_PROFILE_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"UpdateProfileResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            
            if (self.UpdateProfileAPIDelegate) {
                
                if ([self.UpdateProfileAPIDelegate respondsToSelector:@selector(api:didFinishUpdateProfileWithStatus:)]) {
                    
                    [self.UpdateProfileAPIDelegate api:self didFinishUpdateProfileWithStatus:YES];
                }
            }
        } else {
            
            if (self.UpdateProfileAPIDelegate) {
                
                if ([self.UpdateProfileAPIDelegate respondsToSelector:@selector(api:didFailUpdateProfileWithError:)]) {
                    [self.UpdateProfileAPIDelegate api:self didFailUpdateProfileWithError:errorReason];
                }
            }
        }
        
    } else {
        
        if (self.UpdateProfileAPIDelegate) {
            if ([self.UpdateProfileAPIDelegate respondsToSelector:@selector(api:didFailUpdateProfileWithError:)]) {
                
                [self.UpdateProfileAPIDelegate api:self didFailUpdateProfileWithError:@"Unknown Error"];
            }
        }
    }
    
    
    
    [responseData release];
    [self release];
}


-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.UpdateProfileAPIDelegate) {
		if ([self.UpdateProfileAPIDelegate respondsToSelector:@selector(api:didFailUpdateProfileWithError:)]) {
			[self.UpdateProfileAPIDelegate api:self didFailUpdateProfileWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.UpdateProfileAPIDelegate = nil;
	[super dealloc];
}



@end
