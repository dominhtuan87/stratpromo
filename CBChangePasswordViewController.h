//
//  CBChangePasswordViewController.h
//  ClueBox
//
//  Created by cat on 29/11/12.
//
//

#import <UIKit/UIKit.h>

#import "ResetPasswordAPI.h"
#import "ActivityView.h"

@interface CBChangePasswordViewController : UIViewController<ResetPasswordAPIDelegate,UITextFieldDelegate>{
    UIButton *backButton_;
    
    IBOutlet UITextField    *oldPassword_;
    IBOutlet UITextField    *newpassword_;
    IBOutlet UITextField    *confirmnewpassword_;
    
}
- (IBAction)swipeButtonPressed:(id)sender;
- (IBAction)submitButtonPressed:(id)sender;
- (IBAction)backgroundTap:(id)sender;


@property (strong, nonatomic) ActivityView* activity1;

@end
