//
//  CBConfirmReferralViewController.m
//  ClueBox
//
//  Created by cat on 23/11/12.
//
//

#import "CBConfirmReferralViewController.h"

@interface CBConfirmReferralViewController ()

@end

@implementation CBConfirmReferralViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
