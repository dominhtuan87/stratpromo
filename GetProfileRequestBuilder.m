//
//  GetProfileRequestBuilder.m
//  ClueBox
//
//  Created by cat on 27/11/12.
//
//

#import "GetProfileRequestBuilder.h"
#import "Utility.h"


@implementation GetProfileRequestBuilder

+(NSString*)getProfileForUserToken:(NSString *)token{
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"GetProfile" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__"
																 withString:token];
		
				return contentFormat;
	}
	
	return @"";
}

@end
