//
//  ResetPasswordAPI.m
//  ClueBox
//
//  Created by cat on 29/11/12.
//
//

#import "ResetPasswordAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "ResetPasswordRequestBuilder.h"


@implementation ResetPasswordAPI

@synthesize ResetPasswordAPIDelegate = ResetPasswordAPIDelegate_;


- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}

- (void)ResetPasswordWithToken:(NSString*)token withNewPassword:(NSString*)newPassword replaceOldPassword:(NSString*)oldPassword{
    
    [self retain];
	service_.serviceKey = RESET_PASSWORD_KEY;
	NSString * soapBody = [ResetPasswordRequestBuilder ResetPasswordWithToken:token withNewPassword:newPassword replaceOldPassword:oldPassword];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:RESET_PASSWORD_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
    
    if ([responseData objectForKey:RESET_PASSWORD_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:RESET_PASSWORD_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"resetPasswordResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            
            if (self.ResetPasswordAPIDelegate) {
                
                if ([self.ResetPasswordAPIDelegate respondsToSelector:@selector(api:didFinishResetPasswordWithStatus:)]) {
                    
                    [self.ResetPasswordAPIDelegate api:self didFinishResetPasswordWithStatus:YES];
                }
            }
        } else {
            
            if (self.ResetPasswordAPIDelegate) {
                
                if ([self.ResetPasswordAPIDelegate respondsToSelector:@selector(api:didFailResetPasswordWithError:)]) {
                    [self.ResetPasswordAPIDelegate api:self didFailResetPasswordWithError:errorReason];
                }
            }
        }
        
    } else {
        
        if (self.ResetPasswordAPIDelegate) {
            if ([self.ResetPasswordAPIDelegate respondsToSelector:@selector(api:didFailResetPasswordWithError:)]) {
                
                [self.ResetPasswordAPIDelegate api:self didFailResetPasswordWithError:@"Unknown Error"];
            }
        }
    }
    
    
    
    [responseData release];
    [self release];
}

-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    NSLog(@"requestFailedWithError");
    
    if (self.ResetPasswordAPIDelegate) {
		if ([self.ResetPasswordAPIDelegate respondsToSelector:@selector(api:didFailResetPasswordWithError:)]) {
			[self.ResetPasswordAPIDelegate api:self didFailResetPasswordWithError:error];
		}
	}
	[self release];
    
    
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.ResetPasswordAPIDelegate = nil;
	[super dealloc];
}


@end
