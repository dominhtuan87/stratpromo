//
//  GenericPickerViewController.h
//  ClueBox
//
//  Created by cat on 28/11/12.
//
//

#import <UIKit/UIKit.h>


@protocol GenericPickerDelegate <NSObject>

@optional

- (void )pickerView:(UIPickerView *)pickerView titleForRow:(NSString *)title atRow:(NSInteger )row;
- (void)pickerViewDoneButtonDidPressed: (id)sender withIndex:(int)index andTag:(int)tag;
- (void)pickerViewCancelButtonDidPressed: (id)sender;
//- (void)setUI;

@end




@interface GenericPickerViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>{
    UIPickerView * pickerView_;
    NSString * selectedItem_;
	
    NSArray * pickerDataSource_;
    UIBarButtonItem *cancelButton;
    
    UIToolbar *toolbar;
    UILabel *label;
    
	BOOL	optional_;
}

@property(nonatomic,retain) IBOutlet UIPickerView * pickerView;
@property(nonatomic,assign) NSObject <GenericPickerDelegate> * delegate;
@property (nonatomic) NSInteger pickerTag;
@property (nonatomic,retain) IBOutlet UIBarButtonItem * cancelButton;
@property (nonatomic,retain) IBOutlet UIToolbar *toolbar;
@property (nonatomic,retain) IBOutlet UILabel *label;

-(IBAction)cancelButtonPressed:(id)sender;
-(IBAction)doneButtonPressed:(id)sender;

@property (nonatomic,assign) BOOL optional;
@property (nonatomic,copy) NSString * selectedItem;

- (id)initWithDataSource:(NSArray *)dataSource andDelegate:(NSObject *)delegate;
- (void)setUI;

@end
