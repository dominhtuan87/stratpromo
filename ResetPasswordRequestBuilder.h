//
//  ResetPasswordRequestBuilder.h
//  ClueBox
//
//  Created by cat on 29/11/12.
//
//

#import <UIKit/UIKit.h>

@interface ResetPasswordRequestBuilder : NSObject{
    
}

+(NSString*)ResetPasswordWithToken:(NSString*)token withNewPassword:(NSString*)newPassword replaceOldPassword:(NSString*)oldPassword;


@end
