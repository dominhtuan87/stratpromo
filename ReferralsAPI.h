//
//  ReferralsAPI.h
//  ClueBox
//
//  Created by cat on 5/12/12.
//
//

#import <UIKit/UIKit.h>
#import "SOAPWebService.h"


@protocol ReferralsAPIDelegate <NSObject>
@optional

-(void)api:(id)sender didFinishReferralsWithStatus:(BOOL)status andMessage:(NSString*)error;
-(void)api:(id)sender didFailReferralsWithError:(NSString*)error;

@end




@interface ReferralsAPI : NSObject <SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <ReferralsAPIDelegate> * ReferralsAPIDelegate_;
}


@property(nonatomic,assign) NSObject <ReferralsAPIDelegate> * ReferralsAPIDelegate;

- (void)ReferralsWithToken:(NSString*)token withFriends:(NSString*)Friends;

@end
