//
//  CBSplashViewController.h
//  ClueBox
//
//  Created by user on 10/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginAPI.h"
#import "ActivityView.h"

//#import "CBStartScreenViewController.h"

@interface CBLoginViewController : UIViewController <LoginAPIDelegate,UITextFieldDelegate> {
    int verticalOffset;
    BOOL isSelected;
    //CBStartScreenViewController *startScreen_;
    
    IBOutlet UITextField    *userName_;
    IBOutlet UITextField    *password_;
    IBOutlet UIButton       *rememberButton_;
    
    UITextField *activeTextField;
    
    IBOutlet UIScrollView *theScrollView;
}
- (IBAction)loginButtonPressed:(id)sender;
- (IBAction)backgroundTap:(id)sender;
- (IBAction)forgetPassword:(id)sender;
- (IBAction)registerButtonPressed:(id)sender;
- (IBAction)rememberButtonPressed:(id)sender;
- (IBAction)readMoreButtonPressed:(id)sender;
- (IBAction)videoButtonPressed:(id)sender;
- (IBAction)logoutButtonPressed:(id)sender;

@property (strong, nonatomic) ActivityView* activity1;
@property (strong, nonatomic) UITextField *activeTextField;

@end
