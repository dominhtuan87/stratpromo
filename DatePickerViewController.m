//
//  DatePickerViewController.m
//  ClueBox
//
//  Created by cat on 28/11/12.
//
//

#import "DatePickerViewController.h"

@interface DatePickerViewController ()

@end

@implementation DatePickerViewController

@synthesize datePickerDelegate = datePickerDelegate_;
@synthesize DatePickerView = datePickerView_;
@synthesize pickerTag;


-(IBAction)doneButtonPressed:(id)sender {
	
	
	
    if ([self.datePickerDelegate  respondsToSelector:@selector(DatePickerViewDoneButtonDidPressed:andTag:)])
    {
        [self.datePickerDelegate  DatePickerViewDoneButtonDidPressed:self.DatePickerView.date andTag:self.pickerTag];
       
        [self.view removeFromSuperview];
        
    }
	
    
    NSLog(@"DATE PICKED: %@",self.DatePickerView.date);
}


-(IBAction)datePickerValueChanged:(id)sender {
	
	UIDatePicker * picker = (UIDatePicker *)sender;
	
	
	if([self.datePickerDelegate respondsToSelector:@selector(dateValueChanged:forPickerWithTag:)]) {
		[self.datePickerDelegate dateValueChanged:picker.date forPickerWithTag:self.pickerTag];
	}
}


- (IBAction)cancelButtonPressed:(id)sender {
    
    /*if ([self.datePickerDelegate respondsToSelector:@selector(DatePickerViewCancelButtonDidPressed:)]) {
        [self.datePickerDelegate DatePickerViewCancelButtonDidPressed:self.pickerTag];
		
    }
    */
    NSLog(@"CANCEL PRESSED BEFORE DELEGATE");
    
    NSLog(@"self.delegate: %@", self.datePickerDelegate);
    
	if (self.datePickerDelegate) {
        NSLog(@"CANCEL PRESSED IN IF 1 DELEGATE");
		if ([self.datePickerDelegate respondsToSelector:@selector(pickerViewCancelButtonDidPressed:)]) {
            NSLog(@"CANCEL PRESSED IN IF 2 DELEGATE");
			[self.datePickerDelegate DatePickerViewCancelButtonDidPressed:self];
            [self.view removeFromSuperview];
            
		}
	}
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        NSLog(@"CALLING DATE NIB");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.view = nil;
    //self.mscrollview = nil;
}

- (void)dealloc {
	self.view = nil;
    //self.mscrollview = nil;
    [super dealloc];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
