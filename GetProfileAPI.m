//
//  GetProfileAPI.m
//  ClueBox
//
//  Created by cat on 27/11/12.
//
//

#import "GetProfileAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "GetProfileRequestBuilder.h"

@implementation GetProfileAPI

@synthesize GetProfileDelegate = GetProfileDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}


- (void)getProfileWithToken:(NSString *)token {
    
    [self retain];
	service_.serviceKey = GET_PROFILE_KEY;
	NSString * soapBody = [GetProfileRequestBuilder getProfileForUserToken:token];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:GET_PROFILE_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
	
    if ([responseData objectForKey:GET_PROFILE_KEY]) {
        
        
        CBXML * parser = (CBXML *)[responseData objectForKey:GET_PROFILE_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"GetProfileResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        if (errorOnFailure) {
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        
        TBXMLElement * IsFirstTime = [TBXML childElementNamed:@"IsFirstTime" parentElement:childeElement];
        //TBXMLElement * UserID = [TBXML childElementNamed:@"UserID" parentElement:childeElement];
        
        TBXMLElement * Floor = [TBXML childElementNamed:@"Floor" parentElement:childeElement];
        TBXMLElement * Unit = [TBXML childElementNamed:@"Unit" parentElement:childeElement];
        TBXMLElement * Block = [TBXML childElementNamed:@"Block" parentElement:childeElement];
        TBXMLElement * Building = [TBXML childElementNamed:@"Building" parentElement:childeElement];
        TBXMLElement * Street = [TBXML childElementNamed:@"Street" parentElement:childeElement];
        TBXMLElement * PostCode = [TBXML childElementNamed:@"PostCode" parentElement:childeElement];
        TBXMLElement * CountryID = [TBXML childElementNamed:@"CountryID" parentElement:childeElement];
        TBXMLElement * ContactNo = [TBXML childElementNamed:@"ContactNo" parentElement:childeElement];

        TBXMLElement * Name = [TBXML childElementNamed:@"Name" parentElement:childeElement];
        TBXMLElement * DOB = [TBXML childElementNamed:@"DOB" parentElement:childeElement];
        TBXMLElement * Gender = [TBXML childElementNamed:@"Gender" parentElement:childeElement];
        TBXMLElement * MaritalStatus = [TBXML childElementNamed:@"MaritalStatus" parentElement:childeElement];
        TBXMLElement * EducationID = [TBXML childElementNamed:@"EducationID" parentElement:childeElement];
        TBXMLElement * OccupationID = [TBXML childElementNamed:@"OccupationID" parentElement:childeElement];
        TBXMLElement * RaceID = [TBXML childElementNamed:@"RaceID" parentElement:childeElement];
        TBXMLElement * ReligionID = [TBXML childElementNamed:@"ReligionID" parentElement:childeElement];

        
        
        [User activeUser].IsFirstTime = [TBXML textForElement:IsFirstTime];
        //[User activeUser].userName = [TBXML textForElement:UserID];
        
        [User activeUser].floor = [TBXML textForElement:Floor];
        [User activeUser].unit = [TBXML textForElement:Unit];
        [User activeUser].block = [TBXML textForElement:Block];
        [User activeUser].building = [TBXML textForElement:Building];
        [User activeUser].street = [TBXML textForElement:Street];
        [User activeUser].postalCode = [TBXML textForElement:PostCode];
        [User activeUser].country = [TBXML textForElement:CountryID];
        [User activeUser].contact = [TBXML textForElement:ContactNo];
        
        [User activeUser].name = [TBXML textForElement:Name];       
        [User activeUser].dob = [TBXML textForElement:DOB];
        [User activeUser].gender = [TBXML textForElement:Gender];
        [User activeUser].maritalStatus = [TBXML textForElement:MaritalStatus];
        [User activeUser].education = [TBXML textForElement:EducationID];
        [User activeUser].occupation = [TBXML textForElement:OccupationID];
        [User activeUser].race = [TBXML textForElement:RaceID];
        [User activeUser].religion = [TBXML textForElement:ReligionID];
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            
            if (self.GetProfileDelegate) {
                
                if ([self.GetProfileDelegate respondsToSelector:@selector(api:didFinishGetProfileWithStatus:)]) {
                    
                    [self.GetProfileDelegate api:self didFinishGetProfileWithStatus:YES];
                }
            }
        } else {
            
            if (self.GetProfileDelegate) {
                if ([self.GetProfileDelegate respondsToSelector:@selector(api:didFailGetProfileWithError:)]) {
                    [self.GetProfileDelegate api:self didFailGetProfileWithError:errorReason];
                }
            }
        }
        //[syncDetails release];
    }
    
    [responseData release];
    [self release];
}

-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.GetProfileDelegate) {
		if ([self.GetProfileDelegate respondsToSelector:@selector(api:didFailSyncWithError:)]) {
			[self.GetProfileDelegate api:self didFailGetProfileWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.GetProfileDelegate = nil;
	[super dealloc];
}

@end
