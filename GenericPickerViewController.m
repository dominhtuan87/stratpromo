//
//  GenericPickerViewController.m
//  ClueBox
//
//  Created by cat on 28/11/12.
//
//

#import "GenericPickerViewController.h"

@interface GenericPickerViewController ()

@end

@implementation GenericPickerViewController

@synthesize delegate;
@synthesize pickerView = pickerView_;
@synthesize optional = optional_;
@synthesize selectedItem = selectedItem_;
@synthesize pickerTag;
@synthesize cancelButton,toolbar,label;

- (id)initWithDataSource:(NSArray *)dataSource andDelegate:(NSObject *)delegate{
    
    self = [super initWithNibName:@"GenericPickerViewController" bundle:nil];
    if (self) {
        //pickerPopOverDelegate_ = delegate;
        pickerDataSource_ = [dataSource retain];
		
    }
    return self;
}

-(void)setOptional:(BOOL)yesOrNo {
	optional_ = yesOrNo;
	[pickerView_ reloadAllComponents];
}

-(void)setSelectedItem:(NSString *)anItem {
	if (selectedItem_) {
		[selectedItem_ release];
	}
	selectedItem_ = [anItem copy];
	
	if (anItem!=nil&&[anItem length]>0) {
		
		NSUInteger row = [pickerDataSource_ indexOfObject:anItem];
		
		if (row>=0) {
			if (optional_) {
				row++;
			}
			[self.pickerView selectRow:row inComponent:0 animated:NO];
			return;
		}
	}
	[selectedItem_ release];
	selectedItem_ = nil;
	[self.pickerView selectRow:0 inComponent:0 animated:NO];
    
}

- (IBAction)doneButtonPressed:(id)sender {
    if (selectedItem_) {
		[selectedItem_ release];
	}
	selectedItem_ = nil;
	NSInteger row = [self.pickerView selectedRowInComponent:0];
	
	if (optional_) {
		
		if (row==0) {
			if ([self.delegate respondsToSelector:@selector(pickerView:titleForRow:atRow:)]) {
				[self.delegate  pickerView:self.pickerView titleForRow:@"" atRow:row];
				
			}
            
		} else {
			if ([self.delegate  respondsToSelector:@selector(pickerView:titleForRow:atRow:)]) {
				[self.delegate  pickerView:self.pickerView titleForRow:[pickerDataSource_ objectAtIndex:row-1] atRow:row];
			}
			selectedItem_ = [[pickerDataSource_ objectAtIndex:row-1] copy];
            
		}
	} else {
		if ([self.delegate  respondsToSelector:@selector(pickerView:titleForRow:atRow:)]) {
			[self.delegate  pickerView:self.pickerView titleForRow:[pickerDataSource_ objectAtIndex:row] atRow:row];
			
			
		}
		selectedItem_ = [[pickerDataSource_ objectAtIndex:row] copy];
        
	}
	
    if ([self.delegate  respondsToSelector:@selector(pickerViewDoneButtonDidPressed: withIndex:andTag:)]) {
        [self.delegate  pickerViewDoneButtonDidPressed:selectedItem_ withIndex:(row+1) andTag:self.pickerTag];
        [self.view removeFromSuperview];
        
    }
    
    //[self.view release];
}

-(IBAction)cancelButtonPressed:(id)sender {
    NSLog(@"CANCEL PRESSED BEFORE DELEGATE");
    
    NSLog(@"self.delegate: %@", self.delegate);
    
	if (self.delegate) {
         NSLog(@"CANCEL PRESSED IN IF 1 DELEGATE");
		if ([self.delegate respondsToSelector:@selector(pickerViewCancelButtonDidPressed:)]) {
            NSLog(@"CANCEL PRESSED IN IF 2 DELEGATE");
			[self.delegate pickerViewCancelButtonDidPressed:self];
            [self.view removeFromSuperview];
            
		}
	}
    //[self.view release];
}



#pragma mark - PickerView data source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return  1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (optional_) {
		return [pickerDataSource_ count]+1;
	}
	return [pickerDataSource_ count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if (optional_) {
		if (row==0) {
			return @"-";
		} else {
			return [pickerDataSource_ objectAtIndex:row-1];
		}
        
	}
    return [pickerDataSource_ objectAtIndex:row];
}

#pragma mark - PickerView delegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
	   inComponent:(NSInteger)component {
	
	if (selectedItem_) {
		[selectedItem_ release];
	}
	selectedItem_ = nil;
    
	if (optional_) {
		
		if (row==0) {
			if ([self.delegate  respondsToSelector:@selector(pickerView:titleForRow:atRow:)]) {
				[self.delegate  pickerView:pickerView titleForRow:@"" atRow:row];
			}
		} else {
			if ([self.delegate  respondsToSelector:@selector(pickerView:titleForRow:atRow:)]) {
				[self.delegate  pickerView:pickerView titleForRow:[pickerDataSource_ objectAtIndex:row-1] atRow:row];
			}
			selectedItem_ = [[pickerDataSource_ objectAtIndex:row-1] copy];
            
		}
	} else {
		if ([self.delegate  respondsToSelector:@selector(pickerView:titleForRow:atRow:)]) {
			[self.delegate  pickerView:pickerView titleForRow:[pickerDataSource_ objectAtIndex:row] atRow:row];
			
			
		}
		selectedItem_ = [[pickerDataSource_ objectAtIndex:row] copy];
        
	}
    
    NSLog(@"selectedItem_: %@", selectedItem_);
	
}


-(void)setUI{
    [label setHidden:false];
    
    NSLog(@"Before pickerViewController.toolbar.items : %@",toolbar.items);
    NSMutableArray *toolbarButtons = [toolbar.items mutableCopy];
    NSLog(@"Before toolbarButtons : %@",toolbarButtons);
    [toolbarButtons removeObject:cancelButton];
    NSLog(@"After toolbarButtons: %@",toolbarButtons);
    [toolbar setItems:toolbarButtons animated:NO];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.view = nil;
    //self.mscrollview = nil;
}

- (void)dealloc {
	self.view = nil;
    //self.mscrollview = nil;
    [super dealloc];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
