//
//  CBRegisterViewController.h
//  ClueBox
//
//  Created by cat on 12/9/12.
//
//

#import <UIKit/UIKit.h>
#import "LoginAPI.h"
#import "ReferalAPI.h"
#import "GenericPickerViewController.h"
#import "ActivityView.h"

@interface CBRegisterViewController : UIViewController<LoginAPIDelegate,ReferalAPIDelegate,GenericPickerDelegate>{
    
    UIButton *backButton_;
    

}


@property(nonatomic,retain) IBOutlet UITextField *userName;
@property(nonatomic,retain) IBOutlet UITextField *email;
@property(nonatomic,retain) IBOutlet UITextField *password;
@property(nonatomic,retain) IBOutlet UITextField *confirmPassword;


@property (strong, nonatomic) ActivityView* activity1;

- (IBAction)submitButtonPressed:(id)sender;
-(IBAction)backgroundTap:(id)sender;

@end
