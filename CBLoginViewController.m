//
//  CBSplashViewController.m
//  ClueBox
//
//  Created by user on 10/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CBLoginViewController.h"
#import "CBForgotPasswordViewController.h"
#import "CBRegisterViewController.h"
#import "User.h"
#import "Constant.h"
#import "Utility.h"
//#import "CBCategoryViewController.h"

@interface CBLoginViewController (Private)

- (void)loadStartScreen;

@end

@implementation CBLoginViewController

/*
int temp;
int movement;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
*/
@synthesize activity1,activeTextField;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    //[self.navigationController setNavigationBarHidden:YES];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.navigationItem.hidesBackButton = YES;
    if([[Utility getUserInfo] count] > 0) {
        userName_.text = [[Utility getUserInfo] objectForKey:@"username"];
        password_.text = [[Utility getUserInfo] objectForKey:@"password"];
        rememberButton_.selected = YES;
    } 
        
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTap:)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [theScrollView addGestureRecognizer:tapGesture];
    
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    NSLog(@"keyboardWasShown");
    
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
        double navBarHeight = self.navigationController.navigationBar.frame.size.height;
        CGPoint scrollPoint = CGPointMake(0.0, activeTextField.frame.origin.y - (keyboardSize.height-15) + navBarHeight);
        [theScrollView setContentOffset:scrollPoint animated:YES];
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
    
    NSLog(@"keyboardWillHide");
}



-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}



/*
- (void)checkForStartUPScreen:(id)sender {
    
    [sender setSelected:YES];
    isSelected = YES;
}
*/
- (void)checkForStartUPScreen:(id)sender {
    NSLog(@"isSelected: %c",isSelected);
    if (isSelected) {
        [sender setSelected:NO];
        isSelected = NO;
    }
    else{
        [sender setSelected:YES];
        isSelected = YES;
    }
}





- (void)viewDidUnload
{
    //[userName_ release];
    userName_ = nil;
    //[password_ release];
    password_ = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)backgroundTap:(id)sender {

    [userName_ resignFirstResponder];
    [password_ resignFirstResponder];
}

- (IBAction)readMoreButtonPressed:(id)sender {
    
    NSLog(@"readMoreButtonPressed");
}

- (IBAction)videoButtonPressed:(id)sender {
    
    
}


- (IBAction)forgetPassword:(id)sender {
    
    CBForgotPasswordViewController *forgetPassword = [[CBForgotPasswordViewController alloc] initWithNibName:@"CBForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:forgetPassword animated:YES];
    //[forgetPassword release];
}

- (IBAction)registerButtonPressed:(id)sender {
    
    CBRegisterViewController *registerView = [[CBRegisterViewController alloc] initWithNibName:@"CBRegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerView animated:YES];
    //[registerView release];
}


- (IBAction)loginButtonPressed:(id)sender {
    
    
    
    if(![userName_.text length]||![userName_.text length]){
        NSLog(@"Username OR Password Empty!");
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"Username and Password cannot be empty!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        //[alert release];
    }
    
    else{
        [activity1 presentActivityView:LOGIN_LOADING];
        User *user = [[User alloc] init];
        user.userName = userName_.text;
        user.password = password_.text;
        LoginAPI *loginAPI = [[LoginAPI alloc] init];
        loginAPI.authDelegate = self;
        [loginAPI authorizeUser:user];
        //[loginAPI release];
        //[user release];
    }
}

#pragma mark - LoginAPI Delegate Methods

-(void)api:(id)sender didFinishUserAuthenticationWithStatus:(BOOL)status {
    if(rememberButton_.selected == YES) {
        NSLog(@"userName_: %@",userName_.text);
        NSLog(@"password_: %@",password_.text);
    
        NSDictionary * userInfo = [NSDictionary dictionaryWithObjectsAndKeys:userName_.text,@"username",password_.text,@"password",nil];
        [Utility setRememberUserInfo:userInfo];

    }
    else{
        [Utility removeUserInfo];
    }
    
         
    
    [activity1 dismissActivityView];
    if(status) {
        [self.navigationController popViewControllerAnimated:YES];
       
    }
}

-(void)api:(id)sender didFailAuthWithError:(NSString*)error {
    
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:error];
    NSLog(@"%@",error);
}

- (IBAction)rememberButtonPressed:(id)sender {
    
    if(rememberButton_.selected == NO) {
        [sender setSelected:YES];
        
        //NSLog(@"userName_: %@",userName_.text);
        //NSLog(@"password_: %@",password_.text);
        
       // NSDictionary * userInfo = [NSDictionary dictionaryWithObjectsAndKeys:userName_.text,@"username",password_.text,@"password",nil];
        //[Utility setRememberUserInfo:userInfo];
    } else {
        [sender setSelected:NO];
        //[Utility removeUserInfo];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    textField.delegate = self;
    NSLog(@"textFieldDidBeginEditing");
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)logoutButtonPressed:(id)sender{
    [self logoutAction];
}

- (void)logoutAction {
    
    [activity1 presentActivityView:LOGOUT_LOADING];
    LoginAPI *loginAPI = [[LoginAPI alloc] init];
    loginAPI.authDelegate = self;
    [loginAPI logoutUserWithToken:[Utility getUserToken]];
    //[loginAPI release];
}

-(void)api:(id)sender didFinishLogoutWithStatus:(BOOL)status {
    
    [activity1 dismissActivityView];
    
    [[NSUserDefaults standardUserDefaults]setValue:@"YES" forKey:@"FirstLaunched"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}



@end
