//
//  LoginRequestBuilder.h
//  ClueBox
//
//  Created by user on 11/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
typedef enum {
	LOGIN,
	LOGOUT
} AuthRequestType;


@interface LoginRequestBuilder : NSObject {
    
}

+(NSString*)authRequestContentForUser:(User*)user withAuthType:(AuthRequestType)type;
+(NSString*)loginAgainForUserToken:(NSString *)token;
+(NSString*)logoutForUserToken:(NSString *)token;
+(NSString*)forgotPasswordForUserWithEmail:(NSString *)email;
+(NSString*)signUpRequestForUser:(User*)user;

@end
