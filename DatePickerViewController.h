//
//  DatePickerViewController.h
//  ClueBox
//
//  Created by cat on 28/11/12.
//
//

#import <UIKit/UIKit.h>


@protocol DatePickerDelegate <NSObject>

@optional

//- (void)DatePickerView:(UIPickerView *)DatePickerView titleForRow:(NSString *)title atRow:(NSInteger )row;
//- (void)DatePickerViewDoneButtonDidPressed: (id)sender withIndex:(int)index andTag:(int)tag;
//- (void)DatePickerViewCancelButtonDidPressed: (id)sender;
-(void)DatePickerViewDoneButtonDidPressed: (NSDate*)date andTag:(int)tag;
-(void)dateValueChanged:(NSDate *)date forPickerWithTag:(int)pickerTag;
-(void)DatePickerViewCancelButtonDidPressed:(id)sender;



@end




@interface DatePickerViewController : UIViewController {
    NSObject <DatePickerDelegate> * datePickerDelegate_;
	UIDatePicker * datePickerView_;
	
}

//@property(nonatomic,retain) IBOutlet UIPickerView * DatePickerView;
@property (nonatomic,retain) IBOutlet UIDatePicker * DatePickerView;
@property (nonatomic) NSInteger pickerTag;

@property(nonatomic,assign) NSObject <DatePickerDelegate> * datePickerDelegate;
@property (nonatomic) NSInteger DatePickerTag;

-(IBAction)cancelButtonPressed:(id)sender;
-(IBAction)doneButtonPressed:(id)sender;

//-(IBAction)doneButtonPressed:(id)sender;
-(IBAction)datePickerValueChanged:(id)sender;
//-(IBAction)datePickerClearButtonPressed:(id)sender;


//@property (nonatomic,assign) BOOL optional;
//@property (nonatomic,copy) NSString * selectedItem;

//- (id)initWithDataSource:(NSArray *)dataSource andDelegate:(NSObject *)delegate;


@end
