//
//  GetProfileRequestBuilder.h
//  ClueBox
//
//  Created by cat on 27/11/12.
//
//

#import <UIKit/UIKit.h>

@interface GetProfileRequestBuilder : NSObject{
    
}

+(NSString*)getProfileForUserToken:(NSString *)token;

@end
