//
//  ConfirmReferalRequestBuilder.h
//  ClueBox
//
//  Created by Rakesh Raveendran on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfirmReferalRequestBuilder : NSObject {
    
}

+(NSString*)confirmReferalForUserToken:(NSString *)token withEmail:(NSString*)email;

@end
