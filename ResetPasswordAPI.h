//
//  ResetPasswordAPI.h
//  ClueBox
//
//  Created by cat on 29/11/12.
//
//

#import <UIKit/UIKit.h>
#import "SOAPWebService.h"



@protocol ResetPasswordAPIDelegate <NSObject>
@optional

-(void)api:(id)sender didFinishResetPasswordWithStatus:(BOOL)status;
-(void)api:(id)sender didFailResetPasswordWithError:(NSString*)error;

@end



@interface ResetPasswordAPI : NSObject <SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <ResetPasswordAPIDelegate> * ResetPasswordAPIDelegate_;
}

@property(nonatomic,assign) NSObject <ResetPasswordAPIDelegate> * ResetPasswordAPIDelegate;

- (void)ResetPasswordWithToken:(NSString*)token withNewPassword:(NSString*)newPassword replaceOldPassword:(NSString*)oldPassword;

@end
