//
//  UpdateProfileRequestBuilder.h
//  ClueBox
//
//  Created by cat on 28/11/12.
//
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface UpdateProfileRequestBuilder : NSObject{
    
}

+(NSString*)UpdateProfileForUser:(User*)user withToken:(NSString*)token;

@end
