//
//  CBChangePasswordViewController.m
//  ClueBox
//
//  Created by cat on 29/11/12.
//
//

#import "CBChangePasswordViewController.h"
#import "LeftViewController.h"
#import "IIViewDeckController.h"

#import "Utility.h"
#import "Constant.h"




@interface CBChangePasswordViewController ()

@end

@implementation CBChangePasswordViewController
int temp;
int movement;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@synthesize activity1;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.viewDeckController.leftLedge = 220;
    
    oldPassword_.tag=1;
    newpassword_.tag=2;
    confirmnewpassword_.tag=3;
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    
    //[super viewDidAppear:animated];
    self.navigationItem.hidesBackButton = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    //self.navigationItem.hidesBackButton = YES;
    [self addBackButton];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [backButton_ removeFromSuperview];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    //[backButton_ removeFromSuperview];
}

- (void)addBackButton {
    
    UIImage *image = [UIImage imageNamed: @"logo.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    self.navigationItem.titleView = imageView;
    
    
    
    UIImage *buttonImage = [UIImage imageNamed:@"back_btn_black.png"];
    backButton_ = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton_ setImage:buttonImage forState:UIControlStateNormal];
    [backButton_ setFrame:CGRectMake(10,(self.navigationController.navigationBar.frame.size.height-40)/2+5, 50, 30)];
    [backButton_ addTarget:self action:@selector(goToHomeScreen) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:backButton_];
}

- (void)goToHomeScreen {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)swipeButtonPressed:(id)sender {
    
    [self.viewDeckController toggleLeftView];
}

- (IBAction)submitButtonPressed:(id)sender {
    
    
    if ([newpassword_.text isEqualToString:confirmnewpassword_.text]) {
        [activity1 presentActivityView:CHANGE_PASSWORD_LOADING];
        ResetPasswordAPI *resetPassword = [[ResetPasswordAPI alloc] init];
        resetPassword.ResetPasswordAPIDelegate = self;
        //NSLog(@"... Continue ...");
        
        [resetPassword ResetPasswordWithToken:[Utility getUserToken] withNewPassword:newpassword_.text replaceOldPassword:oldPassword_.text];
        
        //[resetPassword release];

    }
    else
        [Utility showAlertViewWithMessage:@"New password and Confirm Password must be same" withDelegate:self];
    
    
}


-(void)api:(id)sender didFinishResetPasswordWithStatus:(BOOL)status {
    
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:@"Your Password is Changed" withDelegate:self];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)api:(id)sender didFailResetPasswordWithError:(NSString*)error {
    NSLog(@"requestFailedWithError");
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:error];
}



-(IBAction)backgroundTap:(id)sender {
    
    for(int i =1 ;i<= 3;i++) {
		
		UITextField * textField = (UITextField *) [self.view viewWithTag:i];
		if([textField isFirstResponder]) {
			[textField resignFirstResponder];
			
		}
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    textField.delegate = self;
     NSLog(@"textFieldDidBeginEditing");
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    NSLog(@"textFieldDidBeginEditing");
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    NSLog(@"textFieldDidEndEditing");
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    int movementDistance;
    
    if (up) {
        movementDistance = textFieldRect.origin.y - 190;
        NSLog(@"UP");// tweak as needed
    }
    else
        movementDistance = temp; // tweak as needed
    
    temp = movementDistance;
    
    const float movementDuration = 0.3f; // tweak as needed
    
    NSLog(@"temp1: %i", temp);
    
    
    movement = (up ? (0-movementDistance) : (0+movementDistance));
    
    NSLog(@"movement: %i", movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    
    [UIView commitAnimations];
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
