//
//  CBProfileViewController.h
//  ClueBox
//
//  Created by Rakesh Raveendran on 17/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GetProfileAPI.h"
#import "GenericPickerViewController.h"
#import "UpdateProfileAPI.h"
#import "DatePickerViewController.h"
#import "ActivityView.h"


NSDictionary *countryNamesByCode;
NSDictionary *countryCodesByName;

@interface CBProfileViewController : UIViewController <GenericPickerDelegate,GetProfileAPIDelegate,UpdateProfileAPIDelegate,DatePickerDelegate>{
    
    UIButton *backButton_;
    IBOutlet UIScrollView *mscrollview;
    
    IBOutlet UITextField    *floor_;
    IBOutlet UITextField    *unit_;
    IBOutlet UITextField    *block_;
    IBOutlet UITextField    *building_;
    IBOutlet UITextField    *street_;
    IBOutlet UITextField    *postalCode_;
    IBOutlet UITextField    *country_;
    IBOutlet UITextField    *countryID_;
    IBOutlet UITextField    *contact_;
    
    IBOutlet UITextField    *name_;
    IBOutlet UITextField    *dob_;
    IBOutlet UITextField    *gender_;
    IBOutlet UITextField    *maritalStatus_;
    IBOutlet UITextField    *education_;
    IBOutlet UITextField    *occupation_;
    IBOutlet UITextField    *educationID_;
    IBOutlet UITextField    *occupationID_;
    IBOutlet UITextField    *race_;
    IBOutlet UITextField    *religion_;
    IBOutlet UITextField    *raceID_;
    IBOutlet UITextField    *religionID_;
    
    //UIPopoverController *pickerPopOver_;
    NSArray *countries;
    int temp_CountryID;
    int temp_EducationID;
    int temp_OccupationID;
    int temp_RaceID;
    int temp_ReligionID;
    
    UIView * transparentView_;
    
     
    IBOutlet UIButton *CountryButton;
    IBOutlet UIButton *GenderButton;
    IBOutlet UIButton *MaritalStatusButton;
    IBOutlet UIButton *EducationButton;
    IBOutlet UIButton *OccupationButton;
    IBOutlet UIButton *RaceButton;
    IBOutlet UIButton *ReligionButton;
    IBOutlet UIButton *DobButton;
    

}

@property (nonatomic,retain) IBOutlet UIWebView * webView;


- (IBAction)swipeButtonPressed:(id)sender;
- (IBAction)pickerButtonClicked:(UIButton*)sender;
- (IBAction)backgroundTap:(id)sender;


@property(nonatomic, retain)IBOutlet UIScrollView *mscrollview;

@property(nonatomic, retain) NSArray *countries;


@property(nonatomic, retain)IBOutlet UIButton *CountryButton;
@property(nonatomic, retain)IBOutlet UIButton *GenderButton;
@property(nonatomic, retain)IBOutlet UIButton *MaritalStatusButton;
@property(nonatomic, retain)IBOutlet UIButton *EducationButton;
@property(nonatomic, retain)IBOutlet UIButton *OccupationButton;
@property(nonatomic, retain)IBOutlet UIButton *RaceButton;
@property(nonatomic, retain)IBOutlet UIButton *ReligionButton;
@property(nonatomic, retain)IBOutlet UIButton *DobButton;
@property(nonatomic, retain) IBOutlet UIButton *swipeButton;

@property (strong, nonatomic) ActivityView* activity1;

- (void)getProfileAction;
- (IBAction)UpdateProfileAction :(id)sender;
- (IBAction)ChangePasswordButtonPressed:(id)sender ;

@end
