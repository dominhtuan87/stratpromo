//
//  ResetPasswordRequestBuilder.m
//  ClueBox
//
//  Created by cat on 29/11/12.
//
//

#import "ResetPasswordRequestBuilder.h"
#import "Utility.h"

@interface ResetPasswordRequestBuilder ()

@end

@implementation ResetPasswordRequestBuilder

+(NSString*)ResetPasswordWithToken:(NSString*)token withNewPassword:(NSString*)newPassword replaceOldPassword:(NSString*)oldPassword {
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ResetPassword" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__"
																 withString:token];
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__NewPassword__"
																 withString:newPassword];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__OldPassword__"
																 withString:oldPassword];
		return contentFormat;
	}
	
	return @"";
}
@end
