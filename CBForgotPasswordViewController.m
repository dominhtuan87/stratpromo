//
//  CBForgotPasswordViewController.m
//  ClueBox
//
//  Created by cat on 12/9/12.
//
//

#import "CBForgotPasswordViewController.h"

#import "Constant.h"
#import "Utility.h"
#import "CBLoginViewController.h"

@interface CBForgotPasswordViewController ()

@end

@implementation CBForgotPasswordViewController
@synthesize email,activity1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    activity1 = [[ActivityView alloc] init];
    
    //self.navigationItem.hidesBackButton = YES;
}

- (IBAction)backgroundTap:(id)sender {
    
    [self.email resignFirstResponder];
}
- (IBAction)submitButtonPressed:(id)sender {
    if ([self isValid]) {
        
        [activity1 presentActivityView:FORGET_LOADING];
        LoginAPI *loginAPI = [[LoginAPI alloc] init];
        loginAPI.authDelegate = self;
        [loginAPI forgotPasswordForUserWithEmail:self.email.text];
        //[loginAPI release];
    }
}

-(BOOL)isValid {
    BOOL status = YES;
    NSString * message = nil;
    
    if(![self.email.text length]){
        status = NO;
        NSLog(@"Status email_: %c", status);
        message = @"Email field cannot be empty";
    }
    else if([Utility isValidEmail:self.email.text]==NO){
        status = NO;
        NSLog(@"Status email_: %c", status);
        message = @"Please enter a valid email";
    }
        
    if (status==NO) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        //[alert release];
    }
    
    return status;
}


-(void)api:(id)sender didFinishForgotPasswordWithStatus:(BOOL)status {
    
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:@"New password will be sent to your mail" withDelegate:self];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)api:(id)sender didFailAuthWithError:(NSString*)error {
    
    [activity1 dismissActivityView];
    [Utility showAlertViewWithMessage:error];
}


- (void)viewWillAppear:(BOOL)animated{
    
    //[super viewDidAppear:animated];
    //self.navigationItem.hidesBackButton = YES;
    //[self addBackButton];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    //self.navigationItem.hidesBackButton = YES;
    [self addBackButton];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [backButton_ removeFromSuperview];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    //[backButton_ removeFromSuperview];
}

- (void)addBackButton {
    
    UIImage *image = [UIImage imageNamed: @"logo.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    self.navigationItem.titleView = imageView;
    
    
    
    UIImage *buttonImage = [UIImage imageNamed:@"back_btn_black.png"];
    backButton_ = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton_ setImage:buttonImage forState:UIControlStateNormal];
    [backButton_ setFrame:CGRectMake(10,(self.navigationController.navigationBar.frame.size.height-40)/2+5, 50, 30)];
    [backButton_ addTarget:self action:@selector(goToHomeScreen) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:backButton_];
}

- (void)goToHomeScreen {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) slideFrameUp;
{
    [self slideFrame:YES];
}

-(IBAction) slideFrameDown;
{
    [self slideFrame:NO];
}


-(void) slideFrame:(BOOL) up
{
    const int movementDistance = 40; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
