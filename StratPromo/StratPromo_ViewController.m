//
//  StratPromo_ViewController.m
//  StratPromo
//
//  Created by cat on 19/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "StratPromo_ViewController.h"

#import "Categories_ViewController.h"
//#import "NearbyPromo_ViewController.h"
//#import "QRReader_ViewController.h"
#import "QRReader_SuperViewController.h"
#import "Search_ViewController.h"
#import "IIViewDeckController.h"

#import "Constant.h"
#import <QuartzCore/QuartzCore.h>

@interface StratPromo_ViewController ()

@end

@implementation StratPromo_ViewController
@synthesize videoInterstitialViewController;
@synthesize bannerView;
@synthesize tabBarController;
@synthesize activity1,activity2;
@synthesize homeViewController,allDealsViewController;
@synthesize swipeButton;

- (void)viewDidLoad
{
    activity1 = [ActivityView alloc];
    [activity1 presentActivityView:SYNC_LOADING];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Create, add Interstitial/Video Ad View Controller and add view to view hierarchy
    self.videoInterstitialViewController = [[AdSdkVideoInterstitialViewController alloc] init];
    
    // Assign delegate
    self.videoInterstitialViewController.delegate = self;
    
    // Defaults to NO. Set to YES to get locationAware Adverts
    self.videoInterstitialViewController.locationAwareAdverts = YES;
    
    // Add view. Note when it is created is transparent, with alpha = 0.0 and hidden
    // Only when an ad is being presented it become visible
    [self.view addSubview:self.videoInterstitialViewController.view];
    
    
    //self.navigationItem.title = @"$tratPromo";
    
    UIBarButtonItem * searchButton =
    [[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchButtonPressed:)];
    
    UIBarButtonItem * refreshButton =
    [[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshButtonPressed:)];
    
    self.navigationItem.leftBarButtonItem = searchButton ;
    
    self.navigationItem.rightBarButtonItem = refreshButton;
    
    self.navigationItem.title = @"$tratPromo";
    
    //self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:221/255.0f green:221/255.0f blue:221/255.0f alpha:1.0f];
    
    //UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    //titleLabel.textColor = [UIColor colorWithRed:146/255.0f green:146/255.0f blue:146/255.0f alpha:1.0f];
    //titleLabel.backgroundColor = [UIColor clearColor];
    //titleLabel.text = @"$tratPromo";
    //[self.navigationItem setTitleView:titleLabel];
    
    //self.navigationItem.title. = [UIColor colorWithRed:146/255.0f green:146/255.0f blue:146/255.0f alpha:1.0f];
    
    [self requestInterstitialAdvert];
    [self loadTabViewController];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    [self syncMasterActionWithTableName:@"Areas"];
    [self syncMasterActionWithTableName:@"CardProviders"];
    [self syncMasterActionWithTableName:@"Cards"];
    [self syncMasterActionWithTableName:@"Categories"];
    [self syncMasterActionWithTableName:@"DiscountTypes"];
    [self syncMasterActionWithTableName:@"Merchants"];
    
    [self syncAction];
}

- (void)syncAction {
    
    [activity1 presentActivityView:SYNC_LOADING];
    SyncAPI *syncAPI = [[SyncAPI alloc] init];
    syncAPI.syncDelegate = self;
    //NSLog(@"[Utility getUserToken]: %@", [Utility getUserToken]);
    [syncAPI performSyncWithToken:@"SampleToken" andPromoID:@"-1"];
    //[syncAPI release];
}

- (void)syncMasterActionWithTableName:(NSString *)TableName {
    
    [activity2 presentActivityView:SYNC_LOADING];
    SyncMasterAPI *syncMasterAPI = [[SyncMasterAPI alloc] init];
    syncMasterAPI.syncMasterDelegate = self;
    //NSLog(@"[Utility getUserToken]: %@", [Utility getUserToken]);
    [syncMasterAPI performSyncMasterWithTableName:TableName  andKey:@"-1"];
    //[syncAPI release];
}

-(void)api:(id)sender didFinishSyncWithStatus:(BOOL)status
{
    NSLog(@"SYNC PROMOTIONS COMPLETED");
    
    //[self prepareDataForTableView];
    
    [activity1 dismissActivityView];
    [homeViewController prepareDataForTableView];
    [allDealsViewController prepareDataForTableView];
    
    
    
}
-(void)api:(id)sender didFailSyncWithError:(NSString*)error {
    
    [activity1 dismissActivityView];
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Sync Failed!"
                          message: error
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
    //[alert release];
}

-(void)api:(id)sender didFinishSyncMasterWithStatus:(BOOL)status
{
    NSLog(@"SYNC MASTER COMPLETED");
    
    
    
    [activity2 dismissActivityView];
    
    
    
    
    
}
-(void)api:(id)sender didFailSyncMasterWithError:(NSString*)error {
    
    [activity2 dismissActivityView];
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Sync Failed!"
                          message: error
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
    //[alert release];
}



- (IBAction)searchButtonPressed:(id)sender {
	// do the opposite fold/flip style from the transition that presented this view
	NSLog(@"SearchButtonPressed");
    
    [self.viewDeckController closeLeftView];
    
    Search_ViewController *searchViewController = [[Search_ViewController alloc] initWithNibName:@"Search_ViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:searchViewController animated:YES];
      
    //UIAlertView *alert = [[UIAlertView alloc]
    //                      initWithTitle: @"Announcement"
   //                       message: @"Search Function is Coming Soon!"
    //                      delegate: nil
    //                      cancelButtonTitle:@"Stay Tuned!"
    //                      otherButtonTitles:nil];
    //[alert show];
    //[alert release];
}

- (IBAction)refreshButtonPressed:(id)sender {
    [activity1 presentActivityView:SYNC_LOADING];
    
    //UIAlertView *alert = [[UIAlertView alloc]
    //                      initWithTitle: @"Sync Failed!"
    //                      message: @"TEST"
    //                      delegate: nil
    //                      cancelButtonTitle:@"OK"
    //                      otherButtonTitles:nil];
    //[alert show];

    
	// do the opposite fold/flip style from the transition that presented this view
	[self syncMasterActionWithTableName:@"Areas"];
    [self syncMasterActionWithTableName:@"CardProviders"];
    [self syncMasterActionWithTableName:@"Cards"];
    [self syncMasterActionWithTableName:@"Categories"];
    [self syncMasterActionWithTableName:@"DiscountTypes"];
    [self syncMasterActionWithTableName:@"Merchants"];
    
    [self syncAction];
    //[alert release];
}

- (void)loadTabViewController{
    Categories_ViewController *categoryViewController;
    //NearbyPromo_ViewController *nearbyPromoViewController;
    //AllDeals_ViewController *allDealsViewController;
    //QRReader_ViewController *qrReaderViewController;
    //Home_ViewController *homeViewController;
    QRReader_SuperViewController *tabBarViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        homeViewController = [[Home_ViewController alloc] initWithNibName:@"Home_ViewController_iPhone" bundle:nil];
        categoryViewController = [[Categories_ViewController alloc] initWithNibName:@"Categories_ViewController_iPhone" bundle:nil];
        //nearbyPromoViewController = [[NearbyPromo_ViewController alloc] initWithNibName:@"NearbyPromo_ViewController_iPhone" bundle:nil];
        allDealsViewController = [[AllDeals_ViewController alloc] initWithNibName:@"AllDeals_ViewController_iPhone" bundle:nil];
        
        //qrReaderViewController = [[QRReader_ViewController alloc] initWithNibName:@"QRReader_ViewController_iPhone" bundle:nil];
        tabBarViewController = [[QRReader_SuperViewController alloc] initWithNibName:@"QRReader_SuperViewController_iPhone" bundle:nil];
    } else {
        homeViewController = [[Home_ViewController alloc] initWithNibName:@"Home_ViewController_iPad" bundle:nil];
        categoryViewController = [[Categories_ViewController alloc] initWithNibName:@"Categories_ViewController_iPad" bundle:nil];
        //nearbyPromoViewController = [[NearbyPromo_ViewController alloc] initWithNibName:@"NearbyPromo_ViewController_iPad" bundle:nil];
        allDealsViewController = [[AllDeals_ViewController alloc] initWithNibName:@"AllDeals_ViewController_iPad" bundle:nil];
        
        //qrReaderViewController = [[QRReader_ViewController alloc] initWithNibName:@"QRReader_ViewController_iPad" bundle:nil];
        tabBarViewController = [[QRReader_SuperViewController alloc] initWithNibName:@"QRReader_SuperViewController_iPad" bundle:nil];

    }
    
    homeViewController.title = @"Home";
    categoryViewController.title = @"Default Preferences";
    //nearbyPromoViewController.title = @"All Deals";
    allDealsViewController.title = @"All Deals";
    //qrReaderViewController.title = @"QR Reader";
    tabBarViewController.title = @"QR Reader";
    
    
    //NSArray *viewControllers = @[homeViewController, categoryViewController, nearbyPromoViewController,tabBarViewController];
    NSArray *viewControllers = @[homeViewController, categoryViewController, allDealsViewController,tabBarViewController];
	tabBarController = [[MHTabBarController alloc] init];
    
    tabBarController.delegate = self;
	tabBarController.viewControllers = viewControllers;
    
    [tabBarController.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"AmericanTypewriter" size:20.0f], UITextAttributeFont, nil] forState:UIControlStateNormal];
    
    tabBarController.view.frame = self.view.frame;
    
        
    [self addChildViewController:tabBarController];
    [self.view addSubview:tabBarController.view];
    [tabBarController didMoveToParentViewController:self];
    
     [self.view bringSubviewToFront:swipeButton];
    
    
    //[self.view bringSubviewToFront:swipeButton];
    [self.view sendSubviewToBack:tabBarController.view];
    
    swipeButton.layer.zPosition = 100;
    //[self sendSubviewToBack:swipeButton];

}

- (BOOL)mh_tabBarController:(MHTabBarController *)tabBarController2 shouldSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index
{
	NSLog(@"mh_tabBarController %@ shouldSelectViewController %@ at index %u", tabBarController2, viewController, index);
    
	// Uncomment this to prevent "Tab 3" from being selected.
	//return (index != 2);
    
	return YES;
}

- (void)mh_tabBarController:(MHTabBarController *)tabBarController2 didSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index
{
	NSLog(@"mh_tabBarController %@ didSelectViewController %@ at index %u", tabBarController2, viewController, index);
}

- (void)requestBannerAdvert {
    
    if (!self.bannerView) {
        
        self.bannerView = [[AdSdkBannerView alloc] initWithFrame:CGRectZero];
        // size does not matter yet
        
        // Don't trigger an Advert load when setting delegate
        self.bannerView.allowDelegateAssigmentToRequestAd = NO;
        
        self.bannerView.delegate = self;
        
        self.bannerView.backgroundColor = [UIColor clearColor];
        self.bannerView.refreshAnimation = UIViewAnimationTransitionFlipFromLeft;
        
        self.bannerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        
        [self.view addSubview:self.bannerView];
    }
    
    self.bannerView.requestURL = @"http://54.251.253.46:8080/madserve/md.request.php";
    
    [self.bannerView requestAd]; // Request a Banner Advert
    
}


- (void)requestInterstitialAdvert{
    
    if(self.videoInterstitialViewController) {
        
        // If a BannerView is currently being displayed we should remove it
        if ([self isBannerViewInHiearchy]) {
            [self slideOutBannerView:self.bannerView];
        }
        
        self.videoInterstitialViewController.requestURL = @"http://54.251.253.46:8080/madserve/md.request.php";
        
        [self.videoInterstitialViewController requestAd];
    }
}

- (void)adsdkVideoInterstitialViewDidDismissScreen:(AdSdkVideoInterstitialViewController *)videoInterstitial{
    [self requestBannerAdvert];
}

#pragma mark AdSdk BannerView Delegate Methods

- (NSString *)publisherIdForAdSdkBannerView:(AdSdkBannerView *)banner {
    
    return @"d98914322d357ad8f48d71032c6b8778";
}

- (void)adsdkBannerViewDidLoadAdSdkAd:(AdSdkBannerView *)banner {
    NSLog(@"AdSdk Banner: did load ad");
    
    
    CGFloat fullViewHeight = self.view.frame.size.height;
    CGRect tableFrame = self.tabBarController.view.frame;
    CGRect bannerFrame = self.bannerView.frame;
    
    NSLog(@"self.tabBarController.view.frame.size.height BEFORE: %f", self.tabBarController.view.frame.size.height);
    
    tableFrame.size.height = fullViewHeight - bannerFrame.size.height;
    self.tabBarController.view.frame = tableFrame;
    
    NSLog(@"self.tabBarController.view.frame.size.height AFTER: %f", self.tabBarController.view.frame.size.height);
    
    [self slideInBannerView:banner];
}

- (void)adsdkBannerViewDidLoadRefreshedAd:(AdSdkBannerView *)banner {
    NSLog(@"AdSdk Banner: Received a 'refreshed' advert");
    
    if (![self isBannerViewInHiearchy]) {
        
        [self slideInBannerView:banner];
    }
    else {
        
        banner.transform = CGAffineTransformIdentity;
        
        // animate banner into view
        [UIView beginAnimations:@"AdSdk" context:nil];
        [UIView setAnimationDuration:1];
        banner.transform = CGAffineTransformIdentity;
        [UIView commitAnimations];
    }
}

- (void)adsdkBannerView:(AdSdkBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    
    NSLog(@"AdSdk Banner: did fail to load ad: %@", [error localizedDescription]);
    
    [self slideOutBannerView:bannerView];
    
    CGFloat fullViewHeight = self.view.frame.size.height;
    CGRect tableFrame = self.tabBarController.view.frame;
    
    
    tableFrame.size.height = fullViewHeight;
    self.tabBarController.view.frame = tableFrame;
    
    [self requestBannerAdvert];
}

#pragma mark AdSdk Interstitial Delegate Methods

- (NSString *)publisherIdForAdSdkVideoInterstitialView:(AdSdkVideoInterstitialViewController *)videoInterstitial {
    return @"fc8f141e534c895e3160d2685b20d23e";
}

- (void)adsdkVideoInterstitialViewDidLoadAdSdkAd:(AdSdkVideoInterstitialViewController *)videoInterstitial advertTypeLoaded:(AdSdkAdType)advertType {
    
    NSLog(@"AdSdk Interstitial: did load ad");
    
    // Means an advert has been retrieved and configured.
    // Display the ad using the presentAd method and ensure you pass back the advertType
    
    [videoInterstitial presentAd:advertType];
}

- (void)adsdkVideoInterstitialView:(AdSdkVideoInterstitialViewController *)banner didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"AdSdk Interstitial: did fail to load ad: %@", [error localizedDescription]);
    
    [self requestBannerAdvert];
}


-(BOOL)isBannerViewInHiearchy {
    
    for (UIView *oneView in self.view.subviews)
    {
        if(oneView == self.bannerView) {
            return YES;
        }
    }
    
    return NO;
}

- (void)slideOutDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    [self.bannerView removeFromSuperview];
    self.bannerView.delegate = nil;
    self.bannerView = nil;
}

- (void)slideOutBannerView:(AdSdkBannerView *)banner {
    
    // move banner to below the bottom of screen
    banner.center = CGPointMake(self.view.bounds.size.width/2.0, self.view.bounds.size.height - banner.bounds.size.height/2.0);
    
    // animate banner outside view
    [UIView beginAnimations:@"AdSdk" context:nil];
    [UIView setAnimationDuration:1];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(slideOutDidStop:finished:context:)];
    banner.transform = CGAffineTransformMakeTranslation(0, banner.bounds.size.height);
    [UIView commitAnimations];
}

- (void)slideInBannerView:(AdSdkBannerView *)banner {
    
    banner.bounds = CGRectMake(0, 0, self.view.bounds.size.width, banner.bounds.size.height);
    
    // move banner to be at bottom of screen
    banner.center = CGPointMake(self.view.bounds.size.width/2.0, self.view.bounds.size.height - banner.bounds.size.height/2.0);
    
    // set transform to be outside of screen
    banner.transform = CGAffineTransformMakeTranslation(0, banner.bounds.size.height);
    
    // animate banner into view
    [UIView beginAnimations:@"AdSdk" context:nil];
    [UIView setAnimationDuration:1];
    banner.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
    
}


- (IBAction)swipeButtonPressed:(id)sender {
    
    [self.viewDeckController toggleLeftView];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	[self setBannerView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (BOOL)shouldAutomaticallyForwardRotationMethods{
    return NO;
}

@end
