//
//  CommentDeal_RequestBuilder.m
//  StratPromo
//
//  Created by Mac OS cat on 26/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "CommentDeal_RequestBuilder.h"

@implementation CommentDeal_RequestBuilder

+(NSString*)CommentForPromoID:(NSString *)PromoID withToken:(NSString *)token andComment: (NSString*)Comment{

    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Comment" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__" 
																 withString:token];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__PromoID__" 
																 withString:PromoID];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Comment__" 
																 withString:Comment];
                         
        return contentFormat;
	}
	return @"";
}


@end
