//
//  SearchRequestBuilder.h
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchRequestBuilder : NSObject{
    
}

+(NSString*)SearchTerm:(NSString *)SearchTerm atPageNumber:(NSString*)PageNumber;


@end
