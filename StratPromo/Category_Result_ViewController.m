//
//  Category_Result_ViewController.m
//  StratPromo
//
//  Created by cat on 27/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "Category_Result_ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailViewController.h"    

@interface Category_Result_ViewController ()

@end

@implementation Category_Result_ViewController
@synthesize dataArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [SDWebImageManager.sharedManager.imageDownloader setValue:@"SDWebImage Demo" forHTTPHeaderField:@"AppName"];
    SDWebImageManager.sharedManager.imageDownloader.executionOrder = SDWebImageDownloaderLIFOExecutionOrder;
    //return self;
    
    mainTable.backgroundColor = [UIColor clearColor];
    mainTable.opaque = NO;
    mainTable.backgroundView = nil;
    
    
    //_foldStyle = MPFoldStyleCubic;
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSLog(@"Result dataArray:%@",self.dataArray);
    //NSDictionary *dictionary = [dataArray objectAtIndex:indexPath.section];
    //NSArray *array = [[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"];
    NSDictionary *lineItem = [dataArray objectAtIndex:indexPath.row];
    NSLog(@"LineItem Cell result: %@",lineItem);
    //NSString *cellValue = [lineItem objectForKey:@"title"];
    //NSLog(@"cellValue Cell: %@",cellValue);
    cell.textLabel.text = [lineItem objectForKey:@"PromotionHeading"];
    
    NSString *concat = [NSString stringWithFormat:@"%@\n%@",[lineItem objectForKey:@"CategoryName"],[lineItem objectForKey:@"CardName"]];
    cell.detailTextLabel.font=[UIFont systemFontOfSize:11];
    cell.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
    cell.detailTextLabel.numberOfLines = 3;
    cell.detailTextLabel.text = concat;
    
    //Images
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [cell.imageView setImageWithURL:[NSURL URLWithString:[lineItem objectForKey:@"ThumbnailImageURL"]]
                   placeholderImage:[UIImage imageNamed:@"placeholder"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
    
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"dataAttay count: %@", dataArray);
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.dataArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *selectedCell = nil;
    
    selectedCell = [dataArray objectAtIndex:indexPath.row];
    
    NSLog(@"selectedCell result: %@", selectedCell);
    
    
    if (!self.detailViewController)
    {
        
        //NSDictionary *dictionary = [dataArray objectAtIndex:indexPath.section];
        //NSArray *array = [[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone" bundle:nil];
        }
        else{
            self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPad" bundle:nil];
        }
        NSDictionary *lineItem = [dataArray objectAtIndex:indexPath.row];
        
        NSLog(@"lineItem Result: %@", lineItem);
        
        NSString *largeImageURL = [lineItem objectForKey:@"ImageURL"];
        
        NSLog(@"largeImageURL: %@",largeImageURL);
        
        self.detailViewController.imageURL = [NSURL URLWithString:largeImageURL];
        self.detailViewController.PromoID = [lineItem objectForKey:@"PromoID"];
        
        //[self setMode:0];
        
        UIBarButtonItem * doneButton =
        [[UIBarButtonItem alloc]
         initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(popPressed:)];
        
        self.detailViewController.navigationItem.rightBarButtonItem = doneButton ;
        
        //[self.navigationController popToRootViewControllerWithFoldStyle:0];
        //[self.navigationController popToRootViewControllerAnimated:NO];
        //[self.navigationController pushViewController:self.detailViewController foldStyle:7];
        [self.navigationController pushViewController:self.detailViewController animated:YES];
        
        self.detailViewController = nil;
        //[self.navigationController popToRootViewControllerAnimated:NO];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
