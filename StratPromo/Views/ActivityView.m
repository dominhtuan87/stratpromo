//
//  ActivityView.m
//  Y3
//
//  Created by user on 11/05/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ActivityView.h"


@implementation ActivityView

static UIAlertView *_activityAlertView;

+ (BOOL)isVisible {
    
    if(_activityAlertView) {
        
        return YES;
    }
    
    return NO;
}


- (void)presentActivityView:(NSString *)activityMessage
{
	if (_activityAlertView) {
		[_activityAlertView dismissWithClickedButtonIndex:0 animated:YES];
		//[_activityAlertView release];
		_activityAlertView = nil;
	}
	_activityAlertView = [[UIAlertView alloc] initWithTitle:@"" message:activityMessage delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
	[_activityAlertView show];
	//NSLog(@"actvty retain count is ===== %d",[_activityAlertView retainCount]);
	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(125, 60, 25, 25)];
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;    
	[indicator startAnimating];
	[_activityAlertView addSubview:indicator];
	//[indicator release];
    
    //return YES;
}

- (void)dismissActivityView
{
	[_activityAlertView dismissWithClickedButtonIndex:0 animated:YES];
	//[_activityAlertView release];
	_activityAlertView = nil;
}

+ (void)presentProgressActivityView:(NSString *)activityMessage{
	if (_activityAlertView) {
		[_activityAlertView dismissWithClickedButtonIndex:0 animated:YES];
		//[_activityAlertView release];
		_activityAlertView = nil;
	}
	_activityAlertView = [[UIAlertView alloc] initWithTitle:@"" message:activityMessage delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
	[_activityAlertView show];
	
	UIProgressView *progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
	progressView.center = CGPointMake(140,100);	
	progressView.tag = 101;
	[_activityAlertView addSubview:progressView];
	//[progressView release];
}

+(void)setActivityProgress:(CGFloat)progress{
	if (_activityAlertView) {
		UIProgressView *progressView = (UIProgressView *)[_activityAlertView viewWithTag:101];
		[progressView setProgress:progress];
	}
}

@end
