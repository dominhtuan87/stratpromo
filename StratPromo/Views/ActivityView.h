//
//  ActivityView.h
//  Y3
//
//  Created by user on 11/05/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityView : NSObject {
    
}

- (void)presentActivityView:(NSString*)activityMessage;
- (void)dismissActivityView;
+ (void)presentProgressActivityView:(NSString *)activityMessage;
+ (void)setActivityProgress:(CGFloat)progress;
+ (BOOL)isVisible;

@end
