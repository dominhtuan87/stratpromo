//
//  AllDeals_ViewController.h
//  StratPromo
//
//  Created by cat on 13/8/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPFoldEnumerations.h"
#import "MPFlipEnumerations.h"
#import <CoreLocation/CoreLocation.h>


@protocol MPModalViewControllerDelegate;
@class DetailViewController;

enum {
	MPTransitionModeFold2,
	MPTransitionModeFlip2
} typedef MPTransitionMode2;


@interface AllDeals_ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate>
{
    NSMutableArray *allDeals;
    IBOutlet UITableView *mainTable;
    
    NSMutableArray *dataArray;
    CLLocationManager *locationManager;
}
@property (strong, nonatomic) NSMutableArray *allDeals;

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (assign, nonatomic) MPTransitionMode2 mode;

@property (assign, nonatomic) MPFoldStyle foldStyle;
@property (assign, nonatomic) MPFlipStyle flipStyle;
@property (readonly, nonatomic) BOOL isFold;

@property (readonly, nonatomic) IBOutlet UITableView *mainTable;
@property (readonly, nonatomic) IBOutlet UIButton *sortByButton;
-(IBAction)sortBy:(id)sender;


-(void)prepareDataForTableView;
@end