//
//  DetailViewController.h
//  SDWebImage Demo
//
//  Created by Olivier Poitrey on 09/05/12.
//  Copyright (c) 2012 Dailymotion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import	"MPFoldEnumerations.h"
#import "CommentDealAPI.h"
#import "RateDealAPI.h"
#import "StarRatingControl.h"
#import "RNBlurModalView.h"
#import "ActivityView.h"
#import "SyncAPI.h"

@interface DetailViewController : UIViewController<UITextFieldDelegate,SyncAPIDelegate,CommentDealAPIDelegate,StarRatingDelegate,RateDealAPIDelegate>{
    UIView *PopupView;
    IBOutlet UIScrollView *theScrollView;
    
    UITextField *activeTextField;

}

@property (strong, nonatomic) ActivityView* activity1;

@property (weak) IBOutlet StarRatingControl *starRatingControl;
@property (weak) IBOutlet UILabel *ratingLabel;

@property (strong, nonatomic) RNBlurModalView *modal;

@property (strong, nonatomic) NSURL *imageURL;

@property (strong, nonatomic) NSString *PromoID;

@property (strong, nonatomic) NSString *tempRating;

@property (strong, nonatomic) NSString *promoURL;

@property (strong, nonatomic) NSString *merchantAddress;

@property (strong, nonatomic) NSString *promotionLocationString;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIView *PopupView;

@property (strong, nonatomic) IBOutlet UILabel *promoHeading;
@property (strong, nonatomic) IBOutlet UILabel *DiscountType;
@property (strong, nonatomic) IBOutlet UILabel *PromoEndDate;
@property (strong, nonatomic) IBOutlet UILabel *Rating;
@property (strong, nonatomic) IBOutlet UITextView *PromotionDetails;
@property (strong, nonatomic) IBOutlet UITextView *Comments;
@property (strong, nonatomic) IBOutlet UITextField *NewComment;

@property (strong, nonatomic) UITextField *activeTextField;

- (IBAction)backgroundTouched:(id)sender;

@property (assign, nonatomic, getter = isFold) BOOL fold;

- (IBAction)popPressed:(id)sender;

- (IBAction)rateButtonPressed:(id)sender;

- (IBAction)rateSubmitButtonPressed:(id)sender;

- (IBAction)getDealButtonPressed:(id)sender;

- (IBAction)mapButtonPressed:(id)sender;

@end
