//
//  CommentDealAPI.h
//  StratPromo
//
//  Created by Mac OS cat on 26/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SOAPWebService.h"


@protocol CommentDealAPIDelegate <NSObject>

@optional

-(void)api:(id)sender didFinishCommentWithStatus:(BOOL)status;
-(void)api:(id)sender didFailCommentWithError:(NSString*)error;

@end


@interface CommentDealAPI : NSObject<SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <CommentDealAPIDelegate> * commentDelegate_;
}

@property(nonatomic,assign) NSObject <CommentDealAPIDelegate> * commentDelegate;

- (void)performCommentForPromoID:(NSString *)PromoID withUserToken:(NSString*)token andComment:(NSString*)comment;

@end
