//
//  SOAPWebService.m
//
//  Created by user on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SOAPWebService.h"
#import "CBXML.h"
#import "Constant.h"
#import "Utility.h"

@implementation SOAPWebService

@synthesize serviceDelegate = serviceDelegate_;
@synthesize serviceKey = serviceKey_;

- (id)init {
    
    self = [super init];
    return self;
}

- (void)doSOAPCallWithUrl:(NSString *)url andSOAPMessage:(NSString *)soapMessage andSOAPAction:(NSString *)action {
    
    if([Utility isNetworkAvailable]==NO){
        [serviceDelegate_ service:self requestFailedWithError:NETWORK_CONNECTION_FAIL];
        return;
    }
    
	[self retain];
	
	if (request_) {
		request_.delegate = nil;
		if ([request_ isExecuting]) {
			[request_ cancel];
		}
		[request_ release];
	}
	
    NSURL * requestUrl = [NSURL URLWithString:
                          [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];   
	request_ = [[ASIHTTPRequest alloc] initWithURL:requestUrl];
    [request_ setRequestMethod:POST_METHOD];
	[request_ addRequestHeader:CONTENT_TYPE value:XML_CONTENT_TYPE_VALUE];
    
    
	[request_ addRequestHeader:@"SOAPAction" value:action];
	
    
	NSError * error = nil;
	NSString * xmlFormat = [[[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"soapRequestFormat" ofType:@"xml"] encoding:NSUTF8StringEncoding error:&error]autorelease];
	NSString * xmlContent = [NSString stringWithFormat:xmlFormat,soapMessage];
	NSLog(@"XMl content = %@",xmlContent);
	NSData *requestData = [xmlContent dataUsingEncoding:NSUTF8StringEncoding];
	
    
    [request_ appendPostData:requestData];
	request_.delegate = self;
	[request_ startAsynchronous];
     
}

#pragma mark -- ASIHTTPRequest Delegate Methods


- (void)requestStarted:(ASIHTTPRequest *)request {
    
}
- (void)request:(ASIHTTPRequest *)request didReceiveResponseHeaders:(NSDictionary *)responseHeaders {
    
}

- (void)requestFinished:(ASIHTTPRequest *)request {
	
    if ([request responseStatusCode] == 200) {
		
		NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
		NSString * newString = [request responseString];
		if (![newString isEqualToString:@""]) {
            
			NSLog(@"\n-----SOAP RESPONSE-----\n %@ \n--------------\n",newString);
			CBXML *parser = [[CBXML alloc] initWithXMLString:[request responseString]];
			[result setObject:parser forKey:self.serviceKey];
			if(serviceDelegate_)
				if ([serviceDelegate_ respondsToSelector:@selector(service:requestSuccessfullyCompleted:)]) {
					[serviceDelegate_ service:self requestSuccessfullyCompleted:result];
				}
			[parser release];
		}else {
			if (serviceDelegate_) {
				if ([serviceDelegate_ respondsToSelector:@selector(service:requestFailedWithError:)]) {
					[serviceDelegate_ service:self requestFailedWithError:@"Web service error"];
				}
			}
		}
        [result release];
        
	}else {
		NSLog(@"[request.error code]: %d",[request.error code]);
        NSLog(@"request.error: %@",request.error);
        
		NSString * newString = [request responseString];
		NSLog(@"\n-----SOAP RESPONSE FAIL-----\n %@ \n--------------\n",newString);
		
		if (serviceDelegate_) {
			if ([serviceDelegate_ respondsToSelector:@selector(service:requestFailedWithError:)]) {
				[serviceDelegate_ service:self requestFailedWithError:[request.error localizedDescription]];
			}
		}
	}
    [self release];

}												   
- (void)requestFailed:(ASIHTTPRequest *)request {
    
    NSLog(@"[request.error code]: %d",[request.error code]);
    
	if (serviceDelegate_) {
		if ([serviceDelegate_ respondsToSelector:@selector(service:requestFailedWithError:)]) {
			[serviceDelegate_ service:self requestFailedWithError:[request.error localizedDescription]];
		}
	}
    [self release];
}

#pragma mark -- Memory management


- (void)dealloc {
	
	self.serviceDelegate = nil;
    if (request_) {
		if ([request_ isExecuting]) {
			[request_ cancel];
		}
        [request_ release];
    }   
    request_ = nil;
    [super dealloc]; 
}


@end
