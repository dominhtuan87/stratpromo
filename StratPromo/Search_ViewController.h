//
//  Search_ViewController.h
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchAPI.h"
#import "ActivityView.h"

@interface Search_ViewController : UIViewController<SearchAPIDelegate>{
    
}
@property (strong, nonatomic) IBOutlet UITextField *searchField;
@property (strong, nonatomic) ActivityView* activity1;
- (IBAction)submitButtonPressed:(id)sender;

- (IBAction)cardsButtonPressed:(id)sender;
- (IBAction)banksButtonPressed:(id)sender;
- (IBAction)areasButtonPressed:(id)sender;
- (IBAction)discountsButtonPressed:(id)sender;
-(IBAction)backgroundTouched:(id)sender;

@end
