//
//  QRReader_ViewController.h
//  StratPromo
//
//  Created by cat on 25/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZXingWidgetController.h"
#import "MessageViewController.h"
#import "ModalViewControllerDelegate.h"

@class UniversalResultParser;
@class ParsedResult;
@class ResultAction;

@interface QRReader_ViewController : UIViewController<ZXingDelegate,UIActionSheetDelegate,ModalViewControllerDelegate,UIAlertViewDelegate> {
    NSArray *actions;
    ParsedResult *result;
}

@property (nonatomic,retain) NSArray *actions;
@property (nonatomic,retain) ParsedResult *result;

- (IBAction)scan:(id)sender;

- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)result;
- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller;
- (void)performResultAction;

@end
