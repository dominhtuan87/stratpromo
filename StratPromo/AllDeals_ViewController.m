//
//  AllDeals_ViewController.m
//  StratPromo
//
//  Created by cat on 13/8/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "AllDeals_ViewController.h"
#import "DetailViewController.h"
#import "MPFoldTransition.h"
#import "MPFoldSegue.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#include "MHTabBarController.h"
#import "Constant.h"

#import "Utility.h"
#import "dealDetails.h"
#import "dealcomments.h"
#import "FMDatabase.h"

@interface AllDeals_ViewController ()

@end

@implementation AllDeals_ViewController
@synthesize foldStyle = _foldStyle;
@synthesize mainTable;
@synthesize allDeals;
@synthesize detailViewController = _detailViewController;
@synthesize sortByButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
    [self prepareDataForTableView];
    
}

-(void)prepareDataForTableView{
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"prepareDataForTableView ALLDEALS");
    
    // INitialize Mutable Arrays
    allDeals = [[NSMutableArray alloc] init];
    dataArray = [[NSMutableArray alloc] init];
    
    //First section data
    NSDictionary *lineItem;
    
    //OPEN DB
    BOOL success = FALSE;
    //NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
    
    //NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
    
    FMDatabase *database = [FMDatabase databaseWithPath:path];
    
    database.traceExecution = YES;
    NSLog(@"Errors: %c",database.logsErrors);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    success = [fileManager fileExistsAtPath:path];
    
    if(success){
        NSLog(@"DB existed ALLDEALS!");
        
    }
    
    if(![database open])
    {
        
        
        NSLog(@"Cant open DB");
        NSLog(@"Errors: %c",database.logsErrors);
        
    }
    
    
    if ([database hadError]) {
        NSLog(@"Err %d: %@", [database lastErrorCode], [database lastErrorMessage]);
    }
    
    if([sortByButton.titleLabel.text isEqualToString:@"Sort By Distance"]){
     
        NSLog(@"Sort By Distance Check");
        
        
        //CLLocationManager *locationManager;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if(![CLLocationManager locationServicesEnabled]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Location service is Disabled" message:@"Please check Location service and try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            [sortByButton setTitle: @"Sort By Most Discount" forState: UIControlStateNormal];
            
        }
        
        else{
            NSLog(@"Location service available");
            
            [locationManager startUpdatingLocation];
            //[locationManager startUpdatingLocation];
            
            if([locationManager location] == nil){
                NSLog(@"Waiting for location ...");
            }
            //locationManager.StopUpdatingLocation();
            
            else{
                [locationManager stopUpdatingLocation];
                
                //CLLocationCoordinate2D location = [locationManager location].coordinate;
                
                NSLog(@"Current latitude: %f", [locationManager location].coordinate.latitude);
                NSLog(@"Current longitude: %f", [locationManager location].coordinate.longitude);
                
            }
        
        }
        FMResultSet *results = [database executeQuery:@"SELECT * FROM promotions ORDER BY DiscountPercentage DESC"];
        
        while([results next]) {
            //NSString *name = [results stringForColumn:@"name"];
            //NSInteger age  = [results intForColumn:@"age"];
            
            
            FMResultSet *results2;
            results2 = [database executeQuery:@"SELECT * FROM Categories WHERE CategoryID = ?", [results stringForColumn:@"CategoryID"]];
            [results2 next];
            NSString *CategoryName = [results2 stringForColumn:@"CategoryName"];
            
            results2 = [database executeQuery:@"SELECT * FROM Cards WHERE CardID = ?", [results stringForColumn:@"CardID"]];
            [results2 next];
            NSString *CardName = [results2 stringForColumn:@"CardName"];
            
            
            CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:[locationManager location].coordinate.latitude longitude:[locationManager location].coordinate.longitude];
            
            NSLog(@"userLoc: %@", userLoc);
            
            CLLocation *poiLoc = [[CLLocation alloc] initWithLatitude:[[[[results stringForColumn:@"PromotionLocation"] componentsSeparatedByString:@","] objectAtIndex:0] floatValue] longitude:[[[[results stringForColumn:@"PromotionLocation"] componentsSeparatedByString:@","] objectAtIndex:1] floatValue]];
            
            NSLog(@"[results stringForColumn:@\"PromotionLocation\"]: %@", [results stringForColumn:@"PromotionLocation"]);
            
            NSLog(@"poiLoc: %@", poiLoc);
            
            CLLocationDistance meters = [userLoc distanceFromLocation:poiLoc];
            
            NSLog(@"meters: %f", meters);
            
            
            
            lineItem = @{@"PromoID" : [results stringForColumn:@"PromoID"],
                         @"PromotionHeading" : [results stringForColumn:@"PromotionHeading"],
                         @"PromotionDetails" : [results stringForColumn:@"PromotionDetails"],
                         @"CategoryName" : CategoryName,
                         @"CardName" : CardName,
                         @"ImageURL" : [results stringForColumn:@"ImageURL"],
                         @"ThumbnailImageURL" : [results stringForColumn:@"ThumbnailImageURL"],
                         @"PromotionLocation" : [results stringForColumn:@"PromotionLocation"],
                         @"DiscountPercentage" : [results stringForColumn:@"DiscountPercentage"],
                         @"Distance" : [NSString stringWithFormat:@"%f", meters]};
            
            NSLog(@"lineItem WHILE Maintable: %@", lineItem);
            
            [allDeals addObject:lineItem];
            
        }
        
        NSSortDescriptor *aSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Distance" ascending:YES];
        
        [allDeals sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
        
        [dataArray addObject:[NSDictionary dictionaryWithObject:allDeals forKey:@"data"]];
        
        
    }
    
    else{
        //ALL DEALS
        
        FMResultSet *results = [database executeQuery:@"SELECT * FROM promotions ORDER BY DiscountPercentage DESC"];
        
        
        
        while([results next]) {
            //NSString *name = [results stringForColumn:@"name"];
            //NSInteger age  = [results intForColumn:@"age"];
            
            NSLog(@"IN WHILE");
            NSLog(@"CategoryID: %@", [results stringForColumn:@"CategoryID"]);
            
            
            
            FMResultSet *results2;
            results2 = [database executeQuery:@"SELECT * FROM Categories WHERE CategoryID = ?", [results stringForColumn:@"CategoryID"]];
            
            [results2 next];
            NSString *CategoryName = [results2 stringForColumn:@"CategoryName"];
            NSLog(@"CategoryName: %@", [results2 stringForColumn:@"CategoryName"]);
            
            results2 = [database executeQuery:@"SELECT * FROM Cards WHERE CardID = ?", [results stringForColumn:@"CardID"]];
            [results2 next];
            NSString *CardName = [results2 stringForColumn:@"CardName"];
            NSLog(@"CardName: %@", [results2 stringForColumn:@"CardName"]);
            
            NSLog(@"PromoID: %@", [results stringForColumn:@"PromoID"]);
            NSLog(@"PromotionHeading: %@", [results stringForColumn:@"PromotionHeading"]);
            NSLog(@"PromotionDetails: %@", [results stringForColumn:@"PromotionDetails"]);
            NSLog(@"ImageURL: %@", [results stringForColumn:@"ImageURL"]);
            NSLog(@"ThumbnailImageURL: %@", [results stringForColumn:@"ThumbnailImageURL"]);
            NSLog(@"PromotionLocation: %@", [results stringForColumn:@"PromotionLocation"]);
            NSLog(@"DiscountPercentage: %@", [results stringForColumn:@"DiscountPercentage"]);
            
            lineItem = @{@"PromoID" : [results stringForColumn:@"PromoID"],
                         @"PromotionHeading" : [results stringForColumn:@"PromotionHeading"],
                         @"PromotionDetails" : [results stringForColumn:@"PromotionDetails"],
                         @"CategoryName" : CategoryName,
                         @"CardName" : CardName,
                         @"ImageURL" : [results stringForColumn:@"ImageURL"],
                         @"ThumbnailImageURL" : [results stringForColumn:@"ThumbnailImageURL"],
                         @"PromotionLocation" : [results stringForColumn:@"PromotionLocation"],
                         @"DiscountPercentage" : [results stringForColumn:@"DiscountPercentage"
                                                  ]};
            
            NSLog(@"lineItem WHILE Maintable: %@", lineItem);
            [allDeals addObject:lineItem];
        }
        
        //[results release];
        [dataArray addObject:[NSDictionary dictionaryWithObject:allDeals forKey:@"data"]];
    
    
    }
    
    
    //CLOSE DB
    
    [database close];
    
    NSLog(@"Data Array MainTable ALLDEALS: %@",dataArray);
    
    [self.mainTable reloadData];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"VIEWDIDLOAD ALLDEALS");
    
    [SDWebImageManager.sharedManager.imageDownloader setValue:@"SDWebImage Demo" forHTTPHeaderField:@"AppName"];
    SDWebImageManager.sharedManager.imageDownloader.executionOrder = SDWebImageDownloaderLIFOExecutionOrder;
    //return self;
    
    mainTable.backgroundColor = [UIColor clearColor];
    mainTable.opaque = NO;
    mainTable.backgroundView = nil;
    
    
    _foldStyle = MPFoldStyleCubic;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //NSLog(@"self.view.frame.size.Home Categories: %f", self.view.frame.size.height);
    

}

- (void)viewWillDisappear:(BOOL)animated{
    //[ActivityView dismissActivityView];
    
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearDisk];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"dataAttay count: %@", dataArray);
    return [dataArray count];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"numberOfRowsInSection START");
    
    
    //Number of rows it should expect should be based on the section
    NSDictionary *dictionary = [dataArray objectAtIndex:section];
    
    //NSLog(@"numberOfRowsInSection dictionary: %@", dictionary);
    
    NSArray *array = [dictionary objectForKey:@"data"];
    
    //NSLog(@"numberOfRowsInSection array: %@", array);
    return [array count];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    //if(section == 1){
    //    return @"This is a footer";
    //} else {
    //    return @"";
    //}
    return @"";
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    
    //NSDictionary *dictionary = [dataArray objectAtIndex:indexPath.section];
    //NSArray *array = [[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"];
    NSDictionary *lineItem = [[[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"] objectAtIndex:indexPath.row];
    // NSLog(@"LineItem Cell: %@",lineItem);
    //NSString *cellValue = [lineItem objectForKey:@"title"];
    //NSLog(@"cellValue Cell: %@",cellValue);
    cell.textLabel.text = [lineItem objectForKey:@"PromotionHeading"];
    
    NSString *concat = [NSString stringWithFormat:@"%@\n%@",[lineItem objectForKey:@"CategoryName"],[lineItem objectForKey:@"CardName"]];
    cell.detailTextLabel.font=[UIFont systemFontOfSize:11];
    cell.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
    cell.detailTextLabel.numberOfLines = 3;
    cell.detailTextLabel.text = concat;
    
    //Images
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [cell.imageView setImageWithURL:[NSURL URLWithString:[lineItem objectForKey:@"ThumbnailImageURL"]]
                   placeholderImage:[UIImage imageNamed:@"placeholder"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
    
    
    return cell;
    
    
    /*
     static NSString *CellIdentifier = @"Cell";
     
     Home_ViewController_CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     if (cell == nil) {
     cell = [[Home_ViewController_CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
     }
     
     // Configure the cell...
     //NSDictionary *dictionary = [dataArray objectAtIndex:indexPath.section];
     //NSArray *array = [[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"];
     NSDictionary *lineItem = [[[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"] objectAtIndex:indexPath.row];
     // NSLog(@"LineItem Cell: %@",lineItem);
     //NSString *cellValue = [lineItem objectForKey:@"title"];
     //NSLog(@"cellValue Cell: %@",cellValue);
     cell.textLabel.text = [lineItem objectForKey:@"PromotionHeading"];
     
     NSString *concat = [NSString stringWithFormat:@"%@\n%@",[lineItem objectForKey:@"CategoryID"],[lineItem objectForKey:@"CardID"]];
     cell.detailTextLabel.font=[UIFont systemFontOfSize:11];
     cell.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
     cell.detailTextLabel.numberOfLines = 3;
     cell.detailTextLabel.text = concat;
     
     //Images
     cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
     [cell.imageView setImageWithURL:[NSURL URLWithString:[lineItem objectForKey:@"ImageURL"]]
     placeholderImage:[UIImage imageNamed:@"placeholder"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
     
     return cell;
     */
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *selectedCell = nil;
    
    selectedCell = [[[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"] objectAtIndex:indexPath.row];
    
    NSLog(@"selectedCell: %@", selectedCell);
    
    
    if (!self.detailViewController)
    {
        
        //NSDictionary *dictionary = [dataArray objectAtIndex:indexPath.section];
        //NSArray *array = [[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone" bundle:nil];
        }
        else{
            self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPad" bundle:nil];
        }
        NSDictionary *lineItem = [[[dataArray objectAtIndex:indexPath.section] objectForKey:@"data"] objectAtIndex:indexPath.row];
        
        NSString *largeImageURL = [lineItem objectForKey:@"ImageURL"];
        
        NSLog(@"largeImageURL: %@",largeImageURL);
        
        self.detailViewController.imageURL = [NSURL URLWithString:largeImageURL];
        self.detailViewController.PromoID = [lineItem objectForKey:@"PromoID"];
        
        [self setMode:0];
        
        
        
        //[self.navigationController popToRootViewControllerWithFoldStyle:0];
        //[self.navigationController popToRootViewControllerAnimated:NO];
        [self.navigationController pushViewController:self.detailViewController foldStyle:7];
        
        self.detailViewController = nil;
        //[self.navigationController popToRootViewControllerAnimated:NO];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
    
    
    
}


#pragma mark - Properties

- (NSUInteger)style
{
	switch ([self mode]) {
		case MPTransitionModeFold2:
			return [self foldStyle];
			
		case MPTransitionModeFlip2:
			return [self flipStyle];
	}
}

- (void)setStyle:(NSUInteger)style
{
	switch ([self mode]) {
		case MPTransitionModeFold2:
			[self setFoldStyle:style];
			break;
			
		case MPTransitionModeFlip2:
			[self setFlipStyle:style];
			break;
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)sortBy:(id)sender{
    if ([[sender currentTitle] isEqualToString:@"Sort By Most Discount"]) {
        [sortByButton setTitle: @"Sort By Distance" forState: UIControlStateNormal];
        [self prepareDataForTableView];
    }
    else{
        [sortByButton setTitle: @"Sort By Most Discount" forState: UIControlStateNormal];
        [self prepareDataForTableView];
    }
}

-(void)sortTableFunction{
    NSSortDescriptor *aSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Distance" ascending:YES];
    
    [allDeals sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    
    [dataArray addObject:[NSDictionary dictionaryWithObject:allDeals forKey:@"data"]];
}

@end

