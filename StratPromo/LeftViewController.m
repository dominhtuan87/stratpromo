//
//  LeftViewController.m
//  ClueBox
//
//  Created by cat on 12/11/12.
//
//

#import "LeftViewController.h"
#import "CBLoginViewController.h"
#import "Utility.h"

//#import "CBMySurveysViewController.h"
//#import "CBMyTrackersViewController.h"
//#import "CBMyForumsViewController.h"
//#import "CBCategoryViewController.h"

#import "IIViewDeckController.h"


@interface LeftViewController ()

@end

@implementation LeftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonPressed:(id)sender {
    [self.viewDeckController closeLeftView];
    
    if([Utility getUserToken] == nil) {
        
        NSLog(@"loginButtonPressed");
        CBLoginViewController *loginViewController = [[CBLoginViewController alloc] initWithNibName:@"CBLoginViewController" bundle:nil];
        [super.navigationController pushViewController:loginViewController animated:YES];
        
        loginViewController.navigationItem.leftBarButtonItem.title = @"Back";
    }
    
    else{
        NSLog(@"loginButtonPressed");
        CBLoginViewController *loginViewController = [[CBLoginViewController alloc] initWithNibName:@"CBLogoutViewController" bundle:nil];
        [super.navigationController pushViewController:loginViewController animated:YES];
        
        loginViewController.navigationItem.leftBarButtonItem.title = @"Back";
        
    }
    
    
}


- (IBAction)favouriteButtonPressed:(id)sender {
    
}


- (IBAction)alertButtonPressed:(id)sender {
    
    

}

@end
