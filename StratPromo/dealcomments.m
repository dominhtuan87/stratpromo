//
//  dealcomments.m
//  StratPromo
//
//  Created by cat on 16/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "dealcomments.h"


@implementation dealcomments

@synthesize UserID = UserID_;
@synthesize Comment = Comment_;
@synthesize Username = Username_;

- (void)dealloc
{
    self.UserID = nil;
    self.Comment = nil;
    self.Username = nil;
    //[super dealloc];
}

@end
