//
//  dealDetails.m
//  StratPromo
//
//  Created by cat on 14/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "dealDetails.h"

@interface dealDetails ()

@end

@implementation dealDetails

@synthesize ImageURL=ImageURL_;
@synthesize PromotionHeading = PromotionHeading_;
@synthesize PromotionDescription = PromotionDescription_;
@synthesize PromotionLocation = PromotionLocation_;
@synthesize PromotionArea = PromotionArea_;

@synthesize AddedDate = AddedDate_;
@synthesize PromotionStart = PromotionStart_;
@synthesize PromotionEnd = PromotionEnd_;

@synthesize Comments = Comments_;

@synthesize PromoID = PromoID_;
@synthesize MerchantID = MerchantID_;
@synthesize CategoryID = CategoryID_;
@synthesize CardID = CardID_;
@synthesize DiscountTypeID = DiscountTypeID_;
@synthesize RewardPoints = RewardPoints_;
@synthesize OriginalPrice = OriginalPrice_;
@synthesize PromotionPrice = PromotionPrice_;
@synthesize Rating = Rating_;

-(void)dealloc{
    
    
    self.PromoID=nil;
    self.MerchantID=nil;
    self.CategoryID=nil;
    self.CardID=nil;
    self.DiscountTypeID=nil;
    self.ImageURL=nil;
    self.RewardPoints=nil;
    self.PromotionHeading=nil;
    self.PromotionDescription=nil;
    self.PromotionLocation=nil;
    self.PromotionArea=nil;
    self.OriginalPrice=nil;
    self.PromotionPrice=nil;
    self.Rating=nil;
    self.AddedDate=nil;
    self.PromotionStart=nil;
    self.PromotionEnd=nil;
    self.Comments=nil;
        
    
    //[super dealloc];
    
}

@end
