//
//  RateDealAPI.m
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "RateDealAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "RateDeal_RequestBuilder.h"

@implementation RateDealAPI

@synthesize rateDelegate = rateDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}


- (void)performRateForPromoID:(NSString *)PromoID withUserToken:(NSString *)token andRating:(NSString *)Rating {
    
    [self retain];
	service_.serviceKey = RATING_KEY;
	NSString * soapBody = [RateDeal_RequestBuilder RateForPromoID:PromoID withToken:token andRating:Rating];
    //NSLog(@"soapBody: %@",soapBody);
    //NSLog(@"SYNC_KEY: %@",SYNC_KEY);
    //NSLog(@"AUTHENTICATE_URL: %@",AUTHENTICATE_URL);
    //NSLog(@"SYNC_SOAP_ACTION: %@",SYNC_SOAP_ACTION);
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:RATING_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
    
    if ([responseData objectForKey:RATING_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:RATING_SOAP_ACTION];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"RateDealResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            
            if (self.rateDelegate) {
                
                if ([self.rateDelegate respondsToSelector:@selector(api:didFinishRatingWithStatus:)]) {
                    
                    [self.rateDelegate api:self didFinishRatingWithStatus:YES];
                }
            }
        } else {
            
            if (self.rateDelegate) {
                
                if ([self.rateDelegate respondsToSelector:@selector(api:didFailRatingWithError:)]) {
                    [self.rateDelegate api:self didFailRatingWithError:errorReason];
                }
            }
        }
        
    } else {
        
        if (self.rateDelegate) {
            if ([self.rateDelegate respondsToSelector:@selector(api:didFailRatingWithError:)]) {
                
                [self.rateDelegate api:self didFailRatingWithError:@"Unknown Error"];
            }
        }
    }
    
    
    
    [responseData release];
    [self release];
}


-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.rateDelegate) {
		if ([self.rateDelegate respondsToSelector:@selector(api:didFailRatingWithError:)]) {
			[self.rateDelegate api:self didFailRatingWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.rateDelegate = nil;
	[super dealloc];
}


@end
