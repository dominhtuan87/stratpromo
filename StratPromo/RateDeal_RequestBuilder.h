//
//  RateDeal_RequestBuilder.h
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RateDeal_RequestBuilder : NSObject{
}

+(NSString*)RateForPromoID:(NSString *)PromoID withToken:(NSString *)token andRating: (NSString*)Rating;




@end
