//
//  QRReader_SuperViewController.h
//  StratPromo
//
//  Created by cat on 10/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface QRReader_SuperViewController : UIViewController {
    IBOutlet UITabBarController *tbc;
}


@property (nonatomic,retain) IBOutlet UITabBarController *tbc;

@end
