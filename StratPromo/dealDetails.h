//
//  dealDetails.h
//  StratPromo
//
//  Created by cat on 14/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dealDetails :  NSObject{
    
    NSString *PromoID_;
    NSString *MerchantID_;
    NSString *CategoryID_;
    NSString *CardID_;
    NSString *DiscountTypeID_;
    
    NSString *RewardPoints_;
    
    NSString *ImageURL_;
    NSString *PromotionHeading_;
    NSString *PromotionDescription_;
    NSString *PromotionLocation_;
    NSString *PromotionArea_;
    
    NSString *OriginalPrice_;
    NSString *PromotionPrice_;
    NSString *Rating_;
    
    NSString *AddedDate_;
    NSString *PromotionStart_;
    NSString *PromotionEnd_;
    
    NSArray *Comments_;
      
}

@property(nonatomic, retain) NSString *PromoID;
@property(nonatomic, retain) NSString *MerchantID;
@property(nonatomic, retain) NSString *CategoryID;
@property(nonatomic, retain) NSString *CardID;
@property(nonatomic, retain) NSString *DiscountTypeID;

@property(nonatomic, retain) NSString *RewardPoints;

@property(nonatomic, retain) NSString *ImageURL;
@property(nonatomic, retain) NSString *PromotionHeading;
@property(nonatomic, retain) NSString *PromotionDescription;
@property(nonatomic, retain) NSString *PromotionLocation;
@property(nonatomic, retain) NSString *PromotionArea;

@property(nonatomic, retain) NSString *OriginalPrice;
@property(nonatomic, retain) NSString *PromotionPrice;
@property(nonatomic, retain) NSString *Rating;

@property(nonatomic, retain) NSString *AddedDate;
@property(nonatomic, retain) NSString *PromotionStart;
@property(nonatomic, retain) NSString *PromotionEnd;

@property(nonatomic, retain) NSArray *Comments;

@end
