//
//  StratPromo_AppDelegate.h
//  StratPromo
//
//  Created by cat on 19/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>


@class StratPromo_ViewController;

@interface StratPromo_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, readonly) UINavigationController *mainViewController;
@property (strong, nonatomic) StratPromo_ViewController *viewController;

@end
