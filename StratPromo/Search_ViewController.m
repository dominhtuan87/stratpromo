//
//  Search_ViewController.m
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "Search_ViewController.h"
#import "Category_Result_ViewController.h"
#import "Constant.h"
#import "CardsFilter_ViewController.h"
#import "BankFilter_ViewController.h"
#import "AreasFilter_ViewController.h"
#import "Discount_ViewController.h"

@interface Search_ViewController ()

@end

@implementation Search_ViewController
@synthesize searchField,activity1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //activity1 = [ActivityView alloc];
}


- (IBAction)submitButtonPressed:(id)sender{
    
    NSLog(@"searchField: %@",searchField.text);
    
    if([searchField.text length]<3){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @""
                              message: @"Search term needs at least 3 characters"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        [self searchAction];
    }
}


- (void)searchAction {
    
    [activity1 presentActivityView:SEARCH_LOADING];
    SearchAPI *searchAPI = [[SearchAPI alloc] init];
    searchAPI.searchDelegate = self;
    //NSLog(@"[Utility getUserToken]: %@", [Utility getUserToken]);
    [searchAPI performSearchTerm:searchField.text atPageNumber:@"1"];
    //[syncAPI release];
}

-(void)api:(id)sender didFinishSearchWithStatus:(BOOL)status andDataArray:(NSMutableArray *)dataArray
{
    //NSLog(@"SEARCHING TERM COMPLETED");
    
    //NSLog(@"SEARCHING TERM COMPLETED dataArray:%@",dataArray);
    
    //[self prepareDataForTableView];
    
    [activity1 dismissActivityView];
    
    Category_Result_ViewController *resultViewController = [[Category_Result_ViewController alloc] initWithNibName:@"Category_Result_ViewController" bundle:nil];
    resultViewController.dataArray = [[NSMutableArray alloc] initWithArray:dataArray];
    //resultViewController.dataArray = dataArray;
    
    searchField.text = @"";
    [self.navigationController pushViewController:resultViewController animated:YES];
}

-(void)api:(id)sender didFailSearchWithError:(NSString *)error{
    
    [activity1 dismissActivityView];
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @""
                          message: error
                          delegate: nil
                          cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    //[alert release];
}



- (IBAction)cardsButtonPressed:(id)sender{
    CardsFilter_ViewController *cardFilterViewController = [[CardsFilter_ViewController alloc] initWithNibName:@"CardsFilter_ViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:cardFilterViewController animated:YES];
}


- (IBAction)banksButtonPressed:(id)sender{
    BankFilter_ViewController *bankFilterViewController = [[BankFilter_ViewController alloc] initWithNibName:@"BankFilter_ViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:bankFilterViewController animated:YES];

}


- (IBAction)areasButtonPressed:(id)sender{
    AreasFilter_ViewController *areaViewController = [[AreasFilter_ViewController alloc] initWithNibName:@"AreasFilter_ViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:areaViewController animated:YES];
}

- (IBAction)discountsButtonPressed:(id)sender{
    Discount_ViewController *discountViewController = [[Discount_ViewController alloc] initWithNibName:@"Discount_ViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:discountViewController animated:YES];

}

-(IBAction)backgroundTouched:(id)sender
{
    
    [searchField resignFirstResponder];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
