//
//  NearbyPromo_ViewController.h
//  StratPromo
//
//  Created by cat on 25/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface NearbyPromo_ViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate,MKAnnotation>{
    CLLocationManager *locationManager;
    MKPointAnnotation *annotationPoint;
    
    CLLocationCoordinate2D coordinate;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

// add an init method so you can set the coordinate property on startup
- (id) initWithCoordinate:(CLLocationCoordinate2D)coord;

@property (weak,nonatomic) IBOutlet MKMapView *DealMap;

@property (strong, nonatomic) CLLocation *poiLoc;
@property (strong, nonatomic) NSString *promotionLocationString;
@property (strong, nonatomic) NSString *merchantAddress;
@property (strong, nonatomic) NSString *promoHeading;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

-(IBAction)mapButtonPressed:(id)sender;

@end
