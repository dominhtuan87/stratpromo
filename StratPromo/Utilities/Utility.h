//
//  Utility.h
//
//  Created by user on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject {
    
}

+(BOOL)isNetworkAvailable;
+(NSString *)absoluteValueForString:(NSString *)string;
+(void)showAlertViewWithMessage:(NSString *)message;
+(void)showAlertViewWithTitle:(NSString *)title andMessage:(NSString *)message;
+(void)showAlertViewWithMessage:(NSString *)message withDelegate:(id)delegate;
+(void)setUserToken:(NSString *)token;
+(NSString *)getUserToken;
+(void)removeUserToken;
+(void)setUserDefaultsForStartScreen;
+(BOOL)isStartScreenLaunched;
+(void)setStatusForStartUPScreen:(BOOL)status;
+(BOOL)isStartUPScreenShowed;
+(void)setDeviceToken:(NSString *)deviceToken;
+(NSString *)getDeviceToken;
+(void)setRememberUserInfo:(NSDictionary *)userdetails;
+(NSDictionary *)getUserInfo;
+(void)removeUserInfo;
+(BOOL)isValidEmail:(NSString*)email;

@end
