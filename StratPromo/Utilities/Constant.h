//
//  Constant.h
//
//  Created by User on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define NETWORK_CONNECTION_FAIL @"No network connection. Please try again."  
#define POST_METHOD @"POST"
#define CONTENT_TYPE @"Content-Type"
#define XML_CONTENT_TYPE_VALUE @"text/xml; charset=utf-8"
#define LOGIN_KEY @"Login"
//#define AUTHENTICATE_URL @"http://cluebox.tk/service.asmx"
#define AUTHENTICATE_URL @"http://stratpromo.tk/service.asmx"

#define DB_NAME @"StratPromoDB.sqlite"

#define LOGIN_SOAP_ACTION @"http://stratpromo.tk/Login"
#define IS_LOGGEDIN_SOAP_ACTION @"http://stratpromo.tk/IsLoggedIn"
#define IS_LOGGEDIN_KEY @"IsLoggedIn"
#define TRUE_STATUS @"true"
#define FALSE_STATUS @"false"

#define ACTIVITY_INDICATOR_MESSAGE @"Please wait"
#define LOGIN_LOADING @"Logging you in Reward World ..."
#define FORGET_LOADING @"Generating New Password ..."
#define SIGNUP_LOADING @"Creating your account ..."
#define SYNC_LOADING @"Getting Promotions ..."
#define LOGOUT_LOADING @"Logging you out! See You again ..."
#define PROFILE_LOADING @"Loading Your Profile ..."
#define UPDATE_PROFILE_LOADING @"Updating Your Profile ..."
#define CHANGE_PASSWORD_LOADING @"Changing Your Password ..."
#define REFER_LOADING @"Referring Your Friends ..."
#define SURVEY_LOADING @"Loading Your Surveys ..."
#define TRACKER_LOADING @"Loading Your Trackers ..."
#define FORUM_LOADING @"Loading Your Forum ..."
#define MY_COMPLETION_LOADING @"Loading Your Completions ..."
#define REDEEM_LOADING @"Loading Redeemable Prizes ..."
#define COMMENT_LOADING @"Submitting your comment ..."
#define SEARCH_LOADING @"Searching ..."

#define LOGIN_AGAIN_KEY @"LoginAgain"
#define LOGIN_AGAIN_SOAP_ACTION @"http://stratpromo.tk/LoginAgain"
#define LOGOUT_KEY @"Logout"
#define LOGOUT_SOAP_ACTION @"http://stratpromo.tk/Logout"
#define FORGOT_PASSWORD_KEY @"ForgetPassword"
#define FORGOT_PASSWORD_SOAP_ACTION @"http://stratpromo.tk/ForgetPassword"

#define SYNC_KEY @"sync"
#define SYNC_SOAP_ACTION @"http://stratpromo.tk/Sync"
#define SYNCMASTER_KEY @"syncmaster"
#define SYNCMASTER_SOAP_ACTION @"http://stratpromo.tk/SyncMaster"

#define COMMENT_KEY @"CommentDeal"
#define COMMENT_SOAP_ACTION @"http://stratpromo.tk/CommentDeal"

#define RATING_KEY @"RateDeal"
#define RATING_SOAP_ACTION @"http://stratpromo.tk/RateDeal"

#define SEARCH_KEY @"Search"
#define SEARCH_SOAP_ACTION @"http://stratpromo.tk/Search"

#define SIGNUP_KEY @"Signup"
#define SIGNUP_SOAP_ACTION @"http://stratpromo.tk/Signup"
#define CONFIRM_REFERAL_KEY @"ConfirmReferral"
#define CONFIRM_REFERAL_SOAP_ACTION @"http://stratpromo.tk/ConfirmReferral"
#define GET_PROFILE_KEY @"GetProfile"
#define GET_PROFILE_SOAP_ACTION @"http://stratpromo.tk/GetProfile"
#define UPDATE_PROFILE_KEY @"UpdateProfile"
#define UPDATE_PROFILE_SOAP_ACTION @"http://stratpromo.tk/UpdateProfile"
#define RESET_PASSWORD_KEY @"resetPassword"
#define RESET_PASSWORD_SOAP_ACTION @"http://stratpromo.tk/resetPassword"
#define REFERRALS_KEY @"Referrals"
#define REFERRALS_SOAP_ACTION @"http://stratpromo.tk/Referrals"
