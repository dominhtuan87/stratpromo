//
//  Utility.m
//
//  Created by user on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Utility.h"
#import "Reachability.h"

static BOOL isLaunched;
static NSString * kDeviceToken;

@implementation Utility

+(void)setDeviceToken:(NSString *)deviceToken {
    
    kDeviceToken = deviceToken;
}

+(NSString *)getDeviceToken {
    
    return kDeviceToken;
}

+(void)setStatusForStartUPScreen:(BOOL)status {
    
    isLaunched = status;
}
+(BOOL)isStartUPScreenShowed {
    
    return isLaunched;
}

+(BOOL)isNetworkAvailable {
	
	Reachability * reachability = [Reachability reachabilityForInternetConnection];
	[reachability startNotifier];
	BOOL status = YES;
	if ([reachability currentReachabilityStatus]==kNotReachable) {
		status = NO;
	}
	[reachability stopNotifier];
	return status;
}

+(NSString *)absoluteValueForString:(NSString *)string {
	
	return (string!=nil)?string:@"";
}

+(void)showAlertViewWithMessage:(NSString *)message {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message
												   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	//[alert release];
}

+(void)showAlertViewWithTitle:(NSString *)title andMessage:(NSString *)message {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
												   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	//[alert release];
}

+(void)showAlertViewWithMessage:(NSString *)message withDelegate:(id)delegate {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message
												   delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	//[alert release];
}

+(void)setUserToken:(NSString *)token {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:token forKey:@"Token"];
    [defaults synchronize];
}

+(NSString *)getUserToken {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults valueForKey:@"Token"];
    return token;
}

+(void)removeUserToken {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"Token"];
    [defaults synchronize];
}

+(void)setRememberUserInfo:(NSDictionary *)userdetails {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:userdetails forKey:@"userDetails"];
    [defaults synchronize];
}

+(NSDictionary *)getUserInfo {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:[defaults valueForKey:@"userDetails"]];
    return userInfo;    
}

+(void)removeUserInfo {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"userDetails"];
    [defaults synchronize];
}

+(void)setUserDefaultsForStartScreen {
    
    [[NSUserDefaults standardUserDefaults]setValue:@"YES" forKey:@"startScreenLaunched"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(BOOL)isStartScreenLaunched {
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"startScreenLaunched"] isEqualToString:@"YES"]) 
        return YES;
    return NO;
}

+(BOOL)isValidEmail:(NSString*)email {
	//Quick return if @ Or . not in the string
	if([email rangeOfString:@"@"].location==NSNotFound || [email rangeOfString:@"."].location==NSNotFound)
		return NO;
	
	//Break email address into its components
	NSString *accountName=[email substringToIndex: [email rangeOfString:@"@"].location];
	email=[email substringFromIndex:[email rangeOfString:@"@"].location+1];
	
	//'.' not present in substring
	if([email rangeOfString:@"."].location==NSNotFound)
		return NO;
	NSString *domainName=[email substringToIndex:[email rangeOfString:@"."].location];
	NSString *subDomain=[email substringFromIndex:[email rangeOfString:@"."].location+1];
	
	//username, domainname and subdomain name should not contain the following charters below
	//filter for user name
	NSString *unWantedInUName = @" ~!@#$%^&*()={}[]|;':\"<>,+?/`";
	
	//filter for domain
	NSString *unWantedInDomain = @" ~!@#$%^&*()={}[]|;’:\"<>,+?/`";
	
	//filter for subdomain
	NSString *unWantedInSub = @" `~!@#$%^&*()={}[]:\";'<>,?/1234567890";
	
	//subdomain should not be less that 2 and not greater 6
	if(!(subDomain.length>=2 && subDomain.length<=6)) return NO;
	
	if([accountName isEqualToString:@""] || [accountName rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInUName]].location!=NSNotFound || [domainName isEqualToString:@""] || [domainName rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInDomain]].location!=NSNotFound || [subDomain isEqualToString:@""] || [subDomain rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInSub]].location!=NSNotFound)
		return NO;
	
	return YES;
}

@end
