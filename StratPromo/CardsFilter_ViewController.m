//
//  CardsFilter_ViewController.m
//  StratPromo
//
//  Created by cat on 30/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "CardsFilter_ViewController.h"
//#import "UIImage+animatedGIF.h"
#import "FMDatabase.h"
#import "Constant.h"
#import "Category_Result_ViewController.h"

@interface CardsFilter_ViewController ()

@end

@implementation CardsFilter_ViewController

@synthesize scrollView, contentView;

//@synthesize travelImageView;
//@synthesize beautyImageView;
//@synthesize wellnessImageView;
//@synthesize homeApplianceImageView;

//@synthesize shoppingImageView;
//@synthesize ITImageView;
//@synthesize diningImageView;
//@synthesize fashionImageView;

@synthesize travelButton;
@synthesize beautyButton;
@synthesize wellnessButton;
@synthesize homeApplianceButton;

@synthesize shoppingButton;
@synthesize ITButton;
@synthesize diningButton;
@synthesize fashionButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.scrollView addSubview:self.contentView];
    scrollView.frame = self.view.bounds;
    NSLog(@"scrollView.frame height: %f",scrollView.frame.size.height);
    NSLog(@"scrollView.frame width: %f",scrollView.frame.size.width);
    
    NSLog(@"contentView.frame height: %f",contentView.frame.size.height);
    NSLog(@"contentView.frame width: %f",contentView.frame.size.width);
    
    self.scrollView.contentSize = self.contentView.bounds.size;
    
    /*
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"travel" withExtension:@"gif"];
    //NSURL *url = [NSURL URLWithString:[@"http://www.picgifs.com/graphics/r/relax/graphics-relax-364258.gif" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    self.travelImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"beauty1" withExtension:@"gif"];
    self.beautyImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"wellness1" withExtension:@"gif"];
    self.wellnessImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"appliance" withExtension:@"gif"];
    self.homeApplianceImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    url = [[NSBundle mainBundle] URLForResource:@"shopping1" withExtension:@"gif"];
    self.shoppingImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"IT" withExtension:@"gif"];
    self.ITImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"dining" withExtension:@"gif"];
    self.diningImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"fashion1" withExtension:@"gif"];
    self.fashionImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    
    //url = [[NSBundle mainBundle] URLForResource:@"variableDuration" withExtension:@"gif"];
    //self.variableDurationImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    */
     
    NSLog(@"self.view.frame.size.height Categories: %f", self.view.frame.size.height);
    
    
    travelButton.tag = 1;
    beautyButton.tag = 2;
    wellnessButton.tag = 3;
    homeApplianceButton.tag = 7;
    
    shoppingButton.tag = 8;
    ITButton.tag = 9;
    diningButton.tag = 10;
    fashionButton.tag = 11;
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClicked:(id)sender {
    NSLog(@"Button pressed: %@", [sender currentTitle]);
    
    UIButton *selectedButton = (UIButton *)sender;
    NSLog(@"Selected button tag is %d", selectedButton.tag);
    
    if (selectedButton.selected) {
        NSLog(@"selectedButton.selected if: %c",selectedButton.selected );
        selectedButton.highlighted = NO;
        selectedButton.selected = NO;
        NSLog(@"selectedButton.selected if: %c",selectedButton.selected );
        //selectedButton.backgroundColor = [UIColor whiteColor];
        selectedButton.backgroundColor = [UIColor clearColor];
        selectedButton.alpha = 1;
    }
    else{
        NSLog(@"selectedButton.selected else: %c",selectedButton.selected );
        selectedButton.highlighted = YES;
        selectedButton.selected = YES;
        NSLog(@"selectedButton.selected else: %c",selectedButton.selected );
        selectedButton.backgroundColor = [UIColor grayColor];
        selectedButton.alpha = 0.3;
    }
}

- (IBAction)showOfferButtonPress:(id)sender {
    NSLog(@"showOfferButtonPress: %@", [sender currentTitle]);
    
    NSString *selectedCategories = @"WHERE";
    
    for(int i =1; i<12;i++){
        UIButton * button = (UIButton *)[self.view viewWithTag:i];
        if (button.selected) {
            NSLog(@"i: %d",i);
            selectedCategories = [selectedCategories stringByAppendingFormat:@" Promotions.CardID = %d OR",i ];
        }
    }
    if ([selectedCategories isEqualToString:@"WHERE"]){
        selectedCategories = @"";
    }
    else{
        NSLog(@"selectedCategories: %@",selectedCategories);
        
        selectedCategories = [selectedCategories substringToIndex:[selectedCategories length]-3];
        //selectedCategories = [selectedCategories stringByAppendingString:@";"];
        NSLog(@"selectedCategories: %@",selectedCategories);
        
    }
    
    NSLog(@"selectedCategories: %@",selectedCategories);
    
    //LOAD DB
    BOOL success = FALSE;
    //NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
    
    //NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
    
    
    FMDatabase *database = [FMDatabase databaseWithPath:path];
    
    database.traceExecution = YES;
    NSLog(@"Errors: %c",database.logsErrors);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    success = [fileManager fileExistsAtPath:path];
    
    if(success){
        NSLog(@"DB existed!");
        
    }
    
    if(![database open])
    {
        
        
        NSLog(@"Cant open DB");
        NSLog(@"Errors: %c",database.logsErrors);
        
    }
    
    
    if ([database hadError]) {
        NSLog(@"Err %d: %@", [database lastErrorCode], [database lastErrorMessage]);
    }
    
    NSString* query = [NSString stringWithFormat:@"SELECT COUNT (*) FROM promotions %@",selectedCategories];
    FMResultSet *results = [database executeQuery:query];
    
    NSString *count;
    
    //dataArray = [[NSMutableArray alloc] init];
    
    while([results next]) {
        
        count = [results stringForColumn:@"COUNT (*)"];
        NSLog(@"PromoName: %@",count);
    }
    
    NSLog(@"[PromoName length]: %d", [count length]);
    NSLog(@"[count string]: %@", count);
    
    if ([count isEqualToString:@"0"]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No promotion is available with selected Categories. Please try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
    else{
        
        Category_Result_ViewController *resultViewController = [[Category_Result_ViewController alloc] initWithNibName:@"Category_Result_ViewController" bundle:nil];
        resultViewController.dataArray = [[NSMutableArray alloc] init];
        
        query = [NSString stringWithFormat:@"SELECT * FROM Promotions INNER JOIN Categories ON Promotions.CategoryID = Categories.CategoryID INNER JOIN Cards ON Promotions.CardID = Cards.CardID %@ ORDER BY CardID ASC, AddedDate DESC;",selectedCategories];
        FMResultSet *results = [database executeQuery:query];
        
        //NSString *count;
        
        NSDictionary *lineItem;
        
        //dataArray = [[NSMutableArray alloc] init];
        
        while([results next]) {
            
            
            lineItem = @{@"PromoID" : [results stringForColumn:@"PromoID"],
                         @"PromotionHeading" : [results stringForColumn:@"PromotionHeading"],
                         @"PromotionDetails" : [results stringForColumn:@"PromotionDetails"],
                         @"CategoryName" : [results stringForColumn:@"CategoryName"],
                         @"CardName" : [results stringForColumn:@"CardName"],
                         @"ImageURL" : [results stringForColumn:@"ImageURL"],
                         @"ThumbnailImageURL" : [results stringForColumn:@"ThumbnailImageURL"],
                         @"PromotionLocation" : [results stringForColumn:@"PromotionLocation"]};
            [resultViewController.dataArray addObject:lineItem];
            
            
        }
        [self.navigationController pushViewController:resultViewController animated:YES];
        
    }
    [database close];
    
    
}

@end
