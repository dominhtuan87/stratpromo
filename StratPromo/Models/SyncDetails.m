//
//  SyncItem.m
//  ClueBox
//
//  Created by Rakesh Raveendran on 19/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SyncDetails.h"

@implementation SyncDetails

@synthesize name = name_;
@synthesize surveys = surveys_;
@synthesize trackers = trackers_;
@synthesize freetrials = freetrials_;
@synthesize totalPoints = totalPoints_;

-(void)dealloc {
    
    self.name = nil;
    self.surveys = nil;
    self.trackers = nil;
    self.freetrials = nil;
    self.totalPoints = nil;
    
    [super dealloc];
}

@end
