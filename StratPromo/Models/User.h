//
//  User.h
//  ClueBox
//
//  Created by user on 11/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject {
    
    NSString * userName_;
	NSString * password_;
    NSString * email_;
    NSString * token_;
    NSString * numberOfReferral_;
   
    NSMutableArray * ListEmails_;
    
    NSString * IsFirstTime_;
    
    NSString    *floor_;
    NSString    *unit_;
    NSString    *block_;
    NSString    *building_;
    NSString    *street_;
    NSString    *postalCode_;
    NSString    *country_;
    NSString    *contact_;
    
    NSString    *name_;
    NSString    *dob_;
    NSString    *gender_;
    NSString    *maritalStatus_;
    NSString    *education_;
    NSString    *occupation_;
    NSString    *race_;
    NSString    *religion_;
    
    
}

@property(nonatomic,retain) NSString * numberOfReferral;
@property(nonatomic,retain) NSString * token;
@property(nonatomic,retain) NSString * userName;
@property(nonatomic,retain) NSString * password;
@property(nonatomic,retain) NSString * email;
@property(nonatomic,retain) NSMutableArray * ListEmails;

@property(nonatomic,retain) NSString    *IsFirstTime;

@property(nonatomic,retain) NSString    *floor;
@property(nonatomic,retain) NSString    *unit;
@property(nonatomic,retain) NSString    *block;
@property(nonatomic,retain) NSString    *building;
@property(nonatomic,retain) NSString    *street;
@property(nonatomic,retain) NSString    *postalCode;
@property(nonatomic,retain) NSString    *country;
@property(nonatomic,retain) NSString    *contact;

@property(nonatomic,retain) NSString    *name;
@property(nonatomic,retain) NSString    *dob;
@property(nonatomic,retain) NSString    *gender;
@property(nonatomic,retain) NSString    *maritalStatus;
@property(nonatomic,retain) NSString    *education;
@property(nonatomic,retain) NSString    *occupation;
@property(nonatomic,retain) NSString    *race;
@property(nonatomic,retain) NSString    *religion;

+(User *)activeUser;

@end
