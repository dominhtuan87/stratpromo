//
//  SyncItem.h
//  ClueBox
//
//  Created by Rakesh Raveendran on 19/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SyncDetails : NSObject {
    
    NSString *name_;
    NSString *surveys_;
    NSString *trackers_;
    NSString *freetrials_;
    NSString *totalPoints_;
}

@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSString *surveys;
@property(nonatomic, retain) NSString *trackers;
@property(nonatomic, retain) NSString *freetrials;
@property(nonatomic, retain) NSString *totalPoints;

@end
