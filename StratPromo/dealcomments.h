//
//  dealcomments.h
//  StratPromo
//
//  Created by cat on 16/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dealcomments : NSObject {
    
    NSString * UserID_;
    NSString * Comment_;
    NSString * Username_;
}


@property(nonatomic,retain) NSString * UserID;
@property(nonatomic,retain) NSString * Comment;
@property(nonatomic,retain) NSString * Username;

@end
