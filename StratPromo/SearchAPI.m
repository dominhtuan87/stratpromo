//
//  SearchAPI.m
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "SearchAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "SearchRequestBuilder.h"
#import "FMDatabase.h"

@interface SearchAPI (Private)

-(NSArray *)PromotionsFromElement:(TBXMLElement *)rootElement;
//-(void)CommentsFromElement:(TBXMLElement *)rootElement withPromoID:(NSString*)PromoID andDB:(FMDatabase*)database;

@end


@implementation SearchAPI

@synthesize searchDelegate = searchDelegate_;
@synthesize dataArray;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}

- (void)performSearchTerm:(NSString *)SearchTerm atPageNumber:(NSString *)PageNumber{
    
    [self retain];
    
    dataArray = [[NSMutableArray alloc] init];
    
	service_.serviceKey = SEARCH_KEY;
	NSString * soapBody = [SearchRequestBuilder SearchTerm:SearchTerm atPageNumber:PageNumber];
    //NSLog(@"soapBody: %@",soapBody);
    //NSLog(@"SYNC_KEY: %@",SYNC_KEY);
    //NSLog(@"AUTHENTICATE_URL: %@",AUTHENTICATE_URL);
    //NSLog(@"SYNC_SOAP_ACTION: %@",SYNC_SOAP_ACTION);
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:SEARCH_SOAP_ACTION];
    
    //return dataArray;
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    //NSLog(@"SearchrequestSuccessfullyCompleted");
    
    NSArray *promotions = [[NSArray alloc] init];
    NSDictionary *responseData = [result retain];
    //NSLog(@"Search response data is ===== %@",responseData);
	
    
    
    if ([responseData objectForKey:SEARCH_KEY]) {
        
        //dealDetails *dealDetail = [[dealDetails alloc] init];
        CBXML * parser = (CBXML *)[responseData objectForKey:SEARCH_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"SearchResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        TBXMLElement *NewPromotions = [TBXML childElementNamed:@"NewPromotions" parentElement:childeElement];
        
        
        
        if (errorOnFailure) {
            errorReason = [TBXML textForElement:errorOnFailure];
            //NSLog(@"Search [TBXML textForElement:errorOnFailure]: %@",[TBXML textForElement:errorOnFailure]);
        }
        //NSLog(@"Search [TBXML textForElement:status]: %@",[TBXML textForElement:status]);
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            //promotions = [NSArray arrayWithArray:[self PromotionsFromElement:NewPromotions]];
            promotions = [[[self PromotionsFromElement:NewPromotions] copy] autorelease];
            
            for(int i=0; i<[promotions count];i++){
                //NSLog(@"Search [promotions[i] count] API: %d", [promotions[i] count]);
            }
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            //NSLog(@"Search TRUE");
            if (self.searchDelegate) {
                //NSLog(@" Search TRUE IF 1");
                if ([self.searchDelegate respondsToSelector:@selector(api:didFinishSearchWithStatus:andDataArray:)]) {
                    //NSLog(@"Search TRUE IF 2");
                    NSLog(@"Search dataArray return: %@",dataArray);
                    [self.searchDelegate api:self didFinishSearchWithStatus:YES andDataArray:dataArray];
                }
            }
        } else {
            //NSLog(@"Search FALSE");
            if (self.searchDelegate) {
                if ([self.searchDelegate respondsToSelector:@selector(api:didFailSearchWithError:)]) {
                    //NSLog(@"Search errorReason: %@",errorReason);
                    [self.searchDelegate api:self didFailSearchWithError:errorReason];
                }
            }
        }
        
        //NSLog(@"Search PUTSIDE TRUE");
        //[dealDetail release];
    }
    //NSLog(@"Search BEFORE RELEASING");
    
    
    //[promotions release];
    [responseData release];
    [self release];
}

-(NSArray *)PromotionsFromElement:(TBXMLElement *)rootElement{
    if (rootElement) {
        
        TBXMLElement * promotionsElement = rootElement->firstChild;
        
        NSString * tempCategory;
        NSString * tempCard;
        
        NSDictionary *lineItem;
        
        //SAVE TO DB
        BOOL success = FALSE;
        //NSError *error = nil;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsPath = [paths objectAtIndex:0];
        NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
        
        //NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
        
        FMDatabase *database = [FMDatabase databaseWithPath:path];
        
        database.traceExecution = YES;
        //NSLog(@"Errors: %c",database.logsErrors);
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        success = [fileManager fileExistsAtPath:path];
        
        if(success){
            NSLog(@"DB existed!");
            
        }
        
        if(![database open])
        {
            
            
            NSLog(@"Cant open DB");
            NSLog(@"Errors: %c",database.logsErrors);
            
        }
        
        
        if ([database hadError]) {
            NSLog(@"Err %d: %@", [database lastErrorCode], [database lastErrorMessage]);
        }
        
        
        
        
        FMResultSet *results;
        
        while (promotionsElement) {
            
            TBXMLElement * PromoID = [TBXML childElementNamed:@"PromoID" parentElement:promotionsElement];
            //TBXMLElement * MerchantID = [TBXML childElementNamed:@"MerchantID" parentElement:promotionsElement];
            TBXMLElement * CategoryID = [TBXML childElementNamed:@"CategoryID" parentElement:promotionsElement];
            TBXMLElement * CardID = [TBXML childElementNamed:@"CardID" parentElement:promotionsElement];
            //TBXMLElement * DiscountTypeID = [TBXML childElementNamed:@"DiscountTypeID" parentElement:promotionsElement];
            
            TBXMLElement * ImageURL = [TBXML childElementNamed:@"ImageURL" parentElement:promotionsElement];
            TBXMLElement * ThumbnailImageURL = [TBXML childElementNamed:@"ThumbnailImageURL" parentElement:promotionsElement];
            //TBXMLElement * ProductImageURL = [TBXML childElementNamed:@"ProductImageURL" parentElement:promotionsElement];
            
            //TBXMLElement * RewardPoints = [TBXML childElementNamed:@"RewardPoints" parentElement:promotionsElement];
            TBXMLElement * PromotionHeading = [TBXML childElementNamed:@"PromotionHeading" parentElement:promotionsElement];
            TBXMLElement * PromotionDescription = [TBXML childElementNamed:@"PromotionDescription" parentElement:promotionsElement];
            TBXMLElement * PromotionLocation = [TBXML childElementNamed:@"PromotionLocation" parentElement:promotionsElement];
            //TBXMLElement * PromotionArea = [TBXML childElementNamed:@"PromotionArea" parentElement:promotionsElement];
            //TBXMLElement * OriginalPrice = [TBXML childElementNamed:@"OriginalPrice" parentElement:promotionsElement];
            //TBXMLElement * PromotionPrice = [TBXML childElementNamed:@"PromotionPrice" parentElement:promotionsElement];
            //TBXMLElement * AddedDate = [TBXML childElementNamed:@"AddedDate" parentElement:promotionsElement];
            //TBXMLElement * PromotionStart = [TBXML childElementNamed:@"PromotionStart" parentElement:promotionsElement];
            //TBXMLElement * PromotionEnd = [TBXML childElementNamed:@"PromotionEnd" parentElement:promotionsElement];
            //TBXMLElement * Rating = [TBXML childElementNamed:@"Rating" parentElement:promotionsElement];
            //TBXMLElement * Comments = [TBXML childElementNamed:@"Comments" parentElement:promotionsElement];
            
            NSLog(@"[TBXML textForElement:PromoID]: %@",[TBXML textForElement:PromoID]);
            NSLog(@"[TBXML textForElement:CategoryID]: %@",[TBXML textForElement:CategoryID]);
            NSLog(@"[TBXML textForElement:CardID]: %@",[TBXML textForElement:CardID]);
                        
            results = [database executeQuery:@"SELECT CategoryName FROM Categories WHERE CategoryID = ?",[TBXML textForElement:CategoryID]];
            
            if ([results next]) {
                tempCategory = [results stringForColumn:@"CategoryName"];
            }
            
            results = [database executeQuery:@"SELECT CardName FROM Cards WHERE CardID = ?",[TBXML textForElement:CardID]];
            
            if ([results next]) {
                tempCard = [results stringForColumn:@"CardName"];
            }
            
            
            lineItem = @{@"PromoID" : [TBXML textForElement:PromoID],
                         @"PromotionHeading" : [TBXML textForElement:PromotionHeading],
                         @"PromotionDetails" : [TBXML textForElement:PromotionDescription],
                         @"CategoryName" : tempCategory,
                         @"CardName" : tempCard,
                         @"ImageURL" : [TBXML textForElement:ImageURL],
                         @"ThumbnailImageURL" : [TBXML textForElement:ThumbnailImageURL],
                         @"PromotionLocation" : [TBXML textForElement:PromotionLocation]};
            
            
            [dataArray addObject:lineItem];
            
            //success = [database executeUpdate:@"INSERT INTO Promotions (PromoID,MerchantID,CategoryID,CardID,DiscountTypeID,ImageURL,RewardPoints,PromotionHeading,PromotionDetails,PromotionLocation,PromotionAreaID,OriginalPrice,PromotionPrice,AddedDate,PromoStartDate,PromoEndDate,Rating,ThumbnailImageURL,ProductImageURL) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[NSNumber numberWithInt:[[TBXML textForElement:PromoID] integerValue]],[NSNumber numberWithInt:[[TBXML textForElement:MerchantID] integerValue]],[NSNumber numberWithInt:[[TBXML textForElement:CategoryID] integerValue]],[NSNumber numberWithInt:[[TBXML textForElement:CardID] integerValue]],[NSNumber numberWithInt:[[TBXML textForElement:DiscountTypeID] integerValue]],[TBXML textForElement:ImageURL],[NSNumber numberWithInt:[[TBXML textForElement:RewardPoints] integerValue]],[TBXML textForElement:PromotionHeading],[TBXML textForElement:PromotionDescription],[TBXML textForElement:PromotionLocation],[NSNumber numberWithInt:[[TBXML textForElement:PromotionArea] integerValue]],[NSNumber numberWithFloat:[[TBXML textForElement:OriginalPrice] floatValue]],[NSNumber numberWithFloat:[[TBXML textForElement:PromotionPrice] floatValue]],[TBXML textForElement:AddedDate],[TBXML textForElement:PromotionStart],[TBXML textForElement:PromotionEnd],[NSNumber numberWithFloat:[[TBXML textForElement:Rating] floatValue]],[TBXML textForElement:ThumbnailImageURL],[TBXML textForElement:ProductImageURL],nil];
            
            
            NSLog(@"Search dataArray: %@",dataArray);
            
            promotionsElement = promotionsElement->nextSibling;
            
        }
        
        
        
        [lineItem autorelease];
        
        [database close];
        //[tempCategory autorelease];
        //[tempCard autorelease];
        //[lineItem autorelease];
        //[paths autorelease];
        //[path autorelease];
        //[docsPath autorelease];
        
    }
    return nil;
}


-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.searchDelegate) {
		if ([self.searchDelegate respondsToSelector:@selector(api:didFailSearchWithError:)]) {
			[self.searchDelegate api:self didFailSearchWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.searchDelegate = nil;
	[super dealloc];
}


@end
