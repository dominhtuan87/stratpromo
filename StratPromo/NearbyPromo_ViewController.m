//
//  NearbyPromo_ViewController.m
//  StratPromo
//
//  Created by cat on 25/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "NearbyPromo_ViewController.h"


@interface NearbyPromo_ViewController ()

@end

@implementation NearbyPromo_ViewController

@synthesize poiLoc,promotionLocationString,coordinate,merchantAddress,promoHeading,title,subtitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    
}

- (id) initWithCoordinate:(CLLocationCoordinate2D)coord
{
    coordinate = coord;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"MapView poiLoc: %@",poiLoc);
    
    self.DealMap.delegate = self;
    
    NearbyPromo_ViewController * annotation = [[NearbyPromo_ViewController alloc] initWithCoordinate:poiLoc.coordinate];
    annotation.title = promoHeading;
    annotation.subtitle = merchantAddress;
    //annotation.subtitle = @"check \n check";
    
    [self.DealMap addAnnotation:annotation];
    
    /*locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    [locationManager startUpdatingLocation];
    
    while([locationManager location] == nil){
        NSLog(@"Waiting for location ...");
    }
    //locationManager.StopUpdatingLocation();
    
    [locationManager stopUpdatingLocation];
     
    CLLocationCoordinate2D location = [locationManager location].coordinate;
    
    NSLog(@"Current latitude: %f", [locationManager location].coordinate.latitude);
    NSLog(@"Current longitude: %f", [locationManager location].coordinate.longitude);
    
    // 1
    //CLLocationCoordinate2D zoomLocation;
    //zoomLocation.latitude = 39.281516;
    //zoomLocation.longitude= -76.580806;
    
    
    
    CLLocationCoordinate2D location = poiLoc.coordinate;
    
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location, 5000,5000);
    
    // 3
    [self.DealMap setRegion:viewRegion animated:YES];
     
     */
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)mapButtonPressed:(id)sender
{
    
    //NearbyPromo_ViewController *mapViewController = [[NearbyPromo_ViewController alloc] initWithNibName:@"NearbyPromo_ViewController_iPhone" bundle:nil];
    //[self.navigationController pushViewController:mapViewController animated:YES];
    
    //mapViewController.poiLoc = [[CLLocation alloc] initWithLatitude:[[[promotionLocationString componentsSeparatedByString:@","] objectAtIndex:0] floatValue] longitude:[[[promotionLocationString componentsSeparatedByString:@","] objectAtIndex:1] floatValue]];
    
    //NSLog(@"DetailView poiLoc: %@", mapViewController.poiLoc);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please choose"
                                                    message:@"Which map provider do you want to launch?"
                                                   delegate:self
                                          cancelButtonTitle:@"Google"
                                          otherButtonTitles:@"Apple(ios6 & up)", nil];
    [alert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Clicked button index 0");
        if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]){
            NSString *url = [NSString stringWithFormat: @"comgooglemaps://?q=%@&center=%@&zoom=12",[promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            NSLog(@"url: %@",url);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            
            
        }
        
        else{
            NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?q=%@",
                             [promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }
    } else {
        NSLog(@"Clicked button index other than 0");
        // Add another action here
        
        NSString *url = [NSString stringWithFormat: @"http://maps.apple.com/?q=%@",
                         [promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}


@end
