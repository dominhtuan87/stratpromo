//
//  CommentDealAPI.m
//  StratPromo
//
//  Created by Mac OS cat on 26/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "CommentDealAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "CommentDeal_RequestBuilder.h"

@interface CommentDealAPI (Private)

//-(NSArray *)PromotionsFromElement:(TBXMLElement *)rootElement;
//-(void)CommentsFromElement:(TBXMLElement *)rootElement withPromoID:(NSString*)PromoID andDB:(FMDatabase*)database;

@end

@implementation CommentDealAPI

@synthesize commentDelegate = commentDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}


- (void)performCommentForPromoID:(NSString *)PromoID withUserToken:(NSString *)token andComment:(NSString *)comment{
    
    [self retain];
	service_.serviceKey = COMMENT_KEY;
	NSString * soapBody = [CommentDeal_RequestBuilder CommentForPromoID:PromoID withToken:token andComment:comment];
    //NSLog(@"soapBody: %@",soapBody);
    //NSLog(@"SYNC_KEY: %@",SYNC_KEY);
    //NSLog(@"AUTHENTICATE_URL: %@",AUTHENTICATE_URL);
    //NSLog(@"SYNC_SOAP_ACTION: %@",SYNC_SOAP_ACTION);
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:COMMENT_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
    
    if ([responseData objectForKey:COMMENT_KEY]) {
        
        CBXML * parser = (CBXML *)[responseData objectForKey:COMMENT_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"CommentDealResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        
        if (errorOnFailure) {
            
            errorReason = [TBXML textForElement:errorOnFailure];
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            
            if (self.commentDelegate) {
                
                if ([self.commentDelegate respondsToSelector:@selector(api:didFinishCommentWithStatus:)]) {
                    
                    [self.commentDelegate api:self didFinishCommentWithStatus:YES];
                }
            }
        } else {
            
            if (self.commentDelegate) {
                
                if ([self.commentDelegate respondsToSelector:@selector(api:didFailCommentWithError:)]) {
                    [self.commentDelegate api:self didFailCommentWithError:errorReason];
                }
            }
        }
        
    } else {
        
        if (self.commentDelegate) {
            if ([self.commentDelegate respondsToSelector:@selector(api:didFailCommentWithError:)]) {
                
                [self.commentDelegate api:self didFailCommentWithError:@"Unknown Error"];
            }
        }
    }
    
    
    
    [responseData release];
    [self release];
}


-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.commentDelegate) {
		if ([self.commentDelegate respondsToSelector:@selector(api:didFailCommentWithError:)]) {
			[self.commentDelegate api:self didFailCommentWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.commentDelegate = nil;
	[super dealloc];
}

@end