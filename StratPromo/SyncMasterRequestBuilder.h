//
//  SyncMasterRequestBuilder.h
//  StratPromo
//
//  Created by cat on 22/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utility.h"


@interface SyncMasterRequestBuilder : NSObject {
    
}

+(NSString*)syncMasterWithTableName:(NSString *)TableName andKey:(NSString*)Key;
@end
