//
//  SyncAPI.h
//  ClueBox
//
//  Created by Rakesh Raveendran on 19/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SOAPWebService.h"
//#import "dealDetails.h"

@protocol SyncAPIDelegate <NSObject>

@optional

-(void)api:(id)sender didFinishSyncWithStatus:(BOOL)status;
-(void)api:(id)sender didFailSyncWithError:(NSString*)error;

@end

@interface SyncAPI : NSObject <SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <SyncAPIDelegate> * syncDelegate_;
}

@property(nonatomic,assign) NSObject <SyncAPIDelegate> * syncDelegate;

- (void)performSyncWithToken:(NSString *)token andPromoID: (NSString*)PromoID;

@end
