//
//  LeftViewController.h
//  ClueBox
//
//  Created by cat on 12/11/12.
//
//

#import <UIKit/UIKit.h>

@interface LeftViewController : UIViewController

- (IBAction)loginButtonPressed:(id)sender;

@end
