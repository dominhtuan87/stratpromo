//
//  SearchRequestBuilder.m
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "SearchRequestBuilder.h"

@implementation SearchRequestBuilder

+(NSString*)SearchTerm:(NSString *)SearchTerm atPageNumber:(NSString *)PageNumber{
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"search" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__SearchTerm__"
																 withString:SearchTerm];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__PageNumber__"
																 withString:PageNumber];
        return contentFormat;
	}
	return @"";
}

@end
