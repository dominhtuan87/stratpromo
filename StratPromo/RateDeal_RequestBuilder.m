//
//  RateDeal_RequestBuilder.m
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "RateDeal_RequestBuilder.h"


@implementation RateDeal_RequestBuilder

+(NSString*)RateForPromoID:(NSString *)PromoID withToken:(NSString *)token andRating:(NSString *)Rating{
    
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Rating" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__"
																 withString:token];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__PromoID__"
																 withString:PromoID];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Rating__"
																 withString:Rating];
        
        return contentFormat;
	}
	return @"";
}@end
