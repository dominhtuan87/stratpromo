//
//  SyncRequestBuilder.m
//  ClueBox
//
//  Created by Rakesh Raveendran on 19/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SyncRequestBuilder.h"

@implementation SyncRequestBuilder

+(NSString*)syncForUserToken:(NSString *)token andPromoID:(NSString *)PromoID{
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Sync" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__" 
																 withString:([Utility getDeviceToken])?[Utility getDeviceToken]:@"abcd"];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__PromoID__"
																 withString:PromoID];
        return contentFormat;
	}
	return @"";
}

@end
