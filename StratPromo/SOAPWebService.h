//
//  SOAPWebService.h
//
//  Created by user on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"
#import "ASIHTTPRequest.h"

@protocol SOAPWebServiceDelegate <NSObject>

@optional

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result;
-(void)service:(id)sender requestFailedWithError:(NSString*)error;
-(void)service:(id)sender buildRequest:(id)request;

@end

@interface SOAPWebService : NSObject <ASIHTTPRequestDelegate> {
    
    
    NSString                    *serviceKey_;
    NSObject <SOAPWebServiceDelegate> *serviceDelegate_;
	ASIHTTPRequest				*request_;
}

@property(nonatomic,assign) NSObject <SOAPWebServiceDelegate> * serviceDelegate;
@property(nonatomic,assign) NSString *serviceKey;

- (void)doSOAPCallWithUrl:(NSString *)url andSOAPMessage:(NSString *)soapMessage andSOAPAction:(NSString *) action;

@end
