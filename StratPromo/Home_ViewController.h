//
//  Home_ViewController.h
//  StratPromo
//
//  Created by cat on 25/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPFoldEnumerations.h"
#import "MPFlipEnumerations.h"


@protocol MPModalViewControllerDelegate;
@class DetailViewController;

enum {
	MPTransitionModeFold,
	MPTransitionModeFlip
} typedef MPTransitionMode;

@interface Home_ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *latestDeals;
    NSMutableArray *hotDeals;
    
    IBOutlet UITableView *mainTable;
    NSMutableArray *dataArray;
    
}
@property (strong, nonatomic) NSMutableArray *latestDeals;
@property (strong, nonatomic) NSMutableArray *topRatedDeals;
@property (strong, nonatomic) NSMutableArray *mostViewsDeals;
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (assign, nonatomic) MPTransitionMode mode;

@property (assign, nonatomic) MPFoldStyle foldStyle;
@property (assign, nonatomic) MPFlipStyle flipStyle;
@property (readonly, nonatomic) BOOL isFold;

@property (readonly, nonatomic) IBOutlet UITableView *mainTable;
-(void)prepareDataForTableView;
@end