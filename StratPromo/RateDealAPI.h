//
//  RateDealAPI.h
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SOAPWebService.h"


@protocol RateDealAPIDelegate <NSObject>

@optional

-(void)api:(id)sender didFinishRatingWithStatus:(BOOL)status;
-(void)api:(id)sender didFailRatingWithError:(NSString*)error;

@end



@interface RateDealAPI : NSObject<SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <RateDealAPIDelegate> * rateDelegate_;
}

@property(nonatomic,assign) NSObject <RateDealAPIDelegate> * rateDelegate;

- (void)performRateForPromoID:(NSString *)PromoID withUserToken:(NSString*)token andRating:(NSString*)Rating;

@end
