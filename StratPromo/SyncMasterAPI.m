//
//  SyncMasterAPI.m
//  StratPromo
//
//  Created by cat on 22/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "SyncMasterAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "SyncMasterRequestBuilder.h"
//#import "dealcomments.h"
#import "FMDatabase.h"

@interface SyncMasterAPI (Private)

-(NSArray *)MasterDataFromElement:(TBXMLElement *)rootElement withTableName:(NSString*)tableName;
//-(void)CommentsFromElement:(TBXMLElement *)rootElement withPromoID:(NSString*)PromoID andDB:(FMDatabase*)database;

@end

@implementation SyncMasterAPI
@synthesize tableName;
@synthesize syncMasterDelegate = syncMasterDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}

- (void)performSyncMasterWithTableName:(NSString *)TableName andKey:(NSString*)Key {
    
    [self retain];
	service_.serviceKey = SYNCMASTER_KEY;
	NSString * soapBody = [SyncMasterRequestBuilder syncMasterWithTableName:TableName andKey:Key];
    //NSLog(@"soapBody: %@",soapBody);
    //NSLog(@"SYNC_KEY: %@",SYNC_KEY);
    //NSLog(@"AUTHENTICATE_URL: %@",AUTHENTICATE_URL);
    //NSLog(@"SYNC_SOAP_ACTION: %@",SYNC_SOAP_ACTION);
    tableName = [[NSString alloc] initWithString:TableName];
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:SYNCMASTER_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    NSArray *MasterData = [[NSArray alloc] init];
    NSDictionary *responseData = [result retain];
    NSLog(@"response data is ===== %@",responseData);
	
    if ([responseData objectForKey:SYNCMASTER_KEY]) {
        
        //dealDetails *dealDetail = [[dealDetails alloc] init];
        CBXML * parser = (CBXML *)[responseData objectForKey:SYNCMASTER_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"SyncMasterResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        TBXMLElement *NewSyncedData = [TBXML childElementNamed:@"NewSyncedData" parentElement:childeElement];
        
        
        
        if (errorOnFailure) {
            errorReason = [TBXML textForElement:errorOnFailure];
            //NSLog(@"[TBXML textForElement:errorOnFailure]: %@",[TBXML textForElement:errorOnFailure]);
        }
        //NSLog(@"[TBXML textForElement:status]: %@",[TBXML textForElement:status]);
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            //promotions = [NSArray arrayWithArray:[self PromotionsFromElement:NewPromotions]];
            MasterData = [[[self MasterDataFromElement:NewSyncedData] copy] autorelease];
            
            //for(int i=0; i<[promotions count];i++){
            //    NSLog(@"[promotions[i] count] API: %d", [promotions[i] count]);
            //}
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            //NSLog(@"TRUE");
            if (self.syncMasterDelegate) {
                //NSLog(@"TRUE IF 1");
                if ([self.syncMasterDelegate respondsToSelector:@selector(api:didFinishSyncMasterWithStatus:)]) {
                    //NSLog(@"TRUE IF 2");
                    [self.syncMasterDelegate api:self didFinishSyncMasterWithStatus:YES];
                }
            }
        } else {
            //NSLog(@"FALSE");
            if (self.syncMasterDelegate) {
                if ([self.syncMasterDelegate respondsToSelector:@selector(api:didFailSyncMasterWithError:)]) {
                    //NSLog(@"errorReason: %@",errorReason);
                    [self.syncMasterDelegate api:self didFailSyncMasterWithError:errorReason];
                }
            }
        }
        
        //NSLog(@"PUTSIDE TRUE");
        //[dealDetail release];
    }
    //NSLog(@"BEFORE RELEASING");
    
    
    //[promotions release];
    [responseData release];
    [self release];
}

-(NSArray *)MasterDataFromElement:(TBXMLElement *)rootElement{
    if (rootElement) {
        TBXMLElement * masterDataElement = rootElement->firstChild;
    
        NSMutableArray * MasterData = [[NSMutableArray alloc] init];
        
        
        //SAVE TO DB
        BOOL success = FALSE;
        //NSError *error = nil;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsPath = [paths objectAtIndex:0];
        NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
        
        NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
        
        FMDatabase *database = [FMDatabase databaseWithPath:path];
        
        database.traceExecution = YES;
        //NSLog(@"Errors: %c",database.logsErrors);
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        success = [fileManager fileExistsAtPath:path];
        
        if(success){
            NSLog(@"DB existed!");
            
        }
        
        if(![database open])
        {
            
            
            return nil;
            
        }
        
        
        if ([database hadError]) {
            NSLog(@"Err %d: %@", [database lastErrorCode], [database lastErrorMessage]);
        }
        
        
        
        while (masterDataElement) {
           /* TBXMLElement * Key = [TBXML childElementNamed:@"Key" parentElement:promotionsElement];
            TBXMLElement * Value = [TBXML childElementNamed:@"Value" parentElement:promotionsElement];
            TBXMLElement * CardProviderId = [TBXML childElementNamed:@"CardProviderId" parentElement:promotionsElement];
            TBXMLElement * UserID = [TBXML childElementNamed:@"UserID" parentElement:promotionsElement];
            TBXMLElement * AreaID = [TBXML childElementNamed:@"AreaID" parentElement:promotionsElement];
            TBXMLElement * MerchantAddress = [TBXML childElementNamed:@"MerchantAddress" parentElement:promotionsElement];
            TBXMLElement * MerchantContact = [TBXML childElementNamed:@"MerchantContact" parentElement:promotionsElement];
            TBXMLElement * MerchantLocation = [TBXML childElementNamed:@"MerchantLocation" parentElement:promotionsElement];
            
            */
            
            if([tableName isEqualToString:@"Areas"])
            {
                TBXMLElement * Key = [TBXML childElementNamed:@"Key" parentElement:masterDataElement];
                TBXMLElement * Value = [TBXML childElementNamed:@"Value" parentElement:masterDataElement];
                success = [database executeUpdate:@"INSERT INTO Areas (AreaID,AreaName) values (?,?)",[NSNumber numberWithInt:[[TBXML textForElement:Key] integerValue]],[TBXML textForElement:Value],nil];
            }
            else if([tableName isEqualToString:@"CardProviders"])
            {
                TBXMLElement * Key = [TBXML childElementNamed:@"Key" parentElement:masterDataElement];
                TBXMLElement * Value = [TBXML childElementNamed:@"Value" parentElement:masterDataElement];
                //TBXMLElement * PhoneNumber = [TBXML childElementNamed:@"PhoneNumber" parentElement:masterDataElement];
                //if (PhoneNumber != nil) {
                //    NSLog(@"PhoneNumber: %@",[TBXML textForElement:PhoneNumber]);
                //}
                //NSLog(@"PhoneNumber: %@",[TBXML textForElement:PhoneNumber]);
                //TBXMLElement * Email = [TBXML childElementNamed:@"Email" parentElement:masterDataElement];
                //NSLog(@"Email: %@",[TBXML textForElement:Email]);
                //success = [database executeUpdate:@"INSERT INTO CardProviders (CardProviderID,CardProviderName,PhoneNumber,Email) values (?,?,?,?)",[NSNumber numberWithInt:[[TBXML textForElement:Key] integerValue]],[TBXML textForElement:Value],[TBXML textForElement:PhoneNumber],[TBXML textForElement:Email],nil];
                success = [database executeUpdate:@"INSERT INTO CardProviders (CardProviderID,CardProviderName) values (?,?)",[NSNumber numberWithInt:[[TBXML textForElement:Key] integerValue]],[TBXML textForElement:Value],nil];
            }
            else if([tableName isEqualToString:@"Cards"])
            {
                TBXMLElement * Key = [TBXML childElementNamed:@"Key" parentElement:masterDataElement];
                TBXMLElement * Value = [TBXML childElementNamed:@"Value" parentElement:masterDataElement];
                TBXMLElement * CardProviderID = [TBXML childElementNamed:@"CardProviderID" parentElement:masterDataElement];
                success = [database executeUpdate:@"INSERT INTO Cards (CardID,CardName,CardProviderID) values (?,?,?)",[NSNumber numberWithInt:[[TBXML textForElement:Key] integerValue]],[TBXML textForElement:Value],[NSNumber numberWithInt:[[TBXML textForElement:CardProviderID] integerValue]],nil];
            }
            else if([tableName isEqualToString:@"Categories"])
            {
                TBXMLElement * Key = [TBXML childElementNamed:@"Key" parentElement:masterDataElement];
                TBXMLElement * Value = [TBXML childElementNamed:@"Value" parentElement:masterDataElement];
                success = [database executeUpdate:@"INSERT INTO Categories (CategoryID,CategoryName) values (?,?)",[NSNumber numberWithInt:[[TBXML textForElement:Key] integerValue]],[TBXML textForElement:Value],nil];
            }
            else if([tableName isEqualToString:@"DiscountTypes"])
            {
                TBXMLElement * Key = [TBXML childElementNamed:@"Key" parentElement:masterDataElement];
                TBXMLElement * Value = [TBXML childElementNamed:@"Value" parentElement:masterDataElement];
                success = [database executeUpdate:@"INSERT INTO DiscountTypes (DiscountTypeID,DiscountTypeName) values (?,?)",[NSNumber numberWithInt:[[TBXML textForElement:Key] integerValue]],[TBXML textForElement:Value],nil];
            }
            else if([tableName isEqualToString:@"Merchants"])
            {
                TBXMLElement * Key = [TBXML childElementNamed:@"Key" parentElement:masterDataElement];
                TBXMLElement * Value = [TBXML childElementNamed:@"Value" parentElement:masterDataElement];
                TBXMLElement * AreaID = [TBXML childElementNamed:@"AreaID" parentElement:masterDataElement];
                TBXMLElement * MerchantAddress = [TBXML childElementNamed:@"MerchantAddress" parentElement:masterDataElement];
                TBXMLElement * MerchantContact = [TBXML childElementNamed:@"MerchantContact" parentElement:masterDataElement];
                TBXMLElement * MerchantLocation = [TBXML childElementNamed:@"MerchantLocation" parentElement:masterDataElement];
                
                success = [database executeUpdate:@"INSERT INTO Merchants (MerchantID,MerchantName,AreaID,MerchantAddress,MerchantContact,MerchantLocation) values (?,?,?,?,?,?)",[NSNumber numberWithInt:[[TBXML textForElement:Key] integerValue]],[TBXML textForElement:Value],[NSNumber numberWithInt:[[TBXML textForElement:AreaID] integerValue]],[TBXML textForElement:MerchantAddress],[TBXML textForElement:MerchantContact],[TBXML textForElement:MerchantLocation],nil];
            }
            
            
            
            // Let fmdb do the work
            
            if (!success) {
                NSLog(@"FAIL INSERT");
                NSLog(@"Error %d: %@", [database lastErrorCode], [database lastErrorMessage]);
            }
            
            
            masterDataElement = masterDataElement->nextSibling;
            
            
        }
        
        [database close];
        return [MasterData autorelease];
    }
    return nil;
}

-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.syncMasterDelegate) {
		if ([self.syncMasterDelegate respondsToSelector:@selector(api:didFailSyncMasterWithError:)]) {
			[self.syncMasterDelegate api:self didFailSyncMasterWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.syncMasterDelegate = nil;
	[super dealloc];
}



@end
