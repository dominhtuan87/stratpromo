//
//  QRReader_SuperViewController.m
//  StratPromo
//
//  Created by cat on 10/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "QRReader_SuperViewController.h"
#import "ActivityView.h"
#import "Constant.h"


@interface QRReader_SuperViewController ()

@end

@implementation QRReader_SuperViewController
@synthesize tbc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tbc.view.frame = self.view.bounds;
    //scrollView.frame = self.view.bounds;
    [self.view addSubview:tbc.view];
    
    NSLog(@"tbc.view.frame.size.height: %f",tbc.view.frame.size.height);
    NSLog(@"tbc.view.frame.size.height: %f",tbc.view.frame.size.width);
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
// pre-iOS 6 support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
