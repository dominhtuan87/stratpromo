//
//  CommentDeal_RequestBuilder.h
//  StratPromo
//
//  Created by Mac OS cat on 26/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentDeal_RequestBuilder : NSObject{
}

+(NSString*)CommentForPromoID:(NSString *)PromoID withToken:(NSString *)token andComment: (NSString*)Comment;



@end
