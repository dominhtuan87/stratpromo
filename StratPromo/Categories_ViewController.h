//
//  Categories_ViewController.h
//  StratPromo
//
//  Created by cat on 25/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface Categories_ViewController : UIViewController<CLLocationManagerDelegate>{
    UIScrollView * scrollView;
    UIView       * contentView;
    //CLLocation *currentLocation;
    CLLocationManager *locationManager;
}

@property (nonatomic, retain) IBOutlet UIScrollView * scrollView;
@property (nonatomic, retain) IBOutlet UIView       * contentView;


@property (strong, nonatomic) IBOutlet UIImageView *travelImageView;
@property (strong, nonatomic) IBOutlet UIImageView *beautyImageView;
@property (strong, nonatomic) IBOutlet UIImageView *playImageView;
@property (strong, nonatomic) IBOutlet UIImageView *homeApplianceImageView;

@property (strong, nonatomic) IBOutlet UIImageView *shoppingImageView;
@property (strong, nonatomic) IBOutlet UIImageView *ITImageView;
@property (strong, nonatomic) IBOutlet UIImageView *eatImageView;
@property (strong, nonatomic) IBOutlet UIImageView *fashionImageView;

@property (strong, nonatomic) IBOutlet UIButton *travelButton;
@property (strong, nonatomic) IBOutlet UIButton *beautyButton;
@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) IBOutlet UIButton *homeApplianceButton;

@property (strong, nonatomic) IBOutlet UIButton *shoppingButton;
@property (strong, nonatomic) IBOutlet UIButton *ITButton;
@property (strong, nonatomic) IBOutlet UIButton *eatButton;
@property (strong, nonatomic) IBOutlet UIButton *fashionButton;

@property (strong, nonatomic) IBOutlet UIButton *SBIButton;
@property (strong, nonatomic) IBOutlet UIButton *HSBCButton;
@property (strong, nonatomic) IBOutlet UIButton *OCBCButton;
@property (strong, nonatomic) IBOutlet UIButton *DBSButton;
@property (strong, nonatomic) IBOutlet UIButton *UOBButton;

@property (strong, nonatomic) IBOutlet UIButton *AllDistanceButton;
@property (strong, nonatomic) IBOutlet UIButton *within2KmButton;

//@property (strong, nonatomic) CLLocation *currentLocation;


- (IBAction)showOfferButtonPress:(id)sender;
- (IBAction)buttonClicked:(id)sender;

@end
