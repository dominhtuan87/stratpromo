//
//  Category_Result_ViewController.h
//  StratPromo
//
//  Created by cat on 27/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface Category_Result_ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    IBOutlet UITableView *mainTable;
}
@property (strong, nonatomic) NSString *Categories;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (strong, nonatomic) DetailViewController *detailViewController;

@end
