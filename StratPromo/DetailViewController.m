//
//  DetailViewController.m
//  SDWebImage Demo
//
//  Created by Olivier Poitrey on 09/05/12.
//  Copyright (c) 2012 Dailymotion. All rights reserved.
//

#import "DetailViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"

#import "MPFoldSegue.h"
#import "MPFoldTransition.h"
#import "MPFlipTransition.h"
#import <QuartzCore/QuartzCore.h>
#import "FMDatabase.h"
#import "Constant.h"
#import "Utility.h"
#import "NearbyPromo_ViewController.h"


@interface DetailViewController ()
- (void)configureView;
@property (strong) NSArray *ratingLabels;
@end

@implementation DetailViewController
@synthesize activity1;
@synthesize modal;
@synthesize ratingLabel = _ratingLabel;
@synthesize ratingLabels = _ratingLabels;
@synthesize starRatingControl = _starRatingControl;

@synthesize PopupView;
@synthesize fold = _fold;

@synthesize imageURL = _imageURL;
@synthesize tempRating;
@synthesize PromoID = _PromoID;
@synthesize imageView = _imageView;
@synthesize promoHeading,DiscountType,PromoEndDate,Rating,PromotionDetails,Comments,NewComment;
@synthesize activeTextField;
@synthesize promoURL,promotionLocationString,merchantAddress;

#pragma mark - Managing the detail item

- (void)setImageURL:(NSURL *)imageURL
{
    if (_imageURL != imageURL)
    {
        _imageURL = imageURL;
        [self configureView];
    }
}

- (void)configureView
{
    if (self.imageURL)
    {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = self.imageView;
        [self.imageView setImageWithURL:self.imageURL placeholderImage:nil options:SDWebImageProgressiveDownload progress:^(NSUInteger receivedSize, long long expectedSize)
        {
            if (!activityIndicator)
            {
                [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                activityIndicator.center = weakImageView.center;
                [activityIndicator startAnimating];
            }
        }
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
        {
            [activityIndicator removeFromSuperview];
            activityIndicator = nil;
        }];
        //[self.imageView.layer setShadowOffset:CGSizeMake(-1.0, -1.0)];
        //[self.imageView.layer setShadowOpacity:0.5];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self loadPromoDetails];
    
}

-(void)loadPromoDetails{
    //LOAD DB
    BOOL success = FALSE;
    //NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
    
    //NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
    
    
    FMDatabase *database = [FMDatabase databaseWithPath:path];
    
    database.traceExecution = YES;
    NSLog(@"Errors: %c",database.logsErrors);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    success = [fileManager fileExistsAtPath:path];
    
    if(success){
        NSLog(@"DB existed!");
        
    }
    
    if(![database open])
    {
        
        
        NSLog(@"Cant open DB");
        NSLog(@"Errors: %c",database.logsErrors);
        
    }
    
    
    if ([database hadError]) {
        NSLog(@"Err %d: %@", [database lastErrorCode], [database lastErrorMessage]);
    }
    
    FMResultSet *results = [database executeQuery:@"SELECT * FROM promotions INNER JOIN Merchants ON promotions.MerchantID = Merchants.MerchantID WHERE promotions.PromoID=?",self.PromoID];
    //FMResultSet *results = [database executeQuery:@"SELECT * FROM promotions WHERE PromoID=?",@"100"];
    
    PromotionDetails.text = @"Promotion Details: \n";
    
    if([results next]) {
        FMResultSet *results2;
        results2 = [database executeQuery:@"SELECT * FROM DiscountTypes WHERE DiscountTypeID = ?", [results stringForColumn:@"DiscountTypeID"]];
        [results2 next];
        NSString *DiscountTypeName = [results2 stringForColumn:@"DiscountTypeName"];

        
        //results2 = [database executeQuery:@"SELECT * FROM Cards WHERE CardID = ?", [results stringForColumn:@"CardID"]];
        //[results2 next];
        //NSString *CardName = [results2 stringForColumn:@"CardName"];
        
        promoHeading.text = [results stringForColumn:@"PromotionHeading"];
        DiscountType.text = DiscountTypeName;
        
        promoURL = [results stringForColumn:@"PromotionURL"];
        NSLog(@"promoURL: %@",promoURL);
        
        promotionLocationString = [results stringForColumn:@"PromotionLocation"];
        NSLog(@"promotionLocationString: %@",promotionLocationString);
        
        merchantAddress = [results stringForColumn:@"MerchantAddress"];
        NSLog(@"merchantAddress: %@",merchantAddress);
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:[results stringForColumn:@"PromoEndDate"]];
        NSLog(@"date: %@",date);
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"dd-MMM-yyyy"];
        // dateStr = [dateFormat stringFromDate:date];
        //[dateFormat release];
        
        
        PromoEndDate.text = [dateFormat stringFromDate:date];
        Rating.text = [results stringForColumn:@"Rating"];
        
        PromotionDetails.text = [PromotionDetails.text stringByAppendingString:[results stringForColumn:@"PromotionDetails"]];
        //[PromotionDetails.text stringByAppendingString:stringForColumn:@"PromotionDetails"];
        
        
        results = [database executeQuery:@"SELECT * FROM Comments WHERE PromoID=?",self.PromoID];
        
        Comments.text = @"Comments: \n";
        while ([results next]) {
            Comments.text = [Comments.text stringByAppendingFormat:@"%@: %@\n",[results stringForColumn:@"UserName"],[results stringForColumn:@"Comment"]];
        }
        
    }
    
    else{
        //UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Sync needed!" message:@"The deal is not on local DB!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //[alert show];
        
        //NSLog(@"GET REMOTE PROMO ...");
        
        [self syncAction];
        
        
    }
}

- (void)syncAction {
    //NSLog(@"GET REMOTE PROMO syncAction ...");
    [activity1 presentActivityView:SYNC_LOADING];
    SyncAPI *syncAPI = [[SyncAPI alloc] init];
    syncAPI.syncDelegate = self;
    //NSLog(@"[Utility getUserToken]: %@", [Utility getUserToken]);
    //[syncAPI performSyncWithToken:@"SampleToken" andPromoID:self.PromoID];
    [syncAPI performSyncWithToken:@"SampleToken" andPromoID:@"100"];
    //[syncAPI release];
}

-(void)api:(id)sender didFinishSyncWithStatus:(BOOL)status
{
    //NSLog(@"GET REMOTE PROMOTIONS COMPLETED");
    
    //[self prepareDataForTableView];
    
    [activity1 dismissActivityView];
    
    [self loadPromoDetails];
    
        
}
-(void)api:(id)sender didFailSyncWithError:(NSString*)error {
    
    [activity1 dismissActivityView];
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Sync Failed!"
                          message: error
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
    //[alert release];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    
    activity1 = [[ActivityView alloc] init];
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    self.navigationItem.title = @"$tratPromo";
    
    _ratingLabels = [NSArray arrayWithObjects:@"Unrated", @"Hate it", @"Don't like it", @"It's OK", @"It's good", @"It's great", nil];
	
	_starRatingControl.delegate = self;
    
    UIBarButtonItem * doneButton =
    [[UIBarButtonItem alloc]
     initWithTitle:@"Home" style:UIBarButtonItemStyleBordered target:self action:@selector(popPressed:)];
    
    self.navigationItem.rightBarButtonItem = doneButton ;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(keyboardWasShown:)
                                             name:UIKeyboardDidShowNotification
                                           object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(keyboardWillHide:)
                                             name:UIKeyboardWillHideNotification
                                           object:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTouched:)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [theScrollView addGestureRecognizer:tapGesture];
    
    //[tapGesture release];
}



- (void)keyboardWasShown:(NSNotification *)notification
{
    NSLog(@"keyboardWasShown");
    
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    NSLog(@"keyboardSize: width: %f height: %f", keyboardSize.width, keyboardSize.height);
    
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
        double navBarHeight = self.navigationController.navigationBar.frame.size.height;
        CGPoint scrollPoint = CGPointMake(0.0, activeTextField.frame.origin.y - (keyboardSize.height-15) + navBarHeight);
        NSLog(@"scrollPoint is %@", NSStringFromCGPoint(scrollPoint));
        [theScrollView setContentOffset:scrollPoint animated:YES];
    }
     
}


- (void) keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
     
    NSLog(@"keyboardWillHide");
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}

-(void)keyboardWillHide{
    NSLog(@"keyboardWillHide");
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.imageView = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//    NSLog(@"textFieldShouldBeginEditing");
    //textField.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f];
//    return YES;
//}

//- (void)textFieldDidEndEditing:(UITextField *)textField{
//    NSLog(@"textFieldDidEndEditing");
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    [textField resignFirstResponder];
    
    if([Utility getUserToken] == nil) {
    
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Login needed!" message:@"Please login to be able to comment!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        //[alert release];
    
    }
    
    else{
        [self commentAction];
        
    }
    
    
    return YES;
}

- (void)commentAction {

    
    
    [activity1 presentActivityView:COMMENT_LOADING];
    CommentDealAPI *commentAPI = [[CommentDealAPI alloc] init];
    commentAPI.commentDelegate = self;
    //NSLog(@"[Utility getUserToken]: %@", [Utility getUserToken]);
    [commentAPI performCommentForPromoID:self.PromoID withUserToken:[Utility getUserToken] andComment:self.NewComment.text];
    //[CommentDealAPI release];
}

-(void)api:(id)sender didFinishCommentWithStatus:(BOOL)status {
    
    [activity1 dismissActivityView];
    if(status) {
        self.Comments.text = [Comments.text stringByAppendingFormat:@"%@: %@\n",[[Utility getUserInfo] objectForKey:@"username"],self.NewComment.text];
        
        
        //SAVE TO DB
        BOOL success = FALSE;
        //NSError *error = nil;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsPath = [paths objectAtIndex:0];
        NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
        
        NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
        
        FMDatabase *database = [FMDatabase databaseWithPath:path];
        
        database.traceExecution = YES;
        //NSLog(@"Errors: %c",database.logsErrors);
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        success = [fileManager fileExistsAtPath:path];
        
        if(success){
            NSLog(@"DB existed!");
            
        }
        
        if(![database open])
        {
            
            
            //return nil;
            
        }
        
        
        if ([database hadError]) {
            NSLog(@"Err %d: %@", [database lastErrorCode], [database lastErrorMessage]);
        }
        NSNumber *currentNumber;
        FMResultSet *results = [database executeQuery:@"SELECT MAX(CommentID) FROM Comments"];
        while ([results next]) {
            currentNumber = [NSNumber numberWithInt:[[results stringForColumn:@"CategoryName"] intValue]+1];
        }
        
        NSLog(@"self.PromoID: %@",self.PromoID);
        success = [database executeUpdate:@"INSERT INTO Comments (CommentID,PromoID,UserName,Comment) values (?,?,?,?)",currentNumber,self.PromoID,[[Utility getUserInfo] objectForKey:@"username"],self.NewComment.text,nil];
            
            // Let fmdb do the work
            
            if (!success) {
                NSLog(@"FAIL INSERT");
                NSLog(@"Error %d: %@", [database lastErrorCode], [database lastErrorMessage]);
            }
    }
    self.NewComment.text = @"";
}

-(void)api:(id)sender didFailCommentWithError:(NSString *)error{
    
    [activity1 dismissActivityView];
    
    NSString* errorMessage = [[NSString alloc] initWithFormat:@"Error: %@, Please try again later!",error];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Unable to comment" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
}

- (void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating {
	_ratingLabel.text = [_ratingLabels objectAtIndex:rating];
    tempRating = [NSString stringWithFormat:@"%d", rating];
}

- (void)starRatingControl:(StarRatingControl *)control willUpdateRating:(NSUInteger)rating {
	_ratingLabel.text = [_ratingLabels objectAtIndex:rating];
}

- (IBAction)rateButtonPressed:(id)sender {

    if([Utility getUserToken] == nil) {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Login needed!" message:@"Please login to be able to rate!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        //[alert release];
        
    }
    
    else{
        modal = [[RNBlurModalView alloc] initWithView:PopupView];
        
        [modal show];
        
    }
    
    
}

- (IBAction)rateSubmitButtonPressed:(id)sender{
    if([tempRating length]){
        
        NSLog(@"tempRating: %@",tempRating);
        
        
        
        [modal hide];
        
        [self ratingAction];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Invalid Rating"
                              message: @"Rating of at least 1 star is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)ratingAction {
    
    
    
    [activity1 presentActivityView:COMMENT_LOADING];
    RateDealAPI *rateAPI = [[RateDealAPI alloc] init];
    rateAPI.rateDelegate = self;
    //NSLog(@"[Utility getUserToken]: %@", [Utility getUserToken]);
    [rateAPI performRateForPromoID:self.PromoID withUserToken:[Utility getUserToken] andRating:tempRating];
    //[CommentDealAPI release];
}

-(void)api:(id)sender didFinishRatingWithStatus:(BOOL)status{
    
    [activity1 dismissActivityView];
    if(status) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Deal Rated"
                              message: @"Rating has been submitted successfully!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)api:(id)sender didFailRatingWithError:(NSString *)error{
    
    [activity1 dismissActivityView];
    
    NSString* errorMessage = [[NSString alloc] initWithFormat:@"Error: %@, Please try again later!",error];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Unable to rate" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}


-(IBAction)backgroundTouched:(id)sender
{
    
    [NewComment resignFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)popPressed:(id)sender {
	// do the opposite fold/flip style from the transition that presented this view
	[self.navigationController popViewControllerWithFoldStyle:MPFoldStyleFlipFoldBit(7)];
}
-(IBAction)getDealButtonPressed:(id)sender
{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:promoURL]];
    
}

-(IBAction)mapButtonPressed:(id)sender
{
    
    NearbyPromo_ViewController *mapViewController = [[NearbyPromo_ViewController alloc] initWithNibName:@"NearbyPromo_ViewController_iPhone" bundle:nil];
    
    
    mapViewController.poiLoc = [[CLLocation alloc] initWithLatitude:[[[promotionLocationString componentsSeparatedByString:@","] objectAtIndex:0] floatValue] longitude:[[[promotionLocationString componentsSeparatedByString:@","] objectAtIndex:1] floatValue]];
    mapViewController.promotionLocationString = promotionLocationString;
    mapViewController.merchantAddress = merchantAddress;
    mapViewController.promoHeading = promoHeading.text;
    
    NSLog(@"DetailView poiLoc: %@", mapViewController.poiLoc);
    NSLog(@"DetailView poiLoc: %@", mapViewController.merchantAddress);
    NSLog(@"DetailView poiLoc: %@", mapViewController.promoHeading);
    
    
    [self.navigationController pushViewController:mapViewController animated:YES];
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please choose"
                                                    message:@"Which map do you want to launch?"
                                                   delegate:self
                                          cancelButtonTitle:@"Google Map"
                                          otherButtonTitles:@"Apple Map", nil];
    [alert show];
    
    */
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Clicked button index 0");
        if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]){
            NSString *url = [NSString stringWithFormat: @"comgooglemaps://?q=%@&center=%@&zoom=12",[promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            NSLog(@"url: %@",url);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            
            
        }
        
        else{
            NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?q=%@",
                             [promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }
    } else {
        NSLog(@"Clicked button index other than 0");
        // Add another action here
        
        NSString *url = [NSString stringWithFormat: @"http://maps.apple.com/?q=%@&ll=%@",
                         [promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[promotionLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}


@end
