//
//  SyncMasterRequestBuilder.m
//  StratPromo
//
//  Created by cat on 22/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "SyncMasterRequestBuilder.h"


@implementation SyncMasterRequestBuilder

+(NSString*)syncMasterWithTableName:(NSString *)TableName andKey:(NSString*)Key{
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SyncMaster" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__TableName__"
																 withString:TableName];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Key__"
																 withString:Key];
        return contentFormat;
	}
	return @"";
}

@end
