//
//  Categories_ViewController.m
//  StratPromo
//
//  Created by cat on 25/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "Categories_ViewController.h"
#import "UIImage+animatedGIF.h"
#import "FMDatabase.h"
#import "Constant.h"
#import "Category_Result_ViewController.h"

@interface Categories_ViewController ()

@end

@implementation Categories_ViewController

@synthesize scrollView, contentView;

@synthesize travelImageView;
@synthesize beautyImageView;
@synthesize playImageView;
@synthesize homeApplianceImageView;

@synthesize shoppingImageView;
@synthesize ITImageView;
@synthesize eatImageView;
@synthesize fashionImageView;

@synthesize travelButton;
@synthesize beautyButton;
@synthesize playButton;
@synthesize homeApplianceButton;

@synthesize shoppingButton;
@synthesize ITButton;
@synthesize eatButton;
@synthesize fashionButton;

@synthesize SBIButton;
@synthesize HSBCButton;
@synthesize OCBCButton;
@synthesize DBSButton;
@synthesize UOBButton;

@synthesize within2KmButton;
@synthesize AllDistanceButton;

//@synthesize currentLocation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.scrollView addSubview:self.contentView];
    scrollView.frame = self.view.bounds;
    NSLog(@"scrollView.frame height: %f",scrollView.frame.size.height);
    NSLog(@"scrollView.frame width: %f",scrollView.frame.size.width);
    
    NSLog(@"contentView.frame height: %f",contentView.frame.size.height);
    NSLog(@"contentView.frame width: %f",contentView.frame.size.width);
    
    self.scrollView.contentSize = self.contentView.bounds.size;
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"travel" withExtension:@"gif"];
    //NSURL *url = [NSURL URLWithString:[@"http://www.picgifs.com/graphics/r/relax/graphics-relax-364258.gif" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    self.travelImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"beauty1" withExtension:@"gif"];
    self.beautyImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"wellness1" withExtension:@"gif"];
    self.playImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"appliance" withExtension:@"gif"];
    self.homeApplianceImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    url = [[NSBundle mainBundle] URLForResource:@"shopping1" withExtension:@"gif"];
    self.shoppingImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"IT" withExtension:@"gif"];
    self.ITImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"dining" withExtension:@"gif"];
    self.eatImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    url = [[NSBundle mainBundle] URLForResource:@"fashion1" withExtension:@"gif"];
    self.fashionImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    
    //url = [[NSBundle mainBundle] URLForResource:@"variableDuration" withExtension:@"gif"];
    //self.variableDurationImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    NSLog(@"self.view.frame.size.height Categories: %f", self.view.frame.size.height);
    
    /*
    travelButton.tag = 1;
    beautyButton.tag = 2;
    wellnessButton.tag = 3;
    homeApplianceButton.tag = 4;
    
    shoppingButton.tag = 5;
    ITButton.tag = 6;
    diningButton.tag = 7;
    fashionButton.tag = 8;
    
    
    */
    
    
    shoppingButton.tag = 1;
    eatButton.tag = 2;
    travelButton.tag = 3;
    playButton.tag = 4;
    
    SBIButton.tag = 11;
    HSBCButton.tag = 12;
    OCBCButton.tag = 13;
    DBSButton.tag = 14;
    UOBButton.tag = 15;

    AllDistanceButton.tag = 101;
    within2KmButton.tag = 102;
    
    AllDistanceButton.highlighted = YES;
    AllDistanceButton.selected = YES;
    AllDistanceButton.backgroundColor = [UIColor grayColor];
    AllDistanceButton.alpha = 0.3;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClicked:(id)sender {
    NSLog(@"Button pressed: %@", [sender currentTitle]);
    
    UIButton *selectedButton = (UIButton *)sender;
    NSLog(@"Selected button tag is %d", selectedButton.tag);
    
    if (selectedButton.selected && selectedButton.tag!=101 && selectedButton.tag!=102) {
        NSLog(@"selectedButton.selected if: %c",selectedButton.selected );
        selectedButton.highlighted = NO;
        selectedButton.selected = NO;
        NSLog(@"selectedButton.selected if: %c",selectedButton.selected );
        //selectedButton.backgroundColor = [UIColor whiteColor];
        selectedButton.backgroundColor = [UIColor clearColor];
        selectedButton.alpha = 1;
    }
    else{
        NSLog(@"selectedButton.selected else: %c",selectedButton.selected );
        selectedButton.highlighted = YES;
        selectedButton.selected = YES;
        NSLog(@"selectedButton.selected else: %c",selectedButton.selected );
        selectedButton.backgroundColor = [UIColor grayColor];
        selectedButton.alpha = 0.3;
        
        if (selectedButton.tag==101) {
            within2KmButton.highlighted = NO;
            within2KmButton.selected = NO;
            within2KmButton.backgroundColor = [UIColor clearColor];
            within2KmButton.alpha = 1;
        }
        else if(selectedButton.tag==102) {
            AllDistanceButton.highlighted = NO;
            AllDistanceButton.selected = NO;
            AllDistanceButton.backgroundColor = [UIColor clearColor];
            AllDistanceButton.alpha = 1;
        }
    }
}

- (IBAction)showOfferButtonPress:(id)sender {
    NSLog(@"showOfferButtonPress: %@", [sender currentTitle]);
    
    NSString *selectedCategories = @"WHERE (";
    
    for(int i =1; i<5;i++){
        UIButton * button = (UIButton *)[self.view viewWithTag:i];
        if (button.selected) {
            NSLog(@"i: %d",i);
            selectedCategories = [selectedCategories stringByAppendingFormat:@" Promotions.CategoryID = %d OR",i ];
        }
    }
    if ([selectedCategories isEqualToString:@"WHERE ("]){
        selectedCategories = @"WHERE (";
    }
    else{
        NSLog(@"selectedCategories: %@",selectedCategories);
    
        selectedCategories = [selectedCategories substringToIndex:[selectedCategories length]-3];
        //selectedCategories = [selectedCategories stringByAppendingString:@";"];
        NSLog(@"selectedCategories: %@",selectedCategories);
        
        selectedCategories = [selectedCategories stringByAppendingString:@") AND ("];
    
    }
    
    
    
    NSLog(@"selectedCategories FINAL 1: %@",selectedCategories);
    
    
    
    for(int i =11; i<16;i++){
        UIButton * button = (UIButton *)[self.view viewWithTag:i];
        if (button.selected) {
            NSLog(@"i: %d",i);
            selectedCategories = [selectedCategories stringByAppendingFormat:@" Cards.CardProviderID = %d OR ",i-10];
        }
    }
    
    NSLog(@"BEFORE testsubstring: %@",selectedCategories);
    
    NSString *testsubstring = [selectedCategories substringFromIndex:([selectedCategories length]-5)];
    NSLog(@"testsubstring: %@",testsubstring);
    
    
    if ([selectedCategories isEqualToString:@"WHERE ("]){
        selectedCategories = @"";
    }
    
    
    
    else if ([testsubstring isEqualToString:@"AND ("]) {
        NSLog(@"selectedCategories else if: %@",selectedCategories);
        
        selectedCategories = [selectedCategories substringToIndex:[selectedCategories length]-7];
        //selectedCategories = [selectedCategories stringByAppendingString:@";"];
        NSLog(@"selectedCategories else if: %@",selectedCategories);
        
        selectedCategories = [selectedCategories stringByAppendingString:@") "];
    }
    
             
    else {
        NSLog(@"selectedCategories 1: %@",selectedCategories);
        
        selectedCategories = [selectedCategories substringToIndex:[selectedCategories length]-4];
        //selectedCategories = [selectedCategories stringByAppendingString:@";"];
        NSLog(@"selectedCategories 2: %@",selectedCategories);
        
        selectedCategories = [selectedCategories stringByAppendingString:@") "];
    }
    
    NSLog(@"selectedCategories FINAL 2: %@",selectedCategories);
    
    //LOAD DB
    BOOL success = FALSE;
    //NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
    
    //NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
    
    
    FMDatabase *database = [FMDatabase databaseWithPath:path];
    
    database.traceExecution = YES;
    NSLog(@"Errors: %c",database.logsErrors);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    success = [fileManager fileExistsAtPath:path];
    
    if(success){
        NSLog(@"DB existed!");
        
    }
    
    if(![database open])
    {
        
        
        NSLog(@"Cant open DB");
        NSLog(@"Errors: %c",database.logsErrors);
        
    }
    
    
    if ([database hadError]) {
        NSLog(@"Err %d: %@", [database lastErrorCode], [database lastErrorMessage]);
    }
    
    NSString* query = [NSString stringWithFormat:@"SELECT COUNT (*) FROM promotions INNER JOIN Categories ON Promotions.CategoryID = Categories.CategoryID INNER JOIN Cards ON Promotions.CardID = Cards.CardID INNER JOIN CardProviders ON Cards.CardProviderID %@",selectedCategories];
    
    NSLog(@"FINAL QUERY: %@",query);
    
    FMResultSet *results = [database executeQuery:query];
    
    NSString *count;
    
    //dataArray = [[NSMutableArray alloc] init];
    
    while([results next]) {
        
        count = [results stringForColumn:@"COUNT (*)"];
        NSLog(@"PromoName: %@",count);
    }
    
    NSLog(@"[PromoName length]: %d", [count length]);
    
    if ([count isEqualToString:@"0"]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No promotion is available with selected Filters. Please try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
    else{
        
        Category_Result_ViewController *resultViewController = [[Category_Result_ViewController alloc] initWithNibName:@"Category_Result_ViewController" bundle:nil];
        resultViewController.dataArray = [[NSMutableArray alloc] init];
        
        query = [NSString stringWithFormat:@"SELECT * FROM Promotions INNER JOIN Categories ON Promotions.CategoryID = Categories.CategoryID INNER JOIN Cards ON Promotions.CardID = Cards.CardID INNER JOIN CardProviders ON Cards.CardProviderID = CardProviders.CardProviderID %@ ORDER BY CategoryID ASC, AddedDate DESC;",selectedCategories];
        FMResultSet *results = [database executeQuery:query];
        
        //NSString *count;
        
        NSDictionary *lineItem;
        
        //dataArray = [[NSMutableArray alloc] init];
        
        UIButton * button = (UIButton *)[self.view viewWithTag:101];
        if (button.selected) {
            while([results next]) {
                
                
                lineItem = @{@"PromoID" : [results stringForColumn:@"PromoID"],
                             @"PromotionHeading" : [results stringForColumn:@"PromotionHeading"],
                             @"PromotionDetails" : [results stringForColumn:@"PromotionDetails"],
                             @"CategoryName" : [results stringForColumn:@"CategoryName"],
                             @"CardName" : [results stringForColumn:@"CardName"],
                             @"ImageURL" : [results stringForColumn:@"ImageURL"],
                             @"ThumbnailImageURL" : [results stringForColumn:@"ThumbnailImageURL"],
                             @"PromotionLocation" : [results stringForColumn:@"PromotionLocation"]};
                
            [resultViewController.dataArray addObject:lineItem];
            }
            
            if([resultViewController.dataArray count]!=0)
                [self.navigationController pushViewController:resultViewController animated:YES];
            else
            {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No promotion is available with selected Categories. Please try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
        
        
        else {
            
            //CLLocationManager *locationManager;
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
            if(![CLLocationManager locationServicesEnabled]){
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Location service is Disabled" message:@"Please check Location service and try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                
                
                
            }
            
            else{
                NSLog(@"Location service available");
                
                [locationManager startUpdatingLocation];
                //[locationManager startUpdatingLocation];
                
                if([locationManager location] == nil){
                    NSLog(@"Waiting for location ...");
                }
                //locationManager.StopUpdatingLocation();
                
                else{
                    [locationManager stopUpdatingLocation];
                    
                    //CLLocationCoordinate2D location = [locationManager location].coordinate;
                    
                    NSLog(@"Current latitude: %f", [locationManager location].coordinate.latitude);
                    NSLog(@"Current longitude: %f", [locationManager location].coordinate.longitude);
                    
                }
                //[locationManager stopUpdatingLocation];
                
                while([results next]) {
                    
                    
                    lineItem = @{@"PromoID" : [results stringForColumn:@"PromoID"],
                                 @"PromotionHeading" : [results stringForColumn:@"PromotionHeading"],
                                 @"PromotionDetails" : [results stringForColumn:@"PromotionDetails"],
                                 @"CategoryName" : [results stringForColumn:@"CategoryName"],
                                 @"CardName" : [results stringForColumn:@"CardName"],
                                 @"ImageURL" : [results stringForColumn:@"ImageURL"],
                                 @"ThumbnailImageURL" : [results stringForColumn:@"ThumbnailImageURL"],
                                 @"PromotionLocation" : [results stringForColumn:@"PromotionLocation"]};
                    
                    CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:[locationManager location].coordinate.latitude longitude:[locationManager location].coordinate.longitude];
                    
                    NSLog(@"userLoc: %@", userLoc);
                    
                    CLLocation *poiLoc = [[CLLocation alloc] initWithLatitude:[[[[results stringForColumn:@"PromotionLocation"] componentsSeparatedByString:@","] objectAtIndex:0] floatValue] longitude:[[[[results stringForColumn:@"PromotionLocation"] componentsSeparatedByString:@","] objectAtIndex:1] floatValue]];
                    
                    NSLog(@"[results stringForColumn:@\"PromotionLocation\"]: %@", [results stringForColumn:@"PromotionLocation"]);
                    
                    NSLog(@"poiLoc: %@", poiLoc);
                    
                    CLLocationDistance meters = [userLoc distanceFromLocation:poiLoc];
                    
                    NSLog(@"meters: %f", meters);
                    
                    if (meters < 2000) {
                        [resultViewController.dataArray addObject:lineItem];
                    }
                    
                    //CLLocation *poiLoc = [[CLLocation alloc] initWithLatitude:place2.latitude longitude:place2.longitude];
                    //CLLocationDistance dist = [userLoc getDistanceFrom:poiLoc]/(1000*distance);
                    
                    //CLLocationDistance dist = [userLoc getDistanceFrom:[locationManager location].coordinate.latitude]/(1000*distance);
                    //NSString *strDistance = [NSString stringWithFormat:@"%.2f", dist];
                    //NSLog(@"%@",strDistance);
                    
                    
                    
                }
                
                
            }
            
            NSLog(@"[resultViewController.dataArray count]: %lu", (unsigned long)[resultViewController.dataArray count]);
            
            if([resultViewController.dataArray count]!=0)
                [self.navigationController pushViewController:resultViewController animated:YES];
            else if ([locationManager location] != nil)
            {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No promotion is available within 2km. Please try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
            else if ([locationManager location] == nil)
            {
                
                UIAlertView *errorAlert = [[UIAlertView alloc]
                                           initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [errorAlert show];

            }
            
        }
        
            
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    [database close];
    
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
       [locationManager stopUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    //currentLocation = newLocation;
    [locationManager stopUpdatingLocation];
}


@end
