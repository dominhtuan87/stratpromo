//
//  StratPromo_AppDelegate.m
//  StratPromo
//
//  Created by cat on 19/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "StratPromo_AppDelegate.h"

#import "StratPromo_ViewController.h"

#import "FMDatabase.h"

#import "Constant.h"

#import "LeftViewController.h"

#import "IIViewDeckController.h"

@implementation StratPromo_AppDelegate

@synthesize mainViewController = mainViewController_;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    [NSThread sleepForTimeInterval:3.0];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
    
    NSLog(@"DB FOLDER PATH: %@",path);
    
    [self createAndCheckDatabasewithPath:path];
    
    
    NSLog(@"Registering for push notifications...");
    [[UIApplication sharedApplication]
     registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeAlert |
      UIRemoteNotificationTypeBadge |
      UIRemoteNotificationTypeSound)];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[StratPromo_ViewController alloc] initWithNibName:@"StratPromo_ViewController_iPhone" bundle:nil];
    } else {
        self.viewController = [[StratPromo_ViewController alloc] initWithNibName:@"StratPromo_ViewController_iPad" bundle:nil];
    }
    
    /*
    mainViewController_ = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    
    LeftViewController* leftController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:self.mainViewController leftViewController:leftController];
    deckController.leftLedge = 225;
    
    
    self.window.rootViewController = deckController;
    
    */
    
    LeftViewController* leftController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:self.viewController leftViewController:leftController];
    deckController.leftLedge = 225;
    
    
    mainViewController_ = [[UINavigationController alloc] initWithRootViewController:deckController];
    
    
    
    
    self.window.rootViewController = self.mainViewController;
    
    [self.window makeKeyAndVisible];
    return YES;
}

-(void) createAndCheckDatabasewithPath:(NSString*)path
{
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    
    success = [fileManager fileExistsAtPath:path];
    
    if(success){
        NSLog(@"DB existed!");
        return;
    }
    
    NSLog(@"DB is missing! Creating DB!");
    
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DB_NAME];
    
    success = [fileManager copyItemAtPath:databasePathFromApp toPath:path error:&error];
    
    if(success){
        NSLog(@"DB Created!");
        
    }
    else{
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        
        
    }
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
