//
//  StratPromo_ViewController.h
//  StratPromo
//
//  Created by cat on 19/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AdSdk/AdSdk.h>
#import "MHTabBarController.h"
#import "SyncAPI.h"
#import "SyncMasterAPI.h"
#import "ActivityView.h"

#import "Home_ViewController.h"
#import "AllDeals_ViewController.h"

@interface StratPromo_ViewController : UIViewController<AdSdkVideoInterstitialViewControllerDelegate, AdSdkBannerViewDelegate, MHTabBarControllerDelegate,SyncAPIDelegate,SyncMasterAPIDelegate>{
    MHTabBarController *tabBarController;
}

@property (nonatomic, strong) AdSdkVideoInterstitialViewController *videoInterstitialViewController;
@property (strong, nonatomic) AdSdkBannerView *bannerView;
@property (strong, nonatomic) MHTabBarController *tabBarController;
@property (strong, nonatomic) ActivityView* activity1;
@property (strong, nonatomic) ActivityView* activity2;

@property (strong, nonatomic) Home_ViewController *homeViewController;
@property (strong, nonatomic) AllDeals_ViewController *allDealsViewController;

@property(nonatomic, retain) IBOutlet UIButton *swipeButton;

- (IBAction)swipeButtonPressed:(id)sender;

- (IBAction)searchButtonPressed:(id)sender;
- (IBAction)refreshButtonPressed:(id)sender;

@end
