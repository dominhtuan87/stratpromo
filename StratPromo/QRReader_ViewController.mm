//
//  QRReader_ViewController.m
//  StratPromo
//
//  Created by cat on 25/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import "QRReader_ViewController.h"
#import "MultiFormatReader.h"
#import <UniversalResultParser.h>
#import <ParsedResult.h>
#import <ResultAction.h>
#import "ArchiveController.h"
#import "Database.h"
#import "ScanViewController.h"
//#import "BarcodesAppDelegate.h"

@interface QRReader_ViewController ()

@end

@implementation QRReader_ViewController
@synthesize actions;
@synthesize result;


#pragma mark -
#pragma mark View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    ZXingWidgetController *widController =
    [[ZXingWidgetController alloc] initWithDelegate:(id<ZXingDelegate>)self showCancel:NO OneDMode:NO];
    
    widController.view.frame = super.view.bounds;
    NSLog(@"widController.view.frame.size.height: %f",widController.view.frame.size.height);
    NSLog(@"widController.view.frame.size.width: %f",widController.view.frame.size.width);
    
    NSMutableSet *readers = [[NSMutableSet alloc ] init];
    
    MultiFormatReader* reader = [[MultiFormatReader alloc] init];
    [readers addObject:reader];
    //[reader release];
    
    widController.readers = readers;
    //[readers release];
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    widController.soundToPlay =
    [NSURL fileURLWithPath:[mainBundle pathForResource:@"beep-beep" ofType:@"aiff"] isDirectory:NO];
    
    widController.wantsFullScreenLayout = NO;
    //widController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);//self.view.frame;
    widController.view.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    widController.overlayView.frame = CGRectMake(0, 0, widController.view.frame.size.width, widController.view.frame.size.height);
    widController.overlayView.cropRect = CGRectMake(40, 40, widController.view.frame.size.width-80, widController.view.frame.size.height/1.2);
    
    
    [self addChildViewController:widController];
    [self.view addSubview:widController.view];
    
    // [self presentViewController:widController animated:YES completion:nil];
    
    //[widController release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    

    
}


- (void)messageReady:(id)sender {
    MessageViewController *messageController = sender;
    //[self presentModalViewController:messageController animated:YES];
    //TODO: change this
    [self.navigationController pushViewController:messageController animated:YES];
    //[messageController release];
}

- (void)messageFailed:(id)sender {
    MessageViewController *messageController = sender;
    NSLog(@"Failed to load message!");
    //[messageController release];
}

- (void)performAction:(ResultAction *)action {
    [action performActionWithController:self shouldConfirm:NO];
}

- (void)modalViewControllerWantsToBeDismissed:(UIViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark -
#pragma mark ZXingDelegateMethods
#pragma mark -
#pragma mark ZXingDelegateMethods
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    [self dismissModalViewControllerAnimated:YES];
#if ZXING_DEBUG
    NSLog(@"result has %d actions", actions ? 0 : actions.count);
#endif
    Scan * scan = [[Database sharedDatabase] addScanWithText:resultString];
    [[NSUserDefaults standardUserDefaults] setObject:resultString forKey:@"lastScan"];
    NSString *returnUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"returnURL"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"returnURL"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"scanFormats"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (returnUrl != nil) {
        resultString = (NSString*)
        CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                (CFStringRef)resultString,
                                                NULL,
                                                (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                kCFStringEncodingUTF8));
        
        NSURL *ourURL =
        [NSURL URLWithString:[returnUrl stringByReplacingOccurrencesOfString:@"{CODE}" withString:resultString]];
        
        //CFRelease(resultString);
        
        // NSLog(@"%@", ourURL);
        
        [[UIApplication sharedApplication] openURL:ourURL];
        return;
    }
    
    ParsedResult *parsedResult = [UniversalResultParser parsedResultForString:resultString];
    self.result = parsedResult ;
    self.actions = self.result.actions;
    ScanViewController *scanViewController = [[ScanViewController alloc] initWithResult:parsedResult forScan:scan];
    [self.navigationController pushViewController:scanViewController animated:NO];
    //[scanViewController release];
    [self performResultAction];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    NSString* returnUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"returnURL"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"returnURL"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"scanFormats"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // NSLog(@"%@ %d", returnUrl, buttonIndex);
    if (returnUrl != nil && buttonIndex != 0) {
        NSURL *ourURL =
        [NSURL URLWithString:[returnUrl stringByReplacingOccurrencesOfString:@"{CODE}" withString:@""]];
        // NSLog(@"%@ %@", ourURL, returnUrl);
        [[UIApplication sharedApplication] openURL:ourURL];
    }
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissModalViewControllerAnimated:YES];
    NSString *returnUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"returnURL"];
    if (returnUrl != nil) {
        UIAlertView* alert = [[UIAlertView alloc]
                              initWithTitle:@"Return to website?"
                              message:nil
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"Return", nil];
        [alert show];
        //[alert release];
    }
}

- (void)confirmAndPerformAction:(ResultAction *)action {
    [action performActionWithController:self shouldConfirm:YES];
}

- (void)performResultAction {
    if (self.result == nil) {
        NSLog(@"no result to perform an action on!");
        return;
    }
    
    if (self.actions == nil || self.actions.count == 0) {
#if ZXING_DEBUG
        NSLog(@"result has no actions to perform!");
#endif
        return;
    }
    
    if (self.actions.count == 1) {
        ResultAction *action = [self.actions lastObject];
#if ZXING_DEBUG
        NSLog(@"Result has the single action, (%@)  '%@', performing it",
              NSStringFromClass([action class]), [action title]);
#endif
        [self performSelector:@selector(confirmAndPerformAction:)
                   withObject:action
                   afterDelay:0.0];
    } else {
#if ZXING_DEBUG
        NSLog(@"Result has multiple actions, popping up an action sheet");
#endif
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithFrame:self.view.bounds];
        
        for (ResultAction *action in self.actions) {
            [actionSheet addButtonWithTitle:[action title]];
        }
        
        int cancelIndex = [actionSheet addButtonWithTitle:NSLocalizedString(@"DecoderViewController cancel button title", @"Cancel")];
        actionSheet.cancelButtonIndex = cancelIndex;
        
        actionSheet.delegate = self;
        
        [actionSheet showInView:self.view];
    }
}



- (void)viewDidUnload {
   
}

- (void)dealloc {
    //[resultsView release];
    //[resultsToDisplay release];
    //[super dealloc];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
// pre-iOS 6 support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
