//
//
//  Created by User on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"


@interface CBXML : TBXML {
    
}

@property (nonatomic, assign) TBXMLElement *soapBodyElement;

- (id)initWithXMLString:(NSString*)aXMLString;

@end
