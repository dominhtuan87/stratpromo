//
//
//  Created by User on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CBXML.h"

@implementation CBXML

@synthesize soapBodyElement;

- (id)initWithXMLString:(NSString*)aXMLString {
    
    self = [super initWithXMLString:aXMLString];
    
    if (self != nil) {
        TBXMLElement * root = self.rootXMLElement;
        if (root) {
            TBXMLElement * soapBody = [TBXML childElementNamed:@"soap:Body" parentElement:root];
            if (soapBody) {
                self.soapBodyElement = soapBody->firstChild;
            }
            else {
                self.soapBodyElement = nil;
            }
        }
    }
    return self;
}

- (void)dealloc {
    
    [super dealloc];
}

@end
