//
//  SyncRequestBuilder.h
//  ClueBox
//
//  Created by Rakesh Raveendran on 19/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utility.h"

@interface SyncRequestBuilder : NSObject {
    
}

+(NSString*)syncForUserToken:(NSString *)token andPromoID:(NSString*)PromoID;

@end
