//
//  Discount_ViewController.h
//  StratPromo
//
//  Created by cat on 30/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Discount_ViewController : UIViewController{
    UIScrollView * scrollView;
    UIView       * contentView;
}

@property (nonatomic, retain) IBOutlet UIScrollView * scrollView;
@property (nonatomic, retain) IBOutlet UIView       * contentView;


//@property (strong, nonatomic) IBOutlet UIImageView *travelImageView;
//@property (strong, nonatomic) IBOutlet UIImageView *beautyImageView;
//@property (strong, nonatomic) IBOutlet UIImageView *wellnessImageView;
//@property (strong, nonatomic) IBOutlet UIImageView *homeApplianceImageView;

//@property (strong, nonatomic) IBOutlet UIImageView *shoppingImageView;
//@property (strong, nonatomic) IBOutlet UIImageView *ITImageView;
//@property (strong, nonatomic) IBOutlet UIImageView *diningImageView;
//@property (strong, nonatomic) IBOutlet UIImageView *fashionImageView;

@property (strong, nonatomic) IBOutlet UIButton *travelButton;
@property (strong, nonatomic) IBOutlet UIButton *beautyButton;
@property (strong, nonatomic) IBOutlet UIButton *wellnessButton;
//@property (strong, nonatomic) IBOutlet UIButton *homeApplianceButton;

//@property (strong, nonatomic) IBOutlet UIButton *shoppingButton;
//@property (strong, nonatomic) IBOutlet UIButton *ITButton;
//@property (strong, nonatomic) IBOutlet UIButton *diningButton;
//@property (strong, nonatomic) IBOutlet UIButton *fashionButton;

- (IBAction)showOfferButtonPress:(id)sender;
- (IBAction)buttonClicked:(id)sender;



@end
