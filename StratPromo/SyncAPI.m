//
//  SyncAPI.m
//  ClueBox
//
//  Created by Rakesh Raveendran on 19/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SyncAPI.h"
#import "Constant.h"
#import "CBXML.h"
#import "Utility.h"
#import "SyncRequestBuilder.h"
//#import "dealcomments.h"
#import "FMDatabase.h"

@interface SyncAPI (Private)

-(NSArray *)PromotionsFromElement:(TBXMLElement *)rootElement;
-(void)CommentsFromElement:(TBXMLElement *)rootElement withPromoID:(NSString*)PromoID andDB:(FMDatabase*)database;

@end



@implementation SyncAPI

@synthesize syncDelegate = syncDelegate_;

- (id)init {
    
    if (self = [super init]) {
		
        if (service_ == nil) {
			
            service_ = [[SOAPWebService alloc] init];
        }
        service_.serviceDelegate = self;
    }
    return self;
}

- (void)performSyncWithToken:(NSString *)token andPromoID:(NSString *)PromoID{
    
    [self retain];
	service_.serviceKey = SYNC_KEY;
	NSString * soapBody = [SyncRequestBuilder syncForUserToken:token andPromoID:PromoID];
    //NSLog(@"soapBody: %@",soapBody);
    //NSLog(@"SYNC_KEY: %@",SYNC_KEY);
    //NSLog(@"AUTHENTICATE_URL: %@",AUTHENTICATE_URL);
    //NSLog(@"SYNC_SOAP_ACTION: %@",SYNC_SOAP_ACTION);
	[service_ doSOAPCallWithUrl:AUTHENTICATE_URL andSOAPMessage:soapBody andSOAPAction:SYNC_SOAP_ACTION];
}

#pragma mark - SOAPWebService Delegate

-(void)service:(id)sender requestSuccessfullyCompleted:(id)result {
    NSArray *promotions = [[NSArray alloc] init];
    NSDictionary *responseData = [result retain];
    //NSLog(@"response data is ===== %@",responseData);
	
    if ([responseData objectForKey:SYNC_KEY]) {
        
        //dealDetails *dealDetail = [[dealDetails alloc] init];
        CBXML * parser = (CBXML *)[responseData objectForKey:SYNC_KEY];
        TBXMLElement * rootElement = parser.soapBodyElement;
        NSString * errorReason = nil;
        TBXMLElement *childeElement = [TBXML childElementNamed:@"SyncResult" parentElement:rootElement];
        TBXMLElement * status = [TBXML childElementNamed:@"Success" parentElement:childeElement];
        TBXMLElement *errorOnFailure = [TBXML childElementNamed:@"ErrorOnFailure" parentElement:childeElement];
        TBXMLElement *NewPromotions = [TBXML childElementNamed:@"NewPromotions" parentElement:childeElement];
        
        
        
        if (errorOnFailure) {
            errorReason = [TBXML textForElement:errorOnFailure];
            //NSLog(@"[TBXML textForElement:errorOnFailure]: %@",[TBXML textForElement:errorOnFailure]);
        }
        //NSLog(@"[TBXML textForElement:status]: %@",[TBXML textForElement:status]);
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            //promotions = [NSArray arrayWithArray:[self PromotionsFromElement:NewPromotions]];
            promotions = [[[self PromotionsFromElement:NewPromotions] copy] autorelease];
            
             //for(int i=0; i<[promotions count];i++){
             //    NSLog(@"[promotions[i] count] API: %d", [promotions[i] count]);
             //}
        }
        
        if ([[TBXML textForElement:status] isEqualToString:TRUE_STATUS]) {
            //NSLog(@"TRUE");
            if (self.syncDelegate) {
                 //NSLog(@"TRUE IF 1");
                if ([self.syncDelegate respondsToSelector:@selector(api:didFinishSyncWithStatus:)]) {
                     //NSLog(@"TRUE IF 2");
                    [self.syncDelegate api:self didFinishSyncWithStatus:YES];
                }
            }
        } else {
            //NSLog(@"FALSE");
            if (self.syncDelegate) {
                if ([self.syncDelegate respondsToSelector:@selector(api:didFailSyncWithError:)]) {
                    //NSLog(@"errorReason: %@",errorReason);
                    [self.syncDelegate api:self didFailSyncWithError:errorReason];
                }
            }
        }
        
         //NSLog(@"PUTSIDE TRUE");
        //[dealDetail release];
    }
     //NSLog(@"BEFORE RELEASING");
    
    
    //[promotions release];
    [responseData release];
    [self release];
}

-(void)CommentsFromElement:(TBXMLElement *)rootElement withPromoID:(NSString *)PromoID andDB:(FMDatabase*)database{
    if (rootElement) {
        
        TBXMLElement * commentsElement = rootElement->firstChild;
        
        //NSMutableArray * comments = [NSMutableArray new];
        BOOL success = FALSE;
        /*
        //OPEN DB
        
        //NSError *error = nil;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsPath = [paths objectAtIndex:0];
        NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
        
        NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
        
        FMDatabase *database = [FMDatabase databaseWithPath:path];
        
        //database.traceExecution = YES;
        //NSLog(@"Errors: %c",database.logsErrors);
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        success = [fileManager fileExistsAtPath:path];
        
        if(success){
            NSLog(@"DB existed!");
            
        }
        
        if(![database open])
        {
            
            NSLog(@"Cant open DB");
            NSLog(@"Errors: %c",database.logsErrors);
            //return nil;
            
        }

        */
        while (commentsElement) {
            
            TBXMLElement * CommentID = [TBXML childElementNamed:@"CommentID" parentElement:commentsElement];
            //TBXMLElement * UserID = [TBXML childElementNamed:@"UserID" parentElement:commentsElement];
            TBXMLElement * Comment = [TBXML childElementNamed:@"Comment" parentElement:commentsElement];
            TBXMLElement * Username = [TBXML childElementNamed:@"Username" parentElement:commentsElement];
            
              
            success = [database executeUpdate:@"INSERT INTO Comments (CommentID,PromoID,UserName,Comment) values (?,?,?,?)",[NSNumber numberWithInt:[[TBXML textForElement:CommentID] integerValue]],[NSNumber numberWithInt:[PromoID integerValue]],[TBXML textForElement:Username],[TBXML textForElement:Comment],nil];
            
            // Let fmdb do the work
            
            if (!success) {
                NSLog(@"FAIL INSERT");
                NSLog(@"Error %d: %@", [database lastErrorCode], [database lastErrorMessage]);
            }
            
            
            
            
            
            
            
            commentsElement = commentsElement->nextSibling;
        }
        
        
    }
    
}

-(NSArray *)PromotionsFromElement:(TBXMLElement *)rootElement{
    if (rootElement) {
        
        TBXMLElement * promotionsElement = rootElement->firstChild;
        
        NSMutableArray * promotions = [[NSMutableArray alloc] init];
        
        //dealDetails *dealDetail;
        
        //SAVE TO DB
        BOOL success = FALSE;
        //NSError *error = nil;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsPath = [paths objectAtIndex:0];
        NSString *path = [docsPath stringByAppendingPathComponent:DB_NAME];
        
        NSLog(@"DB FOLDER PATH SYNCAPI: %@",path);
        
        FMDatabase *database = [FMDatabase databaseWithPath:path];
        
        database.traceExecution = YES;
        //NSLog(@"Errors: %c",database.logsErrors);
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        success = [fileManager fileExistsAtPath:path];
        
        if(success){
            NSLog(@"DB existed!");
            
        }
        
        if(![database open])
        {
            
            
            return nil;
            
        }
        
        
        if ([database hadError]) {
            NSLog(@"Err %d: %@", [database lastErrorCode], [database lastErrorMessage]);
        }
        
        
        
        while (promotionsElement) {
            
            TBXMLElement * PromoID = [TBXML childElementNamed:@"PromoID" parentElement:promotionsElement];
            TBXMLElement * MerchantID = [TBXML childElementNamed:@"MerchantID" parentElement:promotionsElement];
            TBXMLElement * CategoryID = [TBXML childElementNamed:@"CategoryID" parentElement:promotionsElement];
            TBXMLElement * CardID = [TBXML childElementNamed:@"CardID" parentElement:promotionsElement];
            TBXMLElement * DiscountTypeID = [TBXML childElementNamed:@"DiscountTypeID" parentElement:promotionsElement];
            
            TBXMLElement * ImageURL = [TBXML childElementNamed:@"ImageURL" parentElement:promotionsElement];
            TBXMLElement * ThumbnailImageURL = [TBXML childElementNamed:@"ThumbnailImageURL" parentElement:promotionsElement];
            TBXMLElement * ProductImageURL = [TBXML childElementNamed:@"ProductImageURL" parentElement:promotionsElement];
            
            TBXMLElement * RewardPoints = [TBXML childElementNamed:@"RewardPoints" parentElement:promotionsElement];
            TBXMLElement * PromotionHeading = [TBXML childElementNamed:@"PromotionHeading" parentElement:promotionsElement];
            TBXMLElement * PromotionDescription = [TBXML childElementNamed:@"PromotionDescription" parentElement:promotionsElement];
            TBXMLElement * PromotionLocation = [TBXML childElementNamed:@"PromotionLocation" parentElement:promotionsElement];
            TBXMLElement * PromotionArea = [TBXML childElementNamed:@"PromotionArea" parentElement:promotionsElement];
            TBXMLElement * OriginalPrice = [TBXML childElementNamed:@"OriginalPrice" parentElement:promotionsElement];
            TBXMLElement * PromotionPrice = [TBXML childElementNamed:@"PromotionPrice" parentElement:promotionsElement];
            TBXMLElement * AddedDate = [TBXML childElementNamed:@"AddedDate" parentElement:promotionsElement];
            TBXMLElement * PromotionStart = [TBXML childElementNamed:@"PromotionStart" parentElement:promotionsElement];
            TBXMLElement * PromotionEnd = [TBXML childElementNamed:@"PromotionEnd" parentElement:promotionsElement];
            TBXMLElement * Rating = [TBXML childElementNamed:@"Rating" parentElement:promotionsElement];
            TBXMLElement * Comments = [TBXML childElementNamed:@"Comments" parentElement:promotionsElement];
            
            TBXMLElement * Views = [TBXML childElementNamed:@"Views" parentElement:promotionsElement];
            TBXMLElement * DiscountPercentage = [TBXML childElementNamed:@"DiscountPercentage" parentElement:promotionsElement];
            TBXMLElement * PromotionURL = [TBXML childElementNamed:@"PromotionURL" parentElement:promotionsElement];
            
            
            //NSLog(@"[TBXML textForElement:PromoID]: %@",[TBXML textForElement:PromoID]);
            
            
            
            
            // Building the string ourself
            //success = [database executeUpdate:@"INSERT INTO Promotions (PromoID,MerchantID,CategoryID,CardID,DiscountTypeID,ImageURL,RewardPoints,PromotionHeading,PromotionDetails,PromotionLocation,PromotionAreaID,OriginalPrice,PromotionPrice,AddedDate,PromoStartDate,PromoEndDate,Rating) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[[TBXML textForElement:PromoID] integerValue],[[TBXML textForElement:MerchantID] integerValue],[[TBXML textForElement:CategoryID] integerValue],[[TBXML textForElement:CardID] integerValue],[[TBXML textForElement:DiscountTypeID] integerValue],[TBXML textForElement:ImageURL],[[TBXML textForElement:RewardPoints] integerValue],[TBXML textForElement:PromotionHeading],[TBXML textForElement:PromotionDescription],[TBXML textForElement:PromotionLocation],[TBXML textForElement:PromotionArea],[[TBXML textForElement:OriginalPrice] floatValue],[[TBXML textForElement:PromotionPrice] floatValue],[TBXML textForElement:AddedDate],[TBXML textForElement:PromotionStart],[TBXML textForElement:PromotionEnd],[[TBXML textForElement:Rating] floatValue],nil];
            
            success = [database executeUpdate:@"INSERT INTO Promotions (PromoID,MerchantID,CategoryID,CardID,DiscountTypeID,ImageURL,RewardPoints,PromotionHeading,PromotionDetails,PromotionLocation,PromotionAreaID,OriginalPrice,PromotionPrice,AddedDate,PromoStartDate,PromoEndDate,Rating,ThumbnailImageURL,ProductImageURL,Views,DiscountPercentage,PromotionURL) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[NSNumber numberWithInt:[[TBXML textForElement:PromoID] integerValue]],[NSNumber numberWithInt:[[TBXML textForElement:MerchantID] integerValue]],[NSNumber numberWithInt:[[TBXML textForElement:CategoryID] integerValue]],[NSNumber numberWithInt:[[TBXML textForElement:CardID] integerValue]],[NSNumber numberWithInt:[[TBXML textForElement:DiscountTypeID] integerValue]],[TBXML textForElement:ImageURL],[NSNumber numberWithInt:[[TBXML textForElement:RewardPoints] integerValue]],[TBXML textForElement:PromotionHeading],[TBXML textForElement:PromotionDescription],[TBXML textForElement:PromotionLocation],[NSNumber numberWithInt:[[TBXML textForElement:PromotionArea] integerValue]],[NSNumber numberWithFloat:[[TBXML textForElement:OriginalPrice] floatValue]],[NSNumber numberWithFloat:[[TBXML textForElement:PromotionPrice] floatValue]],[TBXML textForElement:AddedDate],[TBXML textForElement:PromotionStart],[TBXML textForElement:PromotionEnd],[NSNumber numberWithFloat:[[TBXML textForElement:Rating] floatValue]],[TBXML textForElement:ThumbnailImageURL],[TBXML textForElement:ProductImageURL],[NSNumber numberWithInt:[[TBXML textForElement:Views] integerValue]],[TBXML textForElement:DiscountPercentage],[TBXML textForElement:PromotionURL],nil];
            
            // Let fmdb do the work
            
            if (!success) {
                NSLog(@"FAIL INSERT");
                NSLog(@"Error %d: %@", [database lastErrorCode], [database lastErrorMessage]);
            }
            
                        
            [self CommentsFromElement:Comments withPromoID:[TBXML textForElement:PromoID] andDB:database];
            /*
            
            
            dealDetail.PromoID = [TBXML textForElement:PromoID];
            dealDetail.MerchantID = [TBXML textForElement:MerchantID];
            dealDetail.CategoryID = [TBXML textForElement:CategoryID];
            dealDetail.CardID = [TBXML textForElement:CardID];
            dealDetail.DiscountTypeID = [TBXML textForElement:DiscountTypeID];
            
            dealDetail.ImageURL = [TBXML textForElement:ImageURL];
            dealDetail.RewardPoints = [TBXML textForElement:RewardPoints];
            dealDetail.PromotionHeading = [TBXML textForElement:PromotionHeading];
            dealDetail.PromotionDescription = [TBXML textForElement:PromotionDescription];
            dealDetail.PromotionLocation = [TBXML textForElement:PromotionLocation];
            dealDetail.PromotionArea = [TBXML textForElement:PromotionArea];
            dealDetail.OriginalPrice = [TBXML textForElement:OriginalPrice];
            dealDetail.PromotionPrice = [TBXML textForElement:PromotionPrice];
            dealDetail.AddedDate = [TBXML textForElement:AddedDate];
            dealDetail.PromotionStart = [TBXML textForElement:PromotionStart];
            dealDetail.PromotionEnd = [TBXML textForElement:PromotionEnd];
            dealDetail.Rating = [TBXML textForElement:Rating];
            dealDetail.Comments = [self CommentsFromElement:Comments];
            
            //NSLog(@"dealDetail.PromoID SUBAPI WHILE: %@",dealDetail.PromoID);
            //NSLog(@"API: [dealDetail.Comments count]: %d",[dealDetail.Comments count]);
            //NSLog(@"API: dealDetail.Comments: %@",dealDetail.Comments);
            
            //NSLog(@"dealDetail.PromoID: %@",dealDetail.PromoID);
            //NSLog(@"dealDetail.MerchantID: %@",dealDetail.MerchantID);
            //NSLog(@"dealDetail.CategoryID: %@",dealDetail.CategoryID);
            //NSLog(@"dealDetail.CardID: %@",dealDetail.CardID);
            //NSLog(@"dealDetail.DiscountTypeID: %@",dealDetail.DiscountTypeID);
            //NSLog(@"dealDetail.ImageURL: %@",dealDetail.ImageURL);
            //NSLog(@"dealDetail.RewardPoints: %@",dealDetail.RewardPoints);
            //NSLog(@"dealDetail.PromotionHeading: %@",dealDetail.PromotionHeading);
            //NSLog(@"dealDetail.PromotionDescription: %@",dealDetail.PromotionDescription);
            //NSLog(@"dealDetail.PromotionLocation: %@",dealDetail.PromotionLocation);
            //NSLog(@"dealDetail.PromotionArea: %@",dealDetail.PromotionArea);
            //NSLog(@"dealDetail.OriginalPrice: %@",dealDetail.OriginalPrice);
            //NSLog(@"dealDetail.PromotionPrice: %@",dealDetail.PromotionPrice);
            //NSLog(@"dealDetail.AddedDate: %@",dealDetail.AddedDate);
            //NSLog(@"dealDetail.PromotionStart: %@",dealDetail.PromotionStart);
            //NSLog(@"dealDetail.PromotionEnd: %@",dealDetail.PromotionEnd);
            //NSLog(@"dealDetail.Rating: %@",dealDetail.Rating);
            //NSLog(@"dealDetail.Comments: %@",dealDetail.Comments);
            
            [promotions addObject:dealDetail];
            
            
            */
            
            
            //TEST DATA SYNC
            /*
            for(int i=0; i<[promotions count];i++){
                dealDetail = promotions[i];
                NSLog(@"dealDetail.PromoID SUBAPI WHILE: %@",dealDetail.PromoID);
            }
            */
            
            
            promotionsElement = promotionsElement->nextSibling;
            //[dealDetail release];
        }
        
        [database close];

        
        //TEST DATA SYNC
        /*
        for(int i=0; i<[promotions count];i++){
            NSLog(@"SYNC PROMOTIONS SUBAPI %d: %@",i, promotions[i]);
            //NSLog(@"Promotion  SUBAPI %d: %@",i, promotions[i]);
            dealDetail = promotions[i];
            
            NSLog(@"dealDetail.PromoID SUBAPI %d: %@",i,dealDetail.PromoID);
            NSLog(@"dealDetail.MerchantID SUBAPI %d: %@",i,dealDetail.MerchantID);
            NSLog(@"dealDetail.CategoryID SUBAPI %d: %@",i,dealDetail.CategoryID);
            NSLog(@"dealDetail.CardID SUBAPI %d: %@",i,dealDetail.CardID);
            NSLog(@"dealDetail.DiscountTypeID SUBAPI %d: %@",i,dealDetail.DiscountTypeID);
            NSLog(@"dealDetail.ImageURL SUBAPI %d: %@",i,dealDetail.ImageURL);
            NSLog(@"dealDetail.RewardPoints SUBAPI %d: %@",i,dealDetail.RewardPoints);
            NSLog(@"dealDetail.PromotionHeading SUBAPI %d: %@",i,dealDetail.PromotionHeading);
            NSLog(@"dealDetail.PromotionDescription SUBAPI %d: %@",i,dealDetail.PromotionDescription);
            NSLog(@"dealDetail.PromotionLocation SUBAPI %d: %@",i,dealDetail.PromotionLocation);
            NSLog(@"dealDetail.PromotionArea SUBAPI %d: %@",i,dealDetail.PromotionArea);
            NSLog(@"dealDetail.OriginalPrice SUBAPI %d: %@",i,dealDetail.OriginalPrice);
            NSLog(@"dealDetail.PromotionPrice SUBAPI %d: %@",i,dealDetail.PromotionPrice);
            NSLog(@"dealDetail.AddedDate SUBAPI %d: %@",i,dealDetail.AddedDate);
            NSLog(@"dealDetail.PromotionStart SUBAPI %d: %@",i,dealDetail.PromotionStart);
            NSLog(@"dealDetail.PromotionEnd SUBAPI %d: %@",i,dealDetail.PromotionEnd);
            NSLog(@"dealDetail.Rating SUBAPI %d: %@",i,dealDetail.Rating);
            
            NSLog(@"dealDetail.Comments SUBAPI: %@", dealDetail.Comments);
            
            NSLog(@"[dealDetail.Comments count] SUBAPI: %d", [dealDetail.Comments count]);
            dealcomments *dealcomment = [[dealcomments alloc] init];
            for (int j=0; j<[dealDetail.Comments count];j++){
                dealcomment = dealDetail.Comments[j];
                NSLog(@"dealDetail.Comments SUBAPI %d for comment number %d: %@ by %@ with UserID: %@",i,j,dealcomment.Comment, dealcomment.Username,dealcomment.UserID);
            }
        }
         
         */
        
        
        //[dealDetail release];

        return [promotions autorelease];
        
        
    }
    return nil;
}


-(void)service:(id)sender requestFailedWithError:(NSString*)error {
    
    if (self.syncDelegate) {
		if ([self.syncDelegate respondsToSelector:@selector(api:didFailSyncWithError:)]) {
			[self.syncDelegate api:self didFailSyncWithError:error];
		}
	}
	[self release];
}

- (void) dealloc
{
	service_.serviceDelegate = nil;
	[service_ release];
	self.syncDelegate = nil;
	[super dealloc];
}


@end
