//
//  SyncMasterAPI.h
//  StratPromo
//
//  Created by cat on 22/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SOAPWebService.h"

@protocol SyncMasterAPIDelegate <NSObject>

@optional

-(void)api:(id)sender didFinishSyncMasterWithStatus:(BOOL)status;
-(void)api:(id)sender didFailSyncMasterWithError:(NSString*)error;

@end



@interface SyncMasterAPI : NSObject <SOAPWebServiceDelegate> {
    SOAPWebService * service_;
	NSObject <SyncMasterAPIDelegate> * syncMasterDelegate_;
    NSString *tableName;
}

@property(nonatomic,assign) NSObject <SyncMasterAPIDelegate> * syncMasterDelegate;
@property(nonatomic,retain) NSString *tableName;

- (void)performSyncMasterWithTableName:(NSString *)TableName andKey:(NSString*)Key;


@end
