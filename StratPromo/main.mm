//
//  main.m
//  StratPromo
//
//  Created by cat on 19/4/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "StratPromo_AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([StratPromo_AppDelegate class]));
    }
}
