//
//  SearchAPI.h
//  StratPromo
//
//  Created by cat on 29/5/13.
//  Copyright (c) 2013 StratAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SOAPWebService.h"

@protocol SearchAPIDelegate <NSObject>

@optional

-(void)api:(id)sender didFinishSearchWithStatus:(BOOL)status andDataArray: (NSMutableArray *)dataArray;
-(void)api:(id)sender didFailSearchWithError:(NSString*)error;

@end

@interface SearchAPI : NSObject<SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <SearchAPIDelegate> * searchDelegate_;
}
@property (strong, nonatomic) NSMutableArray *dataArray;
@property(nonatomic,assign) NSObject <SearchAPIDelegate> * searchDelegate;

- (void)performSearchTerm:(NSString *)SearchTerm atPageNumber:(NSString *)PageNumber;


@end
