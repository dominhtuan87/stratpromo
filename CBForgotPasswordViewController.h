//
//  CBForgotPasswordViewController.h
//  ClueBox
//
//  Created by cat on 12/9/12.
//
//

#import <UIKit/UIKit.h>
#import "LoginAPI.h"
#import "ActivityView.h"

@interface CBForgotPasswordViewController : UIViewController <LoginAPIDelegate> {
    
    //IBOutlet UITextField *emailField_;
    UIButton *backButton_;
}
@property(nonatomic,retain) IBOutlet UITextField *email;

- (IBAction)submitButtonPressed:(id)sender;
- (IBAction)backgroundTap:(id)sender;

@property (strong, nonatomic) ActivityView* activity1;

@end
