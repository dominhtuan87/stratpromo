//
//  GetProfileAPI.h
//  ClueBox
//
//  Created by cat on 27/11/12.
//
//

#import <Foundation/Foundation.h>
#import "SOAPWebService.h"
#import "User.h"

@protocol GetProfileAPIDelegate <NSObject>
@optional

-(void)api:(id)sender didFinishGetProfileWithStatus:(BOOL)status;
-(void)api:(id)sender didFailGetProfileWithError:(NSString*)error;

@end

@interface GetProfileAPI : NSObject <SOAPWebServiceDelegate> {
    
    SOAPWebService * service_;
	NSObject <GetProfileAPIDelegate> * GetProfileDelegate_;
}

@property(nonatomic,assign) NSObject <GetProfileAPIDelegate> * GetProfileDelegate;

- (void)getProfileWithToken:(NSString *)token;

@end
