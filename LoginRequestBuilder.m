//
//  LoginRequestBuilder.m
//  ClueBox
//
//  Created by user on 11/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginRequestBuilder.h"
#import "Utility.h"

@implementation LoginRequestBuilder

+(NSString*)authRequestContentForUser:(User*)user withAuthType:(AuthRequestType)type {
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"login" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Email__" 
																 withString:[Utility absoluteValueForString:user.userName]];
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Password__" 
																 withString:[Utility absoluteValueForString:
                                                                        user.password]];
		return contentFormat;
	}
	
	return @"";
}

+(NSString*)loginAgainForUserToken:(NSString *)token {
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LoginAgain" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__" 
																 withString:token];
        return contentFormat;
	}
	return @"";
}

+(NSString*)logoutForUserToken:(NSString *)token {
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Logout" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__" 
																 withString:token];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__PushKey__" 
																 withString:([Utility getDeviceToken])?[Utility getDeviceToken]:@"abcd"];
        return contentFormat;
	}
	return @"";
}

+(NSString*)forgotPasswordForUserWithEmail:(NSString *)email {
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ForgotPassword" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Email__" 
																 withString:email];
        return contentFormat;
	}
	return @"";
}

+(NSString*)signUpRequestForUser:(User*)user {
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Signup" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    
    if (contentFormat) {
		
        
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Name__"withString:user.userName];
        contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Email__"withString:user.email];
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Password__"withString:user.password];

        return contentFormat;
	}
	return @"";
}

@end
