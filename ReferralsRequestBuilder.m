//
//  ReferralsRequestBuilder.m
//  ClueBox
//
//  Created by cat on 5/12/12.
//
//

#import "ReferralsRequestBuilder.h"
#import "Utility.h"

@interface ReferralsRequestBuilder ()

@end

@implementation ReferralsRequestBuilder


+(NSString*)ReferralsWithToken:(NSString*)token andFriends:(NSString*)Friends{
    
    NSString * contentFormat = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Referrals" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
	
    if (contentFormat) {
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Token__"
																 withString:token];
		
		contentFormat = [contentFormat stringByReplacingOccurrencesOfString:@"__Friends__"
																 withString:Friends];
        
		return contentFormat;
	}
	
	return @"";

    
}



@end
