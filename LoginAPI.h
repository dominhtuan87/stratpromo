//
//  LoginAPI.h
//  ClueBox
//
//  Created by user on 11/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "SOAPWebService.h"

@protocol LoginAPIDelegate <NSObject>

@optional

-(void)api:(id)sender didFinishUserAuthenticationWithStatus:(BOOL)status;
-(void)api:(id)sender didFinishLoginAgainWithStatus:(BOOL)status;
-(void)api:(id)sender didFinishLogoutWithStatus:(BOOL)status;
-(void)api:(id)sender didFinishForgotPasswordWithStatus:(BOOL)status;
-(void)api:(id)sender didFinishSignUpWithStatus:(BOOL)status;
-(void)api:(id)sender didFailAuthWithError:(NSString*)error;

@end

@interface LoginAPI : NSObject  <SOAPWebServiceDelegate>{
    
    SOAPWebService * service_;
	NSObject <LoginAPIDelegate> * authDelegate_;
}

@property(nonatomic,assign) NSObject <LoginAPIDelegate> * authDelegate;

- (void)authorizeUser:(User *)user;
- (void)loginAgainWithToken:(NSString *)token;
- (void)logoutUserWithToken:(NSString *)token;
- (void)forgotPasswordForUserWithEmail:(NSString *)email;
- (void)signUpForUser:(User *)user;

@end
